using System;
using System.Collections.Generic;

namespace UM.P4.CoreTypes
{
    public static class C
    {
        public static Dictionary<string, RegisterId> RegisterMapping = new Dictionary<string, RegisterId>
        {
            {"1.8.0", RegisterId.R180},
            {"1.8.1", RegisterId.R181},
            {"1.8.2", RegisterId.R182},
            {"2.8.0", RegisterId.R280},
            {"2.8.1", RegisterId.R281},
            {"2.8.2", RegisterId.R282}
        };

        public const int PeakHourStartsFrom = 7;
        public const int PeakHourEndsWith = 22;

        public static bool IsPeakDate(DateTime utcDateTime)
        {
            var nlTime = utcDateTime.ToNLDateTime();
            if (nlTime.DayOfWeek == DayOfWeek.Saturday || nlTime.DayOfWeek == DayOfWeek.Sunday)
                return false;

            return nlTime.Hour >= PeakHourStartsFrom && nlTime.Hour <= PeakHourEndsWith;
        }

        public static bool IsPeakDate(DateTime utcDateTime, int peakHourEndsWith)
        {
            var nlTime = utcDateTime.ToNLDateTime();
            if (nlTime.DayOfWeek == DayOfWeek.Saturday || nlTime.DayOfWeek == DayOfWeek.Sunday || BankHolidays.Contains(nlTime.Date))
                return false;

            return nlTime.Hour >= PeakHourStartsFrom && nlTime.Hour <= peakHourEndsWith;
        }

        private static readonly List<DateTime> BankHolidays = new List<DateTime>
        {
            new DateTime(2016, 01, 01),
            new DateTime(2016, 03, 28),
            new DateTime(2016, 04, 27),
            new DateTime(2016, 05, 05),
            new DateTime(2016, 05, 15),
            new DateTime(2016, 05, 16),
            new DateTime(2016, 12, 25),
            new DateTime(2016, 12, 26),

            new DateTime(2017, 01, 01),
            new DateTime(2017, 04, 16),
            new DateTime(2017, 04, 17),
            new DateTime(2017, 04, 27),
            new DateTime(2017, 05, 25),
            new DateTime(2017, 06, 04),
            new DateTime(2017, 06, 05),
            new DateTime(2017, 12, 25),
            new DateTime(2017, 12, 26),

            new DateTime(2018, 01, 01),
            new DateTime(2018, 04, 01),
            new DateTime(2018, 04, 02),
            new DateTime(2018, 04, 27),
            new DateTime(2018, 05, 10),
            new DateTime(2018, 05, 20),
            new DateTime(2018, 05, 21),
            new DateTime(2018, 12, 25),
            new DateTime(2018, 12, 26),

            new DateTime(2019, 01, 01),
            new DateTime(2019, 04, 21),
            new DateTime(2019, 04, 22),
            new DateTime(2019, 04, 27),
            new DateTime(2019, 05, 30),
            new DateTime(2019, 06, 09),
            new DateTime(2019, 06, 10),
            new DateTime(2019, 12, 25),
            new DateTime(2019, 12, 26),

            new DateTime(2020, 01, 01),
            new DateTime(2020, 04, 10),
            new DateTime(2020, 04, 13),
            new DateTime(2020, 04, 27),
            new DateTime(2020, 05, 21),
            new DateTime(2020, 06, 01),
            new DateTime(2020, 12, 25),
            new DateTime(2020, 12, 26),

            new DateTime(2021, 01, 01),
            new DateTime(2021, 04, 02),
            new DateTime(2021, 04, 05),
            new DateTime(2021, 04, 27),
            new DateTime(2021, 05, 13),
            new DateTime(2021, 05, 24),
            new DateTime(2021, 12, 25),
            new DateTime(2021, 12, 26),

            new DateTime(2022, 01, 01),
            new DateTime(2022, 04, 15),
            new DateTime(2022, 04, 18),
            new DateTime(2022, 04, 27),
            new DateTime(2022, 05, 26),
            new DateTime(2022, 06, 06),
            new DateTime(2022, 12, 25),
            new DateTime(2022, 12, 26),

            new DateTime(2023, 01, 01),
            new DateTime(2023, 04, 07),
            new DateTime(2023, 04, 10),
            new DateTime(2023, 04, 27),
            new DateTime(2023, 05, 18),
            new DateTime(2023, 05, 29),
            new DateTime(2023, 12, 25),
            new DateTime(2023, 12, 26),

        };


        public static double ETax = 0.1232;
        public static double GTax = 0.1985;

        public const int Precision = 3;



        public static IReadOnlyCollection<string> DemoEans => new List<string>
        {
            "123456789123456781", // --> 871688540003151654
            "123456789123456782", // --> 871687400006095199
            "123456789123456783", // --> 871687400006095182
            "123456789123456784", // --> 871687140002196406
            "123456789123456785", // --> 871685900041252134
        };
    }
}