using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace UM.P4.CoreTypes.Messages
{

    public class DetailReadingsMessage : BaseMessage
    {
        public Dictionary<RegisterId, Dictionary<DateTime, double>> Readings { get; set; }

        [JsonIgnore]
        public Dictionary<RegisterId, double> Usage
        {
            get
            {
                var usage = Readings.ToDictionary(k => k.Key, v => 0.0);

                foreach (var detailReading in Readings)
                {
                    var firstReading = detailReading.Value.OrderBy(k => k.Key).First().Value;
                    var lastReading = detailReading.Value.OrderBy(k => k.Key).Last().Value;

                    usage[detailReading.Key] = CalculateUsage(firstReading, lastReading);
                }

                return usage;
            }
        }

        private double CalculateUsage(double rFirst, double rLast)
        {
            if (rLast == 0 && rFirst == 0)
            {
                return 0;
            }

            if (rLast == 0 || rFirst == 0)
            {
                return -1;
            }

            var u = rLast - rFirst;


            // probably exceeding of meter
            if (u < 0)
            {

                //Trace.WriteLine("Exceed of counter: " + Ean + ":" + Date.ToString("yyyyMMdd"));

                var numbers = rFirst.ToString().Split('.')[0].Length;

                string biggestNumberStr = string.Empty;
                for (int i = 0; i < numbers; i++)
                {
                    biggestNumberStr += "9";
                }

                var biggestNumber = Int32.Parse(biggestNumberStr);

                u = biggestNumber - rFirst;
                u += rLast;
            }

            return u;
        }

    }
}