﻿using System;

namespace UM.P4.CoreTypes.Messages
{
    public class BaseMessage
    {
        public BaseMessage()
        {
            RaisedOnUtc = DateTime.UtcNow;
        }

        public string Ean { get; set; }
        public string QueryDate { get; set; }

        public DateTime RaisedOnUtc { get; set; }
    }
}
