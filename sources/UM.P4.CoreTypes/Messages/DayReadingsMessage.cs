﻿using System;
using System.Collections.Generic;

namespace UM.P4.CoreTypes.Messages
{

    public class DayReadingsMessage : BaseMessage
    {
        public Dictionary<RegisterId, double> Readings { get; set; }

        public Dictionary<RegisterId, double> GetUsage(DayReadingsMessage prevReading)
        {
            var usage = new Dictionary<RegisterId, double>();
            if (Readings.ContainsKey(RegisterId.R181))
            {
                usage[RegisterId.R180] =
                    CalculateUsage(prevReading.Readings[RegisterId.R181] + prevReading.Readings[RegisterId.R182],
                        Readings[RegisterId.R181] + Readings[RegisterId.R182]);
                usage[RegisterId.R280] =
                    CalculateUsage(prevReading.Readings[RegisterId.R281] + prevReading.Readings[RegisterId.R282],
                        Readings[RegisterId.R281] + Readings[RegisterId.R282]);
            }
            else
            {
                usage[RegisterId.R180] =
                    CalculateUsage(prevReading.Readings[RegisterId.R180], Readings[RegisterId.R180]);

            }

            return usage;
        }

        private double CalculateUsage(double rFirst, double rLast)
        {
            if (Math.Abs(rLast) < 0.0001 && Math.Abs(rFirst) < 0.0001)
            {
                return 0;
            }

            if (Math.Abs(rFirst) < 0.0001)
            {
                return -1;
            }

            var u = rLast - rFirst;


            // probably exceeding of meter
            if (u < 0)
            {
                if (Math.Abs(rLast) < 0.0001 || Math.Abs(rFirst) < 0.0001)
                {
                    return -1;
                }
                var numbers = rFirst.ToString().Split('.')[0].Length;

                string biggestNumberStr = string.Empty;
                for (int i = 0; i < numbers; i++)
                {
                    biggestNumberStr += "9";
                }

                var biggestNumber = Int32.Parse(biggestNumberStr);

                u = biggestNumber - rFirst;
                u += rLast;
            }

            return Math.Round(u, 2);
        }
    }
}
