﻿namespace UM.P4.CoreTypes.Messages
{
    public class RejectionMessage : BaseMessage
    {
        public string QueryReason { get; set; }

        public string RejectionCode { get; set; }
        public string RejectionText { get; set; }
    }
}