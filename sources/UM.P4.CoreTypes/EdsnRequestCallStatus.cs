namespace UM.P4.CoreTypes
{
    public enum EdsnRequestCallStatus
    {
        ToSubmit,
        Submitted,
        SubmitFailed
    }
}