﻿namespace UM.P4.CoreTypes
{
    public enum MPState
    {
        Started,
        Paused,
        Stopped,
        NoSmartMeter,

        StartRequested,
        Rejection_040,
        RenewRequested
    }
}