namespace UM.P4.CoreTypes
{
    public enum EdsnResponseCallStatus
    {
        ToProcess,
        Ok,
        Rejected,
        Error,
        Invalid
    }
}