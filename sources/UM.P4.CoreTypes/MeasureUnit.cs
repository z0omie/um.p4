﻿namespace UM.P4.CoreTypes
{
    public enum MeasureUnit
    {
        Usage,
        Day,
        Wh,
        Kwh,
        Mtq
    }
}