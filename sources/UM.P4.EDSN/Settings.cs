using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using System;
using System.Configuration;
using System.Threading;

namespace UM.P4.EDSN
{
    public sealed class Settings
    {
        private static readonly Lazy<Settings> instance = new Lazy<Settings>(() => new Settings(), LazyThreadSafetyMode.PublicationOnly);
        public static Settings Instance => instance.Value;

        public Settings()
        {
            // TEST
            //Certificate = GetCertificate("p4_test.p12");
            //P4Root = GetCertificate("StaatDerN_PrivateRoot.cer");

            // PROD
            Certificate = GetCertificate("cert_backup.pfx");
            P4Root = GetCertificate("PrivateRootCA-G1.cer");
        }

        public TimeSpan P4BindingCloseTimeout => new TimeSpan(0, 0, 5, 0);
	    public TimeSpan P4BindingOpenTimeout => new TimeSpan(0, 0, 5, 0);
	    public TimeSpan P4BindingReceiveTimeout => new TimeSpan(0, 0, 5, 0);
	    public TimeSpan P4BindingSendTimeout => new TimeSpan(0, 0, 5, 0);

        // TEST
        //public const string P4ServiceAddress = "https://pp4-test.edsn.nl";

        // PROD
        public const string P4ServiceAddress = "https://p4.edsn.nl";

        // TEST
        //public const string P4DnsIdentity = "TEST Staat der Nederlanden Private Root CA - G1";

        // PROD
        public const string P4DnsIdentity = "Staat der Nederlanden Private Root CA - G1";

        public const string P4Port = "/P4BatchVerzoekMeterstand/P4Port";



        public byte[] Certificate;

        private byte[] GetCertificate(string name)
        {
            string storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=p4functions;AccountKey=OTs2aKcfcr7rSfX1eSZPrfSfH7nWv5giDafaoHfLoXsl5sQrl4JH5SCcRJmY4T/Wgzo6si1peVZt1GiH3UtFTg==;EndpointSuffix=core.windows.net";

            // Check whether the connection string can be parsed.
            if (!CloudStorageAccount.TryParse(storageConnectionString, out CloudStorageAccount storageAccount))
            {
                throw new Exception("unparsable blob connection string: " + storageConnectionString);
            }

            CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference("p4certificates");
            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(name);

            cloudBlockBlob.FetchAttributes();

            byte[] fileContent = new byte[cloudBlockBlob.Properties.Length];

            cloudBlockBlob.DownloadToByteArray(fileContent, 0);

            return fileContent;
        }

        public string CertificatePass => "123456";

        public byte[] P4Root;

        public const string UMeterEan = "8712423035904";
    }
}