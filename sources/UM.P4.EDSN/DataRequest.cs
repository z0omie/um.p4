﻿using System;

namespace UM.P4.EDSN
{
    public class DataRequest
    {
        public string Ean { get; set; }
        public string GridOperator { get; set; }
        public byte QueryReasonId { get; set; }
        public DateTime QueryDate { get; set; }
        public string Reference { get; set; }
        public string RsId { get; set; }
    }
}