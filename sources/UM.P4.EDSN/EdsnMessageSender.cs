using MongoDB.Bson;

namespace UM.P4.EDSN
{
    public class EdsnMessageSender
    {
        public static IResponseInfo SendDataRequest(P4CollectedDataBatchRequestRequest request, ObjectId callId)
        {
            return Client.CallServiceMethod<P4PortType>(
                request,
                Client.P4CollectedDataBatchRequestEnvelope,
                callId);
        }

        public static IResponseInfo SendResultRequest(P4CollectedDataBatchResultRequestRequest request,
            ObjectId callId)
        {
            return Client.CallServiceMethod<P4PortType>(
                request,
                Client.P4CollectedDataBatchResultRequestEnvelope,
                callId);
        }
    }
}