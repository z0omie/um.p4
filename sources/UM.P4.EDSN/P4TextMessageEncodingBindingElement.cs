using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Xml;

namespace UM.P4.EDSN
{
    public class P4TextMessageEncodingBindingElement : MessageEncodingBindingElement, IWsdlExportExtension, IPolicyExportExtension
    {
        private MessageVersion _version;
        private string _mediaType;
        private System.Text.Encoding _encoding;
        private readonly XmlDictionaryReaderQuotas _readerQuotas;

        P4TextMessageEncodingBindingElement(P4TextMessageEncodingBindingElement binding)
            : this(binding.WriteEncoding, binding.MediaType, binding.MessageVersion)
        {
            _readerQuotas = new XmlDictionaryReaderQuotas();
            binding.ReaderQuotas.CopyTo(_readerQuotas);
        }

        public P4TextMessageEncodingBindingElement(System.Text.Encoding encoding, string mediaType, MessageVersion version)
        {
            if (encoding == null)
                throw new ArgumentNullException("encoding");

            if (mediaType == null)
                throw new ArgumentNullException("mediaType");

            if (version == null)
                throw new ArgumentNullException("version");

            _version = version;
            _mediaType = mediaType;
            _encoding = encoding;
            _readerQuotas = new XmlDictionaryReaderQuotas();
        }

        public P4TextMessageEncodingBindingElement(System.Text.Encoding encoding, string mediaType)
            : this(encoding, mediaType, MessageVersion.Soap11WSAddressing10)
        { }

        public P4TextMessageEncodingBindingElement(System.Text.Encoding encoding)
            : this(encoding, "text/xml")
        { }

        public P4TextMessageEncodingBindingElement()
            : this(System.Text.Encoding.UTF8)
        { }

        public override MessageVersion MessageVersion
        {
            get { return _version; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");
                _version = value;
            }
        }

        public string MediaType
        {
            get { return _mediaType; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");
                _mediaType = value;
            }
        }

        public System.Text.Encoding WriteEncoding
        {
            get { return _encoding; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");
                _encoding = value;
            }
        }

        // This encoder does not enforces any quotas for the unsecure messages. The 
        // quotas are enforced for the secure portions of messages when this encoder
        // is used in a binding that is configured with security. 
        public XmlDictionaryReaderQuotas ReaderQuotas { get { return _readerQuotas; } }

        #region IMessageEncodingBindingElement Members

        public override MessageEncoderFactory CreateMessageEncoderFactory()
        {
            return new P4TextMessageEncoderFactory(MediaType, WriteEncoding, MessageVersion);
        }

        #endregion

        #region IPolicyExportExtension Members

        public void ExportPolicy(MetadataExporter exporter, PolicyConversionContext context)
        {
            //Logging.LogManager.Handler.Error(new ErrorEntry { Message = "Export policy is not implemented!" });
        }

        #endregion

        #region IWsdlExportExtension

        public void ExportContract(WsdlExporter exporter, WsdlContractConversionContext context)
        {
            //Logging.LogManager.Handler.Error(new ErrorEntry { Message = "Export contact is not implemented!" });
        }

        public void ExportEndpoint(WsdlExporter exporter, WsdlEndpointConversionContext context)
        {
            //Logging.LogManager.Handler.Error(new ErrorEntry { Message = "Export endpoint is not implemented!" });
        }

        #endregion

        public override BindingElement Clone()
        {
            return new P4TextMessageEncodingBindingElement(this);
        }

        public override IChannelFactory<TChannel> BuildChannelFactory<TChannel>(BindingContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            context.BindingParameters.Add(this);
            return context.BuildInnerChannelFactory<TChannel>();
        }

        public override bool CanBuildChannelFactory<TChannel>(BindingContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            return context.CanBuildInnerChannelFactory<TChannel>();
        }

        public override IChannelListener<TChannel> BuildChannelListener<TChannel>(BindingContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            context.BindingParameters.Add(this);
            return context.BuildInnerChannelListener<TChannel>();
        }

        public override bool CanBuildChannelListener<TChannel>(BindingContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            context.BindingParameters.Add(this);
            return context.CanBuildInnerChannelListener<TChannel>();
        }

        public override T GetProperty<T>(BindingContext context)
        {
            if (typeof(T) == typeof(XmlDictionaryReaderQuotas))
            {
                return (T)(object)_readerQuotas;
            }
            return base.GetProperty<T>(context);
        }
    }
}