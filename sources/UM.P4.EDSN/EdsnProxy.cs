﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;

namespace UM.P4.EDSN
{
    public class EdsnProxy
    {
        public static IResponseInfo CreateDataRequest(IEnumerable<DataRequest> dataRequests, string go, ObjectId callId = default(ObjectId))
        {
            if (callId == default(ObjectId))
            {
                callId = ObjectId.GenerateNewId();
            }

            // InsertOneAsync header
            var businessDocumentHeader = new P4CollectedDataBatchRequestEnvelope_EDSNBusinessDocumentHeader
            {
                CreationTimestamp = DateTime.UtcNow,
                MessageID = callId.ToString()
            };
            var documentHeaderSource = new P4CollectedDataBatchRequestEnvelope_EDSNBusinessDocumentHeader_Source
            {
                SenderID = Settings.UMeterEan
            };
            var headerDestination = new P4CollectedDataBatchRequestEnvelope_EDSNBusinessDocumentHeader_Destination
            {
                Receiver = new P4CollectedDataBatchRequestEnvelope_EDSNBusinessDocumentHeader_Destination_Receiver
                {
                    ReceiverID = go
                }
            };
            businessDocumentHeader.Source = documentHeaderSource;
            businessDocumentHeader.Destination = headerDestination;

            var request = new P4CollectedDataBatchRequestRequest
            {
                P4CollectedDataBatchRequestEnvelope = new P4CollectedDataBatchRequestEnvelope
                {
                    EDSNBusinessDocumentHeader = businessDocumentHeader
                }
            };

            // Add requests
            request.P4CollectedDataBatchRequestEnvelope.P4Content =
                dataRequests.Select(dr => new P4CollectedDataBatchRequestEnvelope_P4Content_P4MeteringPoint
                {
                    EANID = dr.Ean,
                    QueryDate = dr.QueryDate.Date,
                    QueryDateSpecified = true,
                    QueryReason = QueryReasonToP4DataRequestEnum(dr.QueryReasonId),
                    ExternalReference = dr.Reference
                })
            .ToArray();


            IResponseInfo response = EdsnMessageSender.SendDataRequest(request, callId);

            //TODO: Log it


            return response;
        }

        public static IResponseInfo GetDataResponse(string go, ObjectId callId = default(ObjectId))
        {
            if (callId == default(ObjectId))
            {
                callId = ObjectId.GenerateNewId();
            }

            // InsertOneAsync envelope
            var resultRequestEnvelope = new P4CollectedDataBatchResultRequestEnvelope();
            var businessDocumentHeader = new P4CollectedDataBatchResultRequestEnvelope_EDSNBusinessDocumentHeader
            {
                CreationTimestamp = DateTime.UtcNow,
                MessageID = callId.ToString()
            };
            var documentHeaderSource = new P4CollectedDataBatchResultRequestEnvelope_EDSNBusinessDocumentHeader_Source
            {
                SenderID = Settings.UMeterEan
            };
            var headerDestination = new P4CollectedDataBatchResultRequestEnvelope_EDSNBusinessDocumentHeader_Destination
            {
                Receiver = new P4CollectedDataBatchResultRequestEnvelope_EDSNBusinessDocumentHeader_Destination_Receiver
                {
                    ReceiverID = go
                }
            };

            // InsertOneAsync header
            businessDocumentHeader.Source = documentHeaderSource;
            businessDocumentHeader.Destination = headerDestination;
            resultRequestEnvelope.EDSNBusinessDocumentHeader = businessDocumentHeader;
            resultRequestEnvelope.P4Content = new P4CollectedDataBatchResultRequestEnvelope_P4Content();

            // Build request, get response
            var request = new P4CollectedDataBatchResultRequestRequest { P4CollectedDataBatchResultRequestEnvelope = resultRequestEnvelope };



            IResponseInfo response = EdsnMessageSender.SendResultRequest(request, callId);

            return response;
        }

        public static P4CollectedDataBatchRequestEnvelope_QueryReasonTypeCode QueryReasonToP4DataRequestEnum(byte queryReason)
        {
            switch (queryReason)
            {
                case 1:
                    return P4CollectedDataBatchRequestEnvelope_QueryReasonTypeCode.INT;
                case 2:
                case 0:
                    return P4CollectedDataBatchRequestEnvelope_QueryReasonTypeCode.DAY;
                case 3:
                    return P4CollectedDataBatchRequestEnvelope_QueryReasonTypeCode.RCY;
                default:
                    throw new Exception("no EDSN mapping setup for Query reason: " + queryReason);
            }
        }
    }
}