using System;
using System.IO;
using System.ServiceModel.Channels;
using System.Text;
using System.Xml;

namespace UM.P4.EDSN
{
    public class P4TextMessageEncoder : MessageEncoder
    {
        private readonly P4TextMessageEncoderFactory _factory;
        private readonly XmlWriterSettings _writerSettings;

        public P4TextMessageEncoder(P4TextMessageEncoderFactory factory)
        {
            _factory = factory;

            _writerSettings = new XmlWriterSettings { Encoding = Encoding.GetEncoding(factory.CharSet) };
            ContentType = $"{_factory.MediaType};charset={_writerSettings.Encoding.HeaderName}";
        }

        public override string ContentType { get; }

        public override string MediaType => _factory.MediaType;

        public override MessageVersion MessageVersion => _factory.MessageVersion;

        public override Message ReadMessage(ArraySegment<byte> buffer, BufferManager bufferManager, string contentType)
        {
            var msgContents = new byte[buffer.Count];
            Array.Copy(buffer.Array, buffer.Offset, msgContents, 0, msgContents.Length);
            bufferManager.ReturnBuffer(buffer.Array);
            var stream = new MemoryStream(msgContents);
            return ReadMessage(RemoveEnvelope(stream), int.MaxValue, contentType);
        }

        // Replaced ... fault message was not handled correctly
        //
        //public static Stream RemoveEnvelope(Stream input)
        //{
        //    const string securityTag = "wsse:Security";
        //    // Cannot process buffered -- read it all into a string
        //    string xml = new StreamReader(input).ReadToEnd();

        //    // Seek the Security tag this is what we need to remove
        //    int header;
        //    if ((header = xml.IndexOf(securityTag, StringComparison.Ordinal)) > 0)
        //    {
        //        int start = xml.LastIndexOf("<", header, StringComparison.Ordinal);
        //        int end = xml.LastIndexOf(securityTag, StringComparison.Ordinal);
        //        end = xml.IndexOf(">", end, StringComparison.Ordinal) + 1;

        //        xml = xml.Substring(0, start) + xml.Substring(end);
        //    }
        //    MemoryStream ms = new MemoryStream(new UTF8Encoding().GetBytes(xml));
        //    return ms;
        //}

        public static Stream RemoveEnvelope(Stream input)
        {
            const string removeThisTag = "wsse:Security";

            // Cannot process buffered -- read it all into a string
            var xml = new StreamReader(input).ReadToEnd();

            // Process the string using the XmlReader class, since it is complex to parse strings with XML namespaces...
            // XmlReader xr = XmlReader.InsertOneAsync(new StringReader(xml));
            // Seek the elements we need to modify
            int header;

            if ((header = xml.IndexOf(removeThisTag, StringComparison.Ordinal)) > 0)
            {
                var start = xml.LastIndexOf("<", header, StringComparison.Ordinal);
                var end = xml.LastIndexOf(removeThisTag, StringComparison.Ordinal);
                end = xml.IndexOf(">", end, StringComparison.Ordinal) + 1;
                xml = xml.Substring(0, start) + xml.Substring(end);
                var ms = new MemoryStream(new UTF8Encoding().GetBytes(xml));
                return ms;
            }

            input.Seek(0L, SeekOrigin.Begin);
            return input;
        }

        public override Message ReadMessage(Stream stream, int maxSizeOfHeaders, string contentType)
        {
            var reader = XmlReader.Create(stream);
            Message msg;
            try { msg = Message.CreateMessage(reader, maxSizeOfHeaders, MessageVersion); }
            catch { msg = null; }
            return msg;
        }

        public override ArraySegment<byte> WriteMessage(Message message, int maxMessageSize, BufferManager bufferManager, int messageOffset)
        {
            var stream = new MemoryStream();
            var writer = XmlWriter.Create(stream, _writerSettings);
            message.WriteMessage(writer);
            writer.Close();

            var messageBytes = stream.GetBuffer();
            var messageLength = (int)stream.Position;
            stream.Close();

            var totalLength = messageLength + messageOffset;
            var totalBytes = bufferManager.TakeBuffer(totalLength);
            Array.Copy(messageBytes, 0, totalBytes, messageOffset, messageLength);

            var byteArray = new ArraySegment<byte>(totalBytes, messageOffset, messageLength);
            return byteArray;
        }

        public override void WriteMessage(Message message, Stream stream)
        {
            var writer = XmlWriter.Create(stream, _writerSettings);
            message.WriteMessage(writer);
            writer.Close();
        }
    }
}