using System;
using System.Configuration;
using System.Runtime.Serialization.Formatters;
using System.ServiceModel;
using System.ServiceModel.Description;
using MongoDB.Bson;
using MongoDB.Driver;
using UM.P4.Mongo;
using UM.P4.Mongo.Contexts;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.EDSN
{
    public class Client
    {
        static MongoClient mongoClient;
        static Client()
        {
            var mongoUrlBuilder = new MongoUrlBuilder(ConfigurationManager.AppSettings["MongoServer"]);

            var connectionString = mongoUrlBuilder.ToMongoUrl();

            mongoClient = new MongoClient(connectionString);
        }


        public static IResponseInfo CallServiceMethod<TClientInterface>(object request,
            Func<object, object, object> function, ObjectId callId)
            where TClientInterface : class
        {
  

            IEndpointBehavior edsnClientBehaviour;
            IRawCallRepo _rawCallTypedRepo;

            if (request is P4CollectedDataBatchRequestRequest)
            {
                edsnClientBehaviour = new EdsnClientBehaviour<RequestRawCall>(callId);
                _rawCallTypedRepo = new RawCallTypedRepository<RequestRawCall>(new MongoRawCallContext<RequestRawCall>(mongoClient));
            }
            else if (request is P4CollectedDataBatchResultRequestRequest)
            {
                edsnClientBehaviour = new EdsnClientBehaviour<ResponseRawCall>(callId);
                _rawCallTypedRepo = new RawCallTypedRepository<ResponseRawCall>(new MongoRawCallContext<ResponseRawCall>(mongoClient));
            }
            else
            {
                throw new Exception($"request type is not supported: {request.GetType().FullName}");
            }

            var channelFactory = new MessageHandlerChannelFactory<TClientInterface>();
            channelFactory.Endpoint.Behaviors.Add(edsnClientBehaviour);

            var client = ((IClientChannel)channelFactory.CreateChannel());

            object response;
            try
            {
                response = function.Invoke(client, request);
            }
            catch (FaultException<SoapFault> ex)
            {
                _rawCallTypedRepo.SubmitFailed(callId, ex.ToString());
                //Logging.LogManager.Handler.Exception(ex);
                return new ResponseInfo(null, callId) { IsSuccess = false, FailureReasonText = ex.ToString() };
            }
            catch (TimeoutException ex)
            {
                _rawCallTypedRepo.SubmitFailed(callId, ex.ToString());
                //Logging.LogManager.Handler.Exception(ex);
                return new ResponseInfo(null, callId) { IsSuccess = false, FailureReasonText = ex.ToString() };
            }
            catch (FaultException ex)
            {
                _rawCallTypedRepo.SubmitFailed(callId, ex.ToString());
                //Logging.LogManager.Handler.Exception(ex);
                return new ResponseInfo(null, callId) { IsSuccess = false, FailureReasonText = ex.ToString() };
            }
            catch (CommunicationException ex)
            {
                _rawCallTypedRepo.SubmitFailed(callId, ex.ToString());
                //Logging.LogManager.Handler.Exception(ex);
                return new ResponseInfo(null, callId) { IsSuccess = false, FailureReasonText = ex.ToString() };
            }
            catch (Exception ex)
            {
                _rawCallTypedRepo.SubmitFailed(callId, ex.ToString());
                //Logging.LogManager.Handler.Exception(ex);
                return new ResponseInfo(null, callId) { IsSuccess = false, FailureReasonText = ex.ToString() };
            }
            finally
            {
                if (client.State == CommunicationState.Opening || client.State == CommunicationState.Opened)
                {
                    client.Close();
                }
            }

            return new ResponseInfo(response, callId) { IsSuccess = true };
        }


        #region EDSN Services

        public static object P4CollectedDataBatchRequestEnvelope(object client,
            object request)
        {
            return ((P4PortType)client).P4CollectedDataBatchRequest((P4CollectedDataBatchRequestRequest)request);
        }

        public static object P4CollectedDataBatchResultRequestEnvelope(object client,
            object request)
        {
            return ((P4PortType)client).P4CollectedDataBatchResultRequest((P4CollectedDataBatchResultRequestRequest)request);
        }

        #endregion
    }
}
