﻿using System.ServiceModel.Channels;

namespace UM.P4.EDSN
{
    public class P4TextMessageEncoderFactory : MessageEncoderFactory
    {
        internal P4TextMessageEncoderFactory(string mediaType, System.Text.Encoding encoding, MessageVersion version)
        {
            MessageVersion = version;
            MediaType = mediaType;
            CharSet = encoding.WebName;
            Encoder = new P4TextMessageEncoder(this);
        }

        public override MessageEncoder Encoder { get; }
        public override MessageVersion MessageVersion { get; }

        internal string MediaType { get; }
        internal string CharSet { get; }
    }
}