using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using MongoDB.Bson;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.EDSN
{
    public class EdsnClientBehaviour<T> : IEndpointBehavior
        where T : RawCallBase
    {
        private readonly ObjectId _callId;

        public EdsnClientBehaviour(ObjectId callId)
        {
            _callId = callId;
        }

        public void AddBindingParameters(
            ServiceEndpoint endpoint,
            BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(
            ServiceEndpoint endpoint,
            ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(Inspector);
        }

        public void ApplyDispatchBehavior(
            ServiceEndpoint endpoint,
            EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(
            ServiceEndpoint endpoint)
        {
        }

        private MessageInspector<T> _inspector;

        public MessageInspector<T> Inspector => _inspector ?? (_inspector = new MessageInspector<T>(_callId));
    }
}