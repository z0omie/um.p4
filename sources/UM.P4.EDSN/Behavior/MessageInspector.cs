using System;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using UM.P4.Mongo;
using UM.P4.Mongo.Contexts;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.EDSN
{
    public class MessageInspector<T> : IClientMessageInspector
        where T : RawCallBase
    {
        private readonly RawCallTypedRepository<T> _rawCallTypedRepo;

        private T entity = null;

        public readonly ObjectId CallId;


        static MongoClient mongoClient;
        static MessageInspector()
        {
            var mongoUrlBuilder = new MongoUrlBuilder(ConfigurationManager.AppSettings["MongoServer"]);

            var connectionString = mongoUrlBuilder.ToMongoUrl();

            mongoClient = new MongoClient(connectionString);
        }


        public MessageInspector(ObjectId callId)
        {
            CallId = callId;

            _rawCallTypedRepo = new RawCallTypedRepository<T>(new MongoRawCallContext<T>(mongoClient));
        }


        public void AfterReceiveReply(
            ref Message reply,
            object correlationState)
        {
            MessageBuffer buffer = reply.CreateBufferedCopy(Int32.MaxValue);
            Message msg = buffer.CreateMessage();
            StringBuilder sb = new StringBuilder();
            using (System.Xml.XmlWriter xw = System.Xml.XmlWriter.Create(sb))
            {
                msg.WriteMessage(xw);
                xw.Close();
            }

            reply = buffer.CreateMessage();

            // save to Mongo
            var content = MinimizeContent(sb.ToString());
            var isFault = XDocument.Parse(content).Descendants().Any(n => n.Name.LocalName == "Fault");

            // entity.AttachResponse(content);

            _rawCallTypedRepo.AttachResponse(CallId, content, isFault).Wait();
        }

        public object BeforeSendRequest(
            ref Message request,
            IClientChannel channel)
        {
            // format content
            var content = MinimizeContent(request.ToString());

            // create entity
            entity = (T)Activator.CreateInstance(typeof(T), CallId, content);
            _rawCallTypedRepo.InsertOneAsync(entity).Wait();

            return null;
        }


        private static string MinimizeContent(string content)
        {
            content = Regex.Replace(content, @"\r", "");
            content = Regex.Replace(content, @"\n", "");
            content = Regex.Replace(content, @"  ", "");
            content = content.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
            return content;
        }
    }
}