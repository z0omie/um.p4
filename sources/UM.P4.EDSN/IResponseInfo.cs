using MongoDB.Bson;

namespace UM.P4.EDSN
{
    public interface IResponseInfo
    {
        bool IsSuccess { get; set; }
        string FailureReasonText { get; set; }
        object Response { get; set; }
        ObjectId CallId { get; set; }
    }
}