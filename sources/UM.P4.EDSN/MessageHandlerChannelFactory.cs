﻿using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Text;

namespace UM.P4.EDSN
{
	public class MessageHandlerChannelFactory<TChannel> : ChannelFactory<TChannel> where TChannel : class
	{
		public MessageHandlerChannelFactory()
		{
			Endpoint.Address = GetEndpointAddress();
			Endpoint.Binding = GetBinding();
			AttachAppropriateCertificates(Credentials);
		}

		private void AttachAppropriateCertificates(ClientCredentials clientCredentials)
		{
			var settings = Settings.Instance;
			clientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = System.ServiceModel.Security.X509CertificateValidationMode.None;
			clientCredentials.ServiceCertificate.Authentication.TrustedStoreLocation = StoreLocation.LocalMachine;
			clientCredentials.ServiceCertificate.DefaultCertificate = new X509Certificate2(Settings.Instance.P4Root);

			clientCredentials.ClientCertificate.Certificate = new X509Certificate2(Settings.Instance.Certificate, Settings.Instance.CertificatePass, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		}

		private Binding GetBinding()
		{
			CustomBinding binding = GetNewEdsnBinding();

			binding.Elements.Add(GetTextEncoderBindingElement());

			var securityBindingElement = SecurityBindingElement.CreateMutualCertificateBindingElement(
				MessageSecurityVersion
					.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10,
				allowSerializedSigningTokenOnReply: true);
			securityBindingElement.IncludeTimestamp = false;
			securityBindingElement.KeyEntropyMode = System.ServiceModel.Security.SecurityKeyEntropyMode.ClientEntropy;
			securityBindingElement.LocalClientSettings.DetectReplays = false;
			securityBindingElement.EnableUnsecuredResponse = true;
			securityBindingElement.LocalServiceSettings.DetectReplays = false;
			securityBindingElement.AllowInsecureTransport = true;
			binding.Elements.Add(securityBindingElement);

			binding.Elements.Add(GetTransportBindingElement());
			return binding;
		}

		private BindingElement GetTransportBindingElement()
		{
			var httpsTransportBindingElement = new HttpsTransportBindingElement
			{
				AuthenticationScheme = System.Net.AuthenticationSchemes.Anonymous,
				RequireClientCertificate = true,
				MaxReceivedMessageSize = 62914560,
			};

			return httpsTransportBindingElement;
		}

		private CustomBinding GetNewEdsnBinding()
		{
			var settings = Settings.Instance;
			return new CustomBinding
			{
				CloseTimeout = settings.P4BindingCloseTimeout,
				OpenTimeout = settings.P4BindingOpenTimeout,
				ReceiveTimeout = settings.P4BindingReceiveTimeout,
				SendTimeout = settings.P4BindingSendTimeout,
			};
		}

		protected virtual BindingElement GetTextEncoderBindingElement()
		{
			var binding = new P4TextMessageEncodingBindingElement
			{
				MessageVersion = MessageVersion.Soap11,
				WriteEncoding = Encoding.UTF8,
			};
			return binding;
		}


		private EndpointAddress GetEndpointAddress()
		{
			var settings = Settings.Instance;

			var suffix = $"{Settings.P4ServiceAddress}{Settings.P4Port}";
			var identity = EndpointIdentity.CreateDnsIdentity(Settings.P4DnsIdentity);
			var endpoint = new EndpointAddress(new Uri(suffix), identity);

			return endpoint;
		}
	}
}