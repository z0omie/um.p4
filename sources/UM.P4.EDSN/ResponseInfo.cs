﻿using MongoDB.Bson;

namespace UM.P4.EDSN
{
    public class ResponseInfo : IResponseInfo
    {
        public ResponseInfo(object response, ObjectId callId)
        {
            Response = response;

            CallId = callId;
        }

        public bool IsSuccess { get; set; }

        public string FailureReasonText { get; set; }

        public object Response { get; set; }
        public ObjectId CallId { get; set; }
    }
}