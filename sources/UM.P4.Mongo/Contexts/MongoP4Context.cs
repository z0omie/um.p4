﻿using MongoDB.Driver;
using UM.P4.Mongo.Models.P4;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.Mongo.Contexts
{
    public class MongoP4Context : IMongoP4Context
    {
        public MongoP4Context(IMongoClient mongoClient)
        {
            var db = mongoClient.GetDatabase("P4");

            P4DayReadings = db.GetCollection<P4DayReadings>("P4DayReadings");
            P4DetailReadings = db.GetCollection<P4DetailReadings>("P4DetailReadings");

            MeteringPoints = db.GetCollection<MeteringPoint>("meteringPoint");
            GridOperators = db.GetCollection<GridOperator>("gridOperator");

            OldRawCalls = db.GetCollection<RawCall>("rawCall");
        }

        public IMongoCollection<P4DayReadings> P4DayReadings { get; }

        public IMongoCollection<P4DetailReadings> P4DetailReadings { get; }

        public IMongoCollection<MeteringPoint> MeteringPoints { get; }

        public IMongoCollection<GridOperator> GridOperators { get; }

        public IMongoCollection<RawCall> OldRawCalls { get; }
    }
}