﻿using MongoDB.Driver;
using UM.P4.Mongo.Models.Requests;

namespace UM.P4.Mongo.Contexts
{
    //TODO: make it geeric singleton per type?
    public class MongoRequestContext<T> : IMongoRequestContext<T>
        where T : RequestBase
    {
        public MongoRequestContext(IMongoClient mongoClient)
        {
            var db = mongoClient.GetDatabase("request");
            Request = db.GetCollection<T>(typeof(T).Name[0].ToString().ToLower() + typeof(T).Name.Substring(1));
        }

        public IMongoCollection<T> Request { get; }
    }
}