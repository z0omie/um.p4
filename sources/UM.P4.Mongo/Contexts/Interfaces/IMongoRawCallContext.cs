﻿using MongoDB.Driver;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.Mongo.Contexts
{
    public interface IMongoRawCallContext<T>
        where T : RawCallBase
    {
        IMongoCollection<T> RawCall { get; }
    }
}