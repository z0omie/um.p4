﻿using MongoDB.Driver;
using UM.P4.Mongo.Models.Requests;

namespace UM.P4.Mongo.Contexts
{
    public interface IMongoRequestContext<T>
        where T : RequestBase
    {
        IMongoCollection<T> Request { get; }
    }
}