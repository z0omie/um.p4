﻿using MongoDB.Driver;
using UM.P4.Mongo.Models.P4;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.Mongo.Contexts
{
    public interface IMongoP4Context
    {
        IMongoCollection<P4DayReadings> P4DayReadings { get; }

        IMongoCollection<P4DetailReadings> P4DetailReadings { get; }

        IMongoCollection<MeteringPoint> MeteringPoints { get; }

        IMongoCollection<GridOperator> GridOperators { get; }

        IMongoCollection<RawCall> OldRawCalls { get; }
    }
}