using MongoDB.Driver;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.Mongo.Contexts
{
    public class MongoRawCallContext<T> : IMongoRawCallContext<T>
        where T : RawCallBase
    {
        public MongoRawCallContext(IMongoClient mongoClient)
        {
            var db = mongoClient.GetDatabase("request");
            RawCall = db.GetCollection<T>(typeof(T).Name[0].ToString().ToLower() + typeof(T).Name.Substring(1));
        }

        public IMongoCollection<T> RawCall { get; }
    }
}