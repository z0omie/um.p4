﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using UM.P4.Mongo.Contexts;

namespace UM.P4.Mongo
{
    public class RawCallRepository
    {
        private readonly IMongoP4Context _mongoP4Context;

        public RawCallRepository(IMongoP4Context mongoP4Context)
        {
            _mongoP4Context = mongoP4Context;
        }

        public RawCall GetResponse(string callId)
        {
            var filter = Builders<RawCall>.Filter.Eq(r => r._id, callId);

            return _mongoP4Context.OldRawCalls.Find(filter).SingleAsync().Result;
        }

        public List<RawCall> GetResponses(int take, DateTime dateFrom, DateTime dateTo)
        {
            var rc = _mongoP4Context.OldRawCalls.AsQueryable();

            var result =
                rc
                    .Where(r => r.RequestCreatedOn >= dateFrom && r.RequestCreatedOn <= dateTo && r.Action == "P4CollectedDataBatchResultRequestEnvelope")
                    .OrderBy(r => r.RequestCreatedOn)
                    .Take(take)
                    .ToListAsync();

            return result.Result;
        }

        public List<RawCall> GetRequests(int take, DateTime dateFrom, DateTime dateTo)
        {
            var rc = _mongoP4Context.OldRawCalls.AsQueryable();

            var result =
                rc
                    .Where(r => r.RequestCreatedOn >= dateFrom && r.RequestCreatedOn <= dateTo && r.Action == "P4CollectedDataBatchRequestEnvelope")
                    .OrderBy(r => r.RequestCreatedOn)
                    .Take(take)
                    .ToListAsync();

            return result.Result;
        }

        public List<RawCall> GetAll(int take, DateTime dateFrom, DateTime dateTo)
        {
            var rc = _mongoP4Context.OldRawCalls.AsQueryable();

            var result =
                rc
                    .Where(r => r.RequestCreatedOn >= dateFrom && r.RequestCreatedOn <= dateTo)
                    .OrderBy(r => r.RequestCreatedOn)
                    .Take(take)
                    .ToListAsync();

            return result.Result;
        }


        public async Task RegisterRequest(Guid callId, string reqXml, string action, string goId, int mpsRequested)
        {
            var rawCall = new RawCall
            {
                _id = callId.ToString(),
                Action = action,
                RequestCreatedOn = DateTime.Now,
                RequestXML = reqXml,
                MpsRequested = mpsRequested,
                GoId = goId
            };

            await _mongoP4Context.OldRawCalls.InsertOneAsync(rawCall);
        }



        public async Task<UpdateResult> RegisterResponse(Guid callId, string resXml, string status)
        {
            var filter = Builders<RawCall>.Filter
                .Eq(t => t._id, callId.ToString());

            var update = Builders<RawCall>.Update
                .Set(t => t.ResponseXML, resXml)
                .Set(t => t.ResponseStatus, status)
                .CurrentDate(t => t.ResponseCreatedOn);

            return await _mongoP4Context.OldRawCalls.UpdateOneAsync(filter, update);
        }

        public async Task<UpdateResult> RegisterResponseNumbers(Guid callId, int mpsResponded, int mpsRejected)
        {
            var filter = Builders<RawCall>.Filter
                .Eq(t => t._id, callId.ToString());

            var update = Builders<RawCall>.Update
                .Set(t => t.ResponseStatus, "Ok")
                .Set(t => t.MpsResponded, mpsResponded)
                .Set(t => t.MpsRejected, mpsRejected);

            return await _mongoP4Context.OldRawCalls.UpdateOneAsync(filter, update);
        }

        public async Task<UpdateResult> RegisterResponseExeption(Guid callId, Exception e)
        {
            var filter = Builders<RawCall>.Filter
                .Eq(t => t._id, callId.ToString());

            var update = Builders<RawCall>.Update
                .Set(t => t.ResponseStatus, "Exception. Details: " + e);

            return await _mongoP4Context.OldRawCalls.UpdateOneAsync(filter, update);
        }
    }

    [BsonIgnoreExtraElements]
    public class RawCall
    {
        [BsonId]
        public string _id { get; set; }

        public string Action { get; set; }

        public string RequestXML { get; set; }
        public DateTime RequestCreatedOn { get; set; }
        public int? MpsRequested { get; set; }

        public string ResponseXML { get; set; }
        public DateTime ResponseCreatedOn { get; set; }
        public string ResponseStatus { get; set; }
        public int? MpsResponded { get; set; }
        public int? MpsRejected { get; set; }

        public string GoId { get; set; }
    }
}