using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using UM.P4.CoreTypes;
using UM.P4.Mongo.Contexts;
using UM.P4.Mongo.Models;
using UM.P4.Mongo.Models.P4;
using UM.P4.Mongo.Models.Requests;

namespace UM.P4.Mongo
{
    public class RequestsRepository<T>
        where T : RequestBase, new()
    {
        private readonly IMongoRequestContext<T> _mongoRequestContext;

        public RequestsRepository(IMongoRequestContext<T> mongoRequestContext)
        {
            _mongoRequestContext = mongoRequestContext;
        }

        public List<T> GetLast(string ean, int take)
        {
            var query =
                from e in _mongoRequestContext.Request.AsQueryable()
                where e._id.StartsWith(ean)
                select e;

            //if (status.HasValue)
            //{
            //    query = query.Where(m => m.Status == status);
            //}
            return query.OrderByDescending(t => t._id).Take(take).ToList();
        }

        public List<T> GetRejections(string ean, string rejectionCode, int take = 50)
        {
            var query =
                from e in _mongoRequestContext.Request.AsQueryable()
                where 1 == 1
                && e._id.StartsWith(ean)
                && e.ResponseLast.Status == EdsnResponseCallStatus.Rejected
                && e.ResponseLast.RejectionCode == rejectionCode
                orderby e._id descending
                select e;

            return query.Take(take).ToList();
        }
        public T GetFirstRejection(string ean, string rejectionCode)
        {
            var query =
                from e in _mongoRequestContext.Request.AsQueryable()
                where 1 == 1
                && e._id.StartsWith(ean)
                && e.CreatedOn > new DateTime(2018, 1, 1)
                && e.ResponseLast.Status == EdsnResponseCallStatus.Rejected
                && e.ResponseLast.RejectionCode == rejectionCode
                orderby e._id ascending
                select e;

            return query.Take(1).FirstOrDefault();
        }

        public List<T> GetRejectedByCodeAndEan(string ean, string rejectionCode, int daysInHistory)
        {
            var historyLimit = DateTime.Now.Date.AddDays(-daysInHistory);

            //var dateLimit = new DateTime(2020, 12, 4);
            var query =
                _mongoRequestContext.Request.AsQueryable()
                .Where(e => 1 == 1
                && e.ResponseLast.Status == EdsnResponseCallStatus.Rejected
                && e.ResponseLast.CreatedOn >= historyLimit
                && e.ResponseLast.RejectionCode == rejectionCode
                //&& e.CreatedOn >= dateLimit
                );

            if (ean != null)
            {
                query = query.Where(e => e._id.StartsWith(ean));
            }

            // here we need to add new request?
            return query.ToList();
        }

        public List<T> GetByStatus(EdsnRequestCallStatus status)
        {
            var dateLimit = new DateTime(2021, 06, 05);
            var query =
                _mongoRequestContext.Request.AsQueryable().Where(e => e.RequestLast.Status == status && e.CreatedOn >= dateLimit);

            return query.ToList();
        }

        public List<T> GetInvalid()
        {
            var dateLimit = DateTime.Now.AddDays(-10);
            var query =
                _mongoRequestContext.Request.AsQueryable().Where(e => e.ResponseLast.Status == EdsnResponseCallStatus.Invalid && e.CreatedOn >= dateLimit);

            return query.ToList();
        }

        public List<T> GetNotAnswered(int daysInThePast)
        {
            var dateFrom = DateTime.Today.AddDays(-daysInThePast);
            var dateTo = DateTime.Today.AddDays(-2);
            var query = _mongoRequestContext.Request.AsQueryable()
                .Where(e => e._id.StartsWith("871717710000453919") && e.RequestLast.CreatedOn >= dateFrom && e.RequestLast.CreatedOn <= dateTo && e.ResponseLast == null);

            return query.ToList();
        }

        public async Task<T> Get(string ean, DateTime queryDate)
        {
            return await Get(RequestBase.BuildId(ean, queryDate));
        }

        public List<T> Get(string ean, List<DateTime> queryDates)
        {
            var ids = queryDates.Select(qd => RequestBase.BuildId(ean, qd)).ToList();

            return Get(ids);
        }

        public async Task<T> Get(string id)
        {
            var builder = Builders<T>.Filter;
            var filter = builder.Eq(t => t._id, id);

            var task = await _mongoRequestContext.Request.Find(filter).SingleOrDefaultAsync();

            return task;
        }

        public List<T> Get(List<string> ids)
        {
            var builder = Builders<T>.Filter;
            var filter = builder.In(t => t._id, ids);

            var task = _mongoRequestContext.Request.Find(filter).ToListAsync();

            return task.Result;
        }

        public T InsertNewRequest(T dr)
        {
            //TODO: ��������� �������!
            BaseMongoOps.InsertOneAsync(_mongoRequestContext.Request, dr).Wait();

            return dr;
        }

        public Task<List<T>> GetRequestsToSend()
        {
            var builder = Builders<T>.Filter;
            var filter = builder.Eq(t => t.RequestLast.Status, EdsnRequestCallStatus.ToSubmit);

            return _mongoRequestContext.Request.Find(filter).ToListAsync();
        }

        public void Replace(T entity)
        {
            var modifyAuditable = entity as IModifyAuditable;
            if (modifyAuditable != null)
            {
                modifyAuditable.ModifiedOn = DateTime.Now;
            }

            var filter = Builders<T>.Filter.Eq(t => t._id, entity._id);

            var task = _mongoRequestContext.Request.ReplaceOneAsync(filter, entity, new UpdateOptions { IsUpsert = true });
            task.Wait();
        }

        public void ReplaceMany(List<T> entities)
        {
            var updates = new List<WriteModel<T>>();

            foreach (var entity in entities)
            {
                if (entity is IModifyAuditable modifyAuditable)
                {
                    modifyAuditable.ModifiedOn = DateTime.Now;
                }

                updates.Add(new ReplaceOneModel<T>(entity._id, entity));
            }
            var task = _mongoRequestContext.Request.BulkWriteAsync(updates, new BulkWriteOptions { IsOrdered = false });
            task.GetAwaiter().GetResult();

        }


        //public Task<UpdateResult> RegisterResponseCall(string ean, DateTime queryDate, EdsnResponseCall mes)
        //{
        //    var builder = Builders<T>.Filter;
        //    var filter = builder.Eq(t => t._id, RequestBase.BuildId(ean, queryDate));

        //    var update = Builders<T>.Update
        //        .Set(t => t.ResponseLast, mes)
        //        .Push(t => t.ResponseHistory, mes);

        //    return BaseMongoOps.UpdateOneAsync(_mongoRequestContext.Request, filter, update, new UpdateOptions { IsUpsert = true });
        //}

        public Task<UpdateResult> RegisterRequestSent(List<string> ids, ObjectId callId)
        {
            var builder = Builders<T>.Filter;
            var filter = builder.In(t => t._id, ids);

            var update = Builders<T>.Update
                .Set(t => t.RequestLast.Status, EdsnRequestCallStatus.Submitted)
                .Set(t => t.RequestLast.ModifiedOn, DateTime.Now)
                .Set(t => t.RequestLast.CallId, callId);

            return BaseMongoOps.UpdateManyAsync(_mongoRequestContext.Request, filter, update, new UpdateOptions { IsUpsert = true });
        }

        public Task<UpdateResult> RegisterSendFailed(List<string> ids, ObjectId callId)
        {
            var builder = Builders<T>.Filter;
            var filter = builder.In(t => t._id, ids);

            var update = Builders<T>.Update
                .Set(t => t.RequestLast.Status, EdsnRequestCallStatus.SubmitFailed)
                .Set(t => t.RequestLast.ModifiedOn, DateTime.Now)
                .Set(t => t.RequestLast.CallId, callId);

            return BaseMongoOps.UpdateManyAsync(_mongoRequestContext.Request, filter, update, new UpdateOptions { IsUpsert = true });
        }

        public Task<UpdateResult> ToRequest(string id)
        {
            var builder = Builders<T>.Filter;
            var filter = builder.Eq(t => t._id, id);

            var update = Builders<T>.Update
                .Set(t => t.RequestLast.Status, EdsnRequestCallStatus.ToSubmit);


            return BaseMongoOps.UpdateManyAsync(_mongoRequestContext.Request, filter, update);
        }

        public List<string> GetRequestDates(string ean, List<DateTime> datesShouldBe)
        {
            var ids = datesShouldBe.Select(qd => P4Readings.BuildId(ean, qd)).ToList();

            return _mongoRequestContext.Request.AsQueryable().Where(m => ids.Contains(m._id)).Select(q => q._id).ToList();
        }

        public Task<UpdateResult> ToRequest(string ean, DateTime queryDate)
        {
            return ToRequest(RequestBase.BuildId(ean, queryDate));
        }
    }
}