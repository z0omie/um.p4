using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using UM.P4.Mongo.Contexts;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.Mongo
{
    public class RawCallTypedRepository<T> : IRawCallRepo
         where T : RawCallBase
    {
        private static List<string> GetDataFor(XDocument docResp, string qReason, string firstNextName)
        {
            return docResp.Descendants().Where(n => n.Name.LocalName == "QueryReason" && n.Value == qReason && n.ElementsAfterSelf().First().Name.LocalName == firstNextName).Select(GetData).ToList();
        }

        private static string GetData(XElement i)
        {
            return $"{((XElement)i.PreviousNode.PreviousNode.PreviousNode).Value}:{((XElement)i.PreviousNode).Value.Replace("-", "")}";
        }
        protected readonly IMongoRawCallContext<T> _mongoContext;
        public RawCallTypedRepository(IMongoRawCallContext<T> mongoContext)
        {
            _mongoContext = mongoContext;
        }

        public async Task<T> GetResponseContent(ObjectId callId)
        {
            return await _mongoContext.RawCall.AsQueryable().SingleOrDefaultAsync(f => f._id == callId);
        }

        public async void SubmitFailed(ObjectId callId, string submitErrorDetails)
        {
            var builder = Builders<T>.Filter;
            var filter = builder.Eq(t => t._id, callId);

            var update = Builders<T>.Update
                .Set(t => t.Status, RawCallStatus.SubmitFailed)
                .Set(t => t.SubmitErrorDetails, submitErrorDetails);

            await BaseMongoOps.UpdateManyAsync(_mongoContext.RawCall, filter, update, new UpdateOptions { IsUpsert = false });
        }

        public async Task<UpdateResult> AttachResponse(ObjectId callId, string responseContent, bool isFault)
        {
            var builder = Builders<T>.Filter;
            var filter = builder.Eq(t => t._id, callId);

            UpdateDefinition<T> update = Builders<T>.Update
                .Set(t => t.Status, isFault ? RawCallStatus.Fault : RawCallStatus.ResponseReceived)
                .Set(t => t.ResponseXML, responseContent)
                .Set(t => t.ResponseCreatedOn, DateTime.Now);

            var updateresult = await BaseMongoOps.UpdateManyAsync(_mongoContext.RawCall, filter, update,
                new UpdateOptions { IsUpsert = false });

            if (isFault || typeof(T) != typeof(ResponseRawCall))
            {
                return updateresult;
            }

            var docResp = XDocument.Parse(responseContent);

            var okDayEans = GetDataFor(docResp, "DAY", "P4EnergyMeter");
            var okIntEans = GetDataFor(docResp, "INT", "P4EnergyMeter");
            var okRcyEans = GetDataFor(docResp, "RCY", "P4EnergyMeter");

            var rejDayEans = GetDataFor(docResp, "DAY", "P4Rejection");
            var rejIntEans = GetDataFor(docResp, "INT", "P4Rejection");
            var rejRcyEans = GetDataFor(docResp, "RCY", "P4Rejection");

            UpdateDefinition<T> update2 = Builders<ResponseRawCall>.Update
                .Set(t => t.OkDayEans, okDayEans)
                .Set(t => t.OkIntEans, okIntEans)
                .Set(t => t.OkRcyEans, okRcyEans)
                .Set(t => t.RejDayEans, rejDayEans)
                .Set(t => t.RejIntEans, rejIntEans)
                .Set(t => t.RejRcyEans, rejRcyEans) as UpdateDefinition<T>;


            var result = await BaseMongoOps.UpdateManyAsync(_mongoContext.RawCall, filter, update2,
                new UpdateOptions { IsUpsert = false });

            // also upload to a blob if no fault and not empty
            // the code bellow saves to blob and then message is pushed to AzureServiceBus that blob is saved, so someone can start processing the blob.
            // FTM disables. Cause not used.

            //if (!isFault && (okDayEans.Any() || okIntEans.Any() || okRcyEans.Any() || rejDayEans.Any() || rejIntEans.Any() || rejRcyEans.Any()))
            //{
            //    try
            //    {
            //        var storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=edsnxmls;AccountKey=kTY0G/sy/mmbgi4EsnRPUBUItjhzVixKcZcP2+mFpJUpfKZx9uAvLEvXglamV71fYnNZKkza9L2J8CJ+Ff69ag==;EndpointSuffix=core.windows.net";
            //        CloudStorageAccount storageAccount;
            //        if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            //        {
            //            CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();
            //            //CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference($"incomingxmls-{DateTime.UtcNow.ToString("yyyy")}-{DateTime.UtcNow.ToString("MM")}");
            //            CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference("incomingxmls");
            //            cloudBlobContainer.CreateIfNotExists();

            //            var fileName = $"{callId}_{Guid.NewGuid()}.xml";
            //            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);

            //            var options = new BlobRequestOptions() { ServerTimeout = TimeSpan.FromMinutes(10) };

            //            using (var stream = new MemoryStream(Encoding.Default.GetBytes(responseContent), false))
            //            {
            //                cloudBlockBlob.UploadFromStream(stream, null, options);
            //            }
            //        }
            //    }
            //    catch (Exception e)
            //    {
            //        // nomatter what - cotinue
            //    }


            return result;
        }

        public Task<IAsyncCursor<T>> GetCursor(int take, DateTime dateFrom, DateTime dateTo)
        {
            var filter = Builders<T>.Filter.And(Builders<T>.Filter.Gte(t => t.RequestCreatedOn, dateFrom), Builders<T>.Filter.Lte(t => t.RequestCreatedOn, dateTo));

            return _mongoContext.RawCall.FindAsync(filter);
        }

        public List<T> Get(int take, DateTime dateFrom, DateTime dateTo)
        {
            var rc = _mongoContext.RawCall.AsQueryable().OfType<T>();

            var result =
                    rc
                    .Where(r => r.RequestCreatedOn >= dateFrom && r.RequestCreatedOn <= dateTo)
                    .Take(take)
                    .ToListAsync();

            return result.Result;
        }

        public List<T> Get(int take, string idFrom, string idTo)
        {
            var rc = _mongoContext.RawCall.AsQueryable().OfType<T>();

            var result =
                    rc
                    .Where(r => r._id >= ObjectId.Parse(idFrom) && r._id <= ObjectId.Parse(idTo))
                    .Take(take)
                    .ToListAsync();

            return result.Result;
        }

        public T Get(string id)
        {
            var rc = _mongoContext.RawCall.AsQueryable().OfType<T>();

            var result =
                    rc
                    .SingleOrDefaultAsync(r => r._id == new ObjectId(id));

            return result.Result;
        }

        public List<T> Get(List<string> ids)
        {
            var objectIds = ids.Select(t => new ObjectId(t)).ToList();

            var rc = _mongoContext.RawCall.AsQueryable().OfType<T>();

            var result =
                    rc
                    .Where(r => objectIds.Contains(r._id))
                    .ToListAsync();

            return result.Result;
        }

        public async Task InsertOneAsync(T request)
        {
            await _mongoContext.RawCall.InsertOneAsync(request);
        }

        public async Task Replace(T request)
        {
            var filter = Builders<T>.Filter.Eq(t => t._id, request._id);

            await _mongoContext.RawCall.ReplaceOneAsync(filter, request);
        }
    }


    public interface IRawCallRepo
    {
        void SubmitFailed(ObjectId callId, string submitErrorDetails);
    }
}