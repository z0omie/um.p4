using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using UM.P4.CoreTypes;
using UM.P4.Mongo.Contexts;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.Mongo
{
    public class MeteringPointRepository
    {
        private readonly IMongoP4Context _mongoP4Context;

        public MeteringPointRepository(IMongoP4Context mongoP4Context)
        {
            _mongoP4Context = mongoP4Context;
        }

        public Task<List<MeteringPoint>> GetMPsByStatus(MPState status)
        {
            var builder = Builders<MeteringPoint>.Filter;
            var filter = builder.Eq(mp => mp.State, status);
            //var filter = builder.Eq(mp => mp.EanId, "871690930000280979");
            

            return _mongoP4Context.MeteringPoints.Find(filter).ToListAsync();
        }

        public Task<MeteringPoint> GetMP(string ean)
        {
            return _mongoP4Context.MeteringPoints.AsQueryable().SingleOrDefaultAsync(mp => mp.EanId == ean);
        }

        public Task<List<MeteringPoint>> GetMPsForGO(string goEan)
        {
            IMongoQueryable<MeteringPoint> mps = _mongoP4Context.MeteringPoints.AsQueryable()
                .Where(mp => mp.GridOperatorId == goEan);

            return mps.ToListAsync();
        }

        public Task InsertMP(MeteringPoint mp)
        {
            return BaseMongoOps.InsertOneAsync(_mongoP4Context.MeteringPoints, mp);
        }

        public Task<ReplaceOneResult> ReplaceMP(MeteringPoint mp)
        {
            var builder = Builders<MeteringPoint>.Filter;
            var filter = builder.Eq(m => m.EanId, mp.EanId);

            return _mongoP4Context.MeteringPoints.ReplaceOneAsync(filter, mp);
        }

        public Task<UpdateResult> AddMeter(string eanId, EnergyMeter meter)
        {
            var builder = Builders<MeteringPoint>.Filter;
            var filter = 
                builder.Eq(t => t.EanId, eanId) 
                & builder.Not(
                    builder.ElemMatch(
                        t => t.Meters, 
                        Builders<EnergyMeter>.Filter.Eq(e => e.Id,meter.Id)));

            var update = Builders<MeteringPoint>.Update.AddToSet(aa => aa.Meters, meter);

            return BaseMongoOps.UpdateOneAsync(_mongoP4Context.MeteringPoints, filter, update);
        }

        public Task StopMP(string ean)
        {
            var builder = Builders<MeteringPoint>.Filter;
            var filter = builder.Eq(t => t.EanId, ean);

            var stateLatest = new StateHistory { State = MPState.Stopped, CreatedOn = DateTime.Now };

            var update = Builders<MeteringPoint>.Update
                .Set(aa => aa.State, MPState.Stopped)
                .Set(aa => aa.StatusUpdatedOn, DateTime.Now)
                .Set(aa => aa.MonitoredTill, DateTime.Today)
                .Set(aa => aa.StateLatest, stateLatest)
                .Push(aa => aa.StateHistory, stateLatest);

            return BaseMongoOps.UpdateOneAsync(_mongoP4Context.MeteringPoints, filter, update);
        }

        //TODO: make one method for Start Stop Pause
        public Task PauseMP(string ean)
        {
            var builder = Builders<MeteringPoint>.Filter;
            var filter = builder.Eq(t => t.EanId, ean);

            var stateLatest = new StateHistory { State = MPState.Paused, CreatedOn = DateTime.Now };

            var update = Builders<MeteringPoint>.Update
                .Set(aa => aa.State, MPState.Paused)
                .Set(aa => aa.StatusUpdatedOn, DateTime.Now)
                .Set(aa => aa.StateLatest, stateLatest)
                .Push(aa => aa.StateHistory, stateLatest);

            return BaseMongoOps.UpdateOneAsync(_mongoP4Context.MeteringPoints, filter, update);
        }
        public Task StartMP(string ean, DateTime monitoredFrom)
        {
            var builder = Builders<MeteringPoint>.Filter;
            var filter = builder.Eq(t => t.EanId, ean);

            var stateLatest = new StateHistory { State = MPState.Started, CreatedOn = DateTime.Now };

            var update = Builders<MeteringPoint>.Update
                .Set(aa => aa.State, MPState.Started)
                .Set(aa => aa.MonitoredFrom, monitoredFrom)
                .Set(aa => aa.StateLatest, stateLatest)
                .Push(aa => aa.StateHistory, stateLatest);

            return BaseMongoOps.UpdateOneAsync(_mongoP4Context.MeteringPoints, filter, update);
        }
    }
}