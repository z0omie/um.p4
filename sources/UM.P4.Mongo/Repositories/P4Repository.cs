﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using UM.P4.Mongo.Contexts;
using UM.P4.Mongo.Models.P4;

namespace UM.P4.Mongo
{
	public class P4Repository<T>
		where T : P4Readings
	{
		private readonly IMongoCollection<T> _collection;

		public P4Repository(IMongoP4Context mongoContext)
		{
			_collection = typeof(T) == typeof(P4DayReadings)
				? (IMongoCollection<T>)mongoContext.P4DayReadings
				: (IMongoCollection<T>)mongoContext.P4DetailReadings;
		}

		#region getFull

		public Task<T> Get(string ean, DateTime queryDate)
		{
			return Get(P4Readings.BuildId(ean, queryDate));
		}

		public Task<T> Get(string id)
		{
			var q = _collection.AsQueryable();

			var task = q.Where(r => r.ID == id).SingleOrDefaultAsync();

			return task;
		}

		#endregion

		public void ReplaceP4Readings(T entity)
		{
			if (entity.ID == null)
			{
				entity.ID = P4Readings.BuildId(entity.Ean, entity.QueryDate);
			}

			var filter = Builders<T>.Filter.Eq(t => t.ID, entity.ID);

			var task = _collection.ReplaceOneAsync(filter, entity, new UpdateOptions { IsUpsert = true });
			task.Wait();
		}

		public Task<T> GetReadings(string ean, DateTime dateTime)
		{
			var t = _collection.AsQueryable();
			var ra = t.Where(m => m.ID == P4Readings.BuildId(ean, dateTime)).SingleAsync();

			return ra;
		}

	    public Task<IAsyncCursor<T>> GetCursor()
	    {
	        var filter = new BsonDocument();

            return _collection.FindAsync(filter);
        }

		public async Task<DateTime?> GetFirstReadingDate(string ean)
		{
			var t = _collection.AsQueryable();
			var ra = await t.Where(m => m.ID.StartsWith(ean)).OrderBy(m => m.ID).Select(m => m.ID).Take(1).SingleOrDefaultAsync();

			return P4Readings.GetQueryDate(ra);
		}

		public async Task<List<T>> GetReadings(string ean, DateTime queryDateFrom, DateTime queryDateTo)
		{
			var queryDates = Enumerable.Range(0, 1 + queryDateTo.Subtract(queryDateFrom).Days)
				.Select(offset => queryDateFrom.AddDays(offset))
				.ToList();

			var ids = queryDates.Select(qd => P4Readings.BuildId(ean, qd)).ToList();

			return await GetReadings(ids).ToListAsync();
		}
		public async Task<List<T>> GetReadings(DateTime queryDate)
		{
			var id = P4Readings.BuildId("", queryDate);
			var t = _collection.AsQueryable();

			return await t.Where(m => m.ID.EndsWith(id)).ToListAsync();
		}

		//public async Task<DateTime?> GetFirstDayReading(string ean)
		//{
		//    var res =
		//        await _mongoContext.P4Readings.AsQueryable()
		//            .Where(g => g.ID.StartsWith(ean) && g.DayReadings != null)
		//            .Select(g => g.ID)
		//            .OrderBy(g => g)
		//            .FirstOrDefaultAsync();

		//    if (res == null)
		//    {
		//        return null;
		//    }

		//    return P4Readings.GetQueryDate(res);
		//}


		public async Task<T> GetLastReading(string ean)
		{
			var res = await _collection.AsQueryable()
						.Where(g => g.ID.StartsWith(ean))
						.OrderByDescending(g => g.ID)
						.FirstOrDefaultAsync();

			return res;
		}
		public async Task<List<T>> GetReadings(string ean, IEnumerable<DateTime> queryDates)
		{
			var ids = queryDates.Select(qd => P4Readings.BuildId(ean, qd)).ToList();

			return await GetReadings(ids).ToListAsync();
		}

		private IMongoQueryable<T> GetReadings(List<string> ids)
		{
			return _collection.AsQueryable().Where(m => ids.Contains(m.ID));
		}
	}
}