﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MongoDB.Driver;
using UM.P4.Mongo.Contexts;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.Mongo
{
    public class GORepository
    {
        private readonly IMongoP4Context _mongoP4Context;

        public GORepository(IMongoP4Context mongoP4Context)
        {
            _mongoP4Context = mongoP4Context;
        }

        public Task AddGO(string name, string ean)
        {
            var builder = Builders<GridOperator>.Filter;
            var filter = builder.Eq(t => t.Name, name);

            var update = Builders<GridOperator>.Update.AddToSet(t => t.Eans, ean);

            return BaseMongoOps.UpdateOneAsync(_mongoP4Context.GridOperators, filter, update, new UpdateOptions { IsUpsert = true });
        }

        public Task<UpdateResult> NeedSendODAFile(string goEan, bool value)
        {
            var builder = Builders<GridOperator>.Filter;
            var filter = builder.AnyEq(t => t.Eans, goEan);

            var update = Builders<GridOperator>.Update.Set(t => t.NeedToSendODAFile, value);

            return BaseMongoOps.UpdateOneAsync(_mongoP4Context.GridOperators, filter, update);
        }


        public Task<UpdateResult> NeedSendODAFileByName(string goName, bool value)
        {
            var builder = Builders<GridOperator>.Filter;
            var filter = builder.Eq(t => t.Name, goName);

            var update = Builders<GridOperator>.Update.Set(t => t.NeedToSendODAFile, value);

            return BaseMongoOps.UpdateOneAsync(_mongoP4Context.GridOperators, filter, update);
        }

        public Task<List<GridOperator>> GetAllGOs()
        {
            return _mongoP4Context.GridOperators.Find(Builders<GridOperator>.Filter.Empty).ToListAsync();
        }


        public GridOperator GetGO(string name)
        {
            var builder = Builders<GridOperator>.Filter;
            var filter = builder.Eq(f => f.Name, name);

            return _mongoP4Context.GridOperators.Find(filter).SingleAsync().Result;
        }
    }
}