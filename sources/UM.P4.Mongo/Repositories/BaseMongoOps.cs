﻿
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using UM.P4.Mongo.Models;

namespace UM.P4.Mongo
{
    public class BaseMongoOps
    {
        public static Task<UpdateResult> UpdateManyAsync<T>(IMongoCollection<T> collection, FilterDefinition<T> filter, UpdateDefinition<T> update, UpdateOptions updateOptions = null)
        {
            if (typeof(IModifyAuditable).IsAssignableFrom(typeof(T)))
            {
                IModifyAuditable g = null;

                var extraUpdate = Builders<T>.Update.CurrentDate(GetVariableName(() => g.ModifiedOn));

                update = Builders<T>.Update.Combine(update, extraUpdate);
            }

            //in case insert
            if (typeof(ICreationAuditable).IsAssignableFrom(typeof(T)))
            {
                ICreationAuditable g = null;

                update = update.SetOnInsert(GetVariableName(() => g.CreatedOn), DateTime.Now);
            }

            return collection.UpdateManyAsync(filter, update, updateOptions);
        }

        public static Task<UpdateResult> UpdateOneAsync<T>(IMongoCollection<T> collection, FilterDefinition<T> filter, UpdateDefinition<T> update, UpdateOptions updateOptions = null)
        {
            if (typeof(IModifyAuditable).IsAssignableFrom(typeof(T)))
            {
                IModifyAuditable g = null;

                update = update.CurrentDate(GetVariableName(() => g.ModifiedOn));
            }

            //in case insert
            if (typeof(ICreationAuditable).IsAssignableFrom(typeof(T)))
            {
                ICreationAuditable g = null;

                update = update.SetOnInsert(GetVariableName(() => g.CreatedOn), DateTime.Now);
            }

            return collection.UpdateOneAsync(filter, update, updateOptions);
        }


        public static Task InsertOneAsync<T>(
            IMongoCollection<T> collection,
            T objectToInsert)
        {
            if (typeof(ICreationAuditable).IsAssignableFrom(typeof(T)))
            {
                ((ICreationAuditable)objectToInsert).CreatedOn = DateTime.Now;
            }

            return collection.InsertOneAsync(objectToInsert);
        }



        private static string GetVariableName<TG>(Expression<Func<TG>> expr)
        {
            var body = (MemberExpression)expr.Body;
            return body.Member.Name;
        }
    }
}