﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using UM.P4.CoreTypes;

namespace UM.P4.Mongo.Models.P4
{
    public class UsageMax
    {
        public double Usage;
        public DateTime From;
        public DateTime To;
    }

    [BsonIgnoreExtraElements]
    public class P4DetailReadings : P4Readings
    {
        public P4DetailReadings(string ean, DateTime queryDate) : base(ean, queryDate)
        { }
        public List<DetailReading> Readings = null;

        [BsonElement("uMax")]
        public Dictionary<RegisterId, UsageMax> UsageMax;

        public class DetailReading
        {
            public DateTime DateTime;

            public Dictionary<RegisterId, double> Value;

            public DetailReading(DateTime dateTime, Dictionary<RegisterId, double> value)
            {
                DateTime = dateTime;
                Value = value;
            }

            //public static DetailUsage operator -(DetailReading x, DetailReading y)
            //{
            //    if (x.Value.Count != y.Value.Count)
            //    {
            //        throw new Exception("wtf!");
            //    }

            //    if (x.DateTime == y.DateTime)
            //    {
            //        throw new Exception("wtf!");
            //    }

            //    var start = x.DateTime < y.DateTime ? x : y;
            //    var end = x.DateTime < y.DateTime ? y : x;

            //    var usage = new DetailUsage
            //    (
            //        start.DateTime,
            //        end.DateTime,
            //        new Dictionary<RegisterId, double>()
            //    );

            //    foreach (var startReading in start.Value)
            //    {
            //        usage.Value.Add(
            //            key: startReading.Key,
            //            value: Math.Round(end.Value[startReading.Key] - startReading.Value, 3));
            //    }

            //    return usage;
            //}
        }
    }
}