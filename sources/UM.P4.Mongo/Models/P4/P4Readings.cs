﻿using System;
using System.Globalization;
using MongoDB.Bson.Serialization.Attributes;

namespace UM.P4.Mongo.Models.P4
{
    [BsonIgnoreExtraElements]
    public abstract class P4Readings : ICreationAuditable
    {
        [BsonId]
        public string ID;
        
        public DateTime CreatedOn { get; set; }

        protected P4Readings(string ean, DateTime queryDate)
        {
            ID = BuildId(ean, queryDate);
            CreatedOn = DateTime.Now;
        }

        //public void ReCalculateDayUsage()
        //{
        //    DayUsage = DetailUsage
        //        .SelectMany(d => d.Value)
        //        .GroupBy(f => f.Key)
        //        .ToDictionary(a => a.Key, a => Math.Round(a.Sum(d => d.Value), 3));
        //}

        //public void ReCalculateDetailUsage(int? peakHourEndsBy)
        //{
        //    DetailUsage = new List<DetailUsage>();
        //    for (var i = 1; i < DetailReadings.Count; i++)
        //    {
        //        DetailUsage.Add(new DetailUsage(DetailReadings[i - 1], DetailReadings[i], peakHourEndsBy));
        //    }
        //}

        //public static DetailReading GetDetailReadings(DetailReading dr, int? peakHourEndsWith)
        //{
        //    // if gas
        //    if (!dr.Value.ContainsKey(RegisterId.R280)) return dr;

        //    if (!peakHourEndsWith.HasValue)
        //    {
        //        throw new Exception("cannot extract DetailRequests. peakHourEndsWith is null");
        //    }

        //    var isPeak = C.IsPeakDate(dr.DateTime, peakHourEndsWith.Value);
        //    return new DetailReading
        //    (
        //        dr.DateTime,
        //        new Dictionary<RegisterId, double>
        //        {
        //            {RegisterId.R181, isPeak ? 0 : dr.Value[RegisterId.R180]},
        //            {RegisterId.R182, isPeak ? dr.Value[RegisterId.R180] : 0},
        //            {RegisterId.R281, isPeak ? 0 : dr.Value[RegisterId.R280]},
        //            {RegisterId.R282, isPeak ? dr.Value[RegisterId.R280] : 0}
        //        }
        //    );
        //}

        public string Ean => ID.Substring(0, 18);

        public DateTime QueryDate => GetQueryDate(ID);

        public static DateTime GetQueryDate(string id)
        {
            return DateTime.ParseExact(id.Substring(19), DateFromat, CultureInfo.InvariantCulture);
        }

        public string GetPrevId()
        {
            return BuildId(Ean, QueryDate.AddDays(-1));
        }

        public string GetNextId()
        {
            return BuildId(Ean, QueryDate.AddDays(1));
        }

        public static string BuildId(string ean, string date)
        {
            return $"{ean}:{date}";
        }

        public static string BuildId(string ean, DateTime date)
        {
            return BuildId(ean, date.ToString(DateFromat));
        }

        private const string DateFromat = "yyyyMMdd";
    }
}