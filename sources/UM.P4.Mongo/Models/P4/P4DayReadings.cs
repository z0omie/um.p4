﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using UM.P4.CoreTypes;

namespace UM.P4.Mongo.Models.P4
{
    [BsonIgnoreExtraElements]
    public class P4DayReadings : P4Readings
    {
        public P4DayReadings(string ean, DateTime queryDate) : base(ean, queryDate)
        { }

        [BsonDictionaryOptions(DictionaryRepresentation.Document)]
        public Dictionary<RegisterId, double> Readings = null;
    }
}