﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using UM.P4.CoreTypes;

namespace UM.P4.Mongo.Models.Requests
{
    [BsonIgnoreExtraElements]
    public class EdsnResponseCall
    {
        public EdsnResponseCall(ObjectId callId, DateTime? createdOn = null)
        {
            CallId = callId;
            CreatedOn = createdOn ?? DateTime.Now;
            Status = EdsnResponseCallStatus.ToProcess;
            ModifiedOn = null;
        }
        public ObjectId CallId { get; private set; }
        public EdsnResponseCallStatus Status { get; set; }

        public string RejectionCode { get; set; }
        public string RejectionText { get; set; }
        public string ErrorDetails { get; set; }

        public DateTime CreatedOn { get; private set; }
        public DateTime? ModifiedOn { get; private set; }
    }
}