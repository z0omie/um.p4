using System;
using MongoDB.Bson;
using UM.P4.CoreTypes;

namespace UM.P4.Mongo.Models.Requests
{
    public class EdsnRequestCall
    {
        public EdsnRequestCall(DateTime? createdOn = null)
        {
            CallId = null;
            CreatedOn = createdOn ?? DateTime.Now;
            Status = EdsnRequestCallStatus.ToSubmit;
            ModifiedOn = null;
        }

        public ObjectId? CallId { get; private set; }
        public string FileName { get; private set; }

        public EdsnRequestCallStatus Status { get; private set; }

        public DateTime CreatedOn { get; private set; }
        public DateTime? ModifiedOn { get; private set; }
    }
}