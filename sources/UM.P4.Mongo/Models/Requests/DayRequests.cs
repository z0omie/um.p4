﻿using UM.P4.CoreTypes;

namespace UM.P4.Mongo.Models.Requests
{
    public class DayRequests : RequestBase
    {
        public DayRequestType DayRequestType { get; set; }
    }
}