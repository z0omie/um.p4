﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using UM.P4.CoreTypes;

namespace UM.P4.Mongo.Models.Requests
{
    [DebuggerDisplay("{" + nameof(_id) + "}")]
    public abstract class RequestBase : ICreationAuditable, IModifyAuditable
    {
        [BsonId]
        public string _id { get; set; }

        public string GridOperatorEan { get; set; }

        public EdsnRequestCall RequestLast { get; set; }
        public List<EdsnRequestCall> RequestHistory { get; set; }

        public EdsnResponseCall ResponseLast { get; set; }
        public List<EdsnResponseCall> ResponseHistory { get; set; }


        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }



        private const string DateFromat = "yyyyMMdd";

        public string Ean => _id.Substring(0, 18);

        public DateTime QueryDate => DateTime.ParseExact(_id.Substring(19), DateFromat, CultureInfo.InvariantCulture);

        public string GetPrevId()
        {
            return BuildId(Ean, QueryDate.AddDays(-1));
        }

        public string GetNextId()
        {
            return BuildId(Ean, QueryDate.AddDays(1));
        }

        public string GetLastYearId()
        {
            return BuildId(Ean, QueryDate.AddYears(-1));
        }

        public static string BuildId(string ean, string date)
        {
            return $"{ean}:{date}";
        }

        public static string BuildId(string ean, DateTime date)
        {
            return BuildId(ean, date.ToString(DateFromat));
        }

        public void AppendNewRequest(DateTime? createdOn = null)
        {
            if (RequestLast != null)
            {
                RequestHistory.Add(RequestLast);
            }

            var req = new EdsnRequestCall(createdOn);
            RequestLast = req;
        }
        
        public void AppendNewResponse(ObjectId callId, DateTime? createdOn = null)
        {
            if (ResponseLast != null)
            {
                ResponseHistory.Add(ResponseLast);
            }

            var resp = new EdsnResponseCall(callId, createdOn);
            ResponseLast = resp;
        }

        public void LogResponseException(Exception e)
        {
            ResponseLast.Status = EdsnResponseCallStatus.Error;
            ResponseLast.ErrorDetails = e.ToString();
        }
        public void LogResponseRejection(string rejectionCode, string rejectionText)
        {
            ResponseLast.Status = EdsnResponseCallStatus.Rejected;
            ResponseLast.RejectionCode = rejectionCode;
            ResponseLast.RejectionText = rejectionText;
        }
        public void LogResponseInvalid(string errorDetails)
        {
            ResponseLast.Status = EdsnResponseCallStatus.Invalid;
            ResponseLast.ErrorDetails = errorDetails;
        }

        public void LogResponseOk()
        {
            ResponseLast.Status = EdsnResponseCallStatus.Ok;
        }
    }
}