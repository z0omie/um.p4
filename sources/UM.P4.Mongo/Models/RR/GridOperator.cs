﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace UM.P4.Mongo.Models.RR
{
    public class GridOperator : ICreationAuditable, IModifyAuditable
    {
        [BsonId]
        public string Name;
        public List<string> Eans;

        public ODAEmailSettings ODACSVSettigns;
        
        public bool NeedToSendODAFile;

        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}