﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using MongoDB.Bson.Serialization.Attributes;
using UM.P4.CoreTypes;

namespace UM.P4.Mongo.Models.RR
{
    [DebuggerDisplay("{EanId}")]
    [BsonIgnoreExtraElements]
    public class MeteringPoint : ICreationAuditable, IModifyAuditable
    {
        [BsonId]
        public string EanId;

        public string City;
        public string Street;
        public int? BuildingNr;
        public string BuildingNrEx;
        public string ZipCode;

        public bool IsSpecialConnection;
        
        public string GridOperatorId;

        public MPState State;

        public List<StateHistory> StateHistory;
        public StateHistory StateLatest;

        public int? PeakHourEndsWith;

        public DateTime StatusUpdatedOn;

        public ProductType ProdType;

        public string ClientName;

        public List<EnergyMeter> Meters;

        public DateTime MonitoredFrom;
        public DateTime? MonitoredTill;

        public DateTime? InODAEmailOn;

        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }



    public class StateHistory
    {
        public MPState State;
        public DateTime CreatedOn;
    }
}