﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;

namespace UM.P4.Mongo.Models.RR
{
    public class ResponseRawCall : RawCallBase
    {
        public ResponseRawCall(ObjectId id, string requestContent) : base(id, requestContent, null)
        {
            OkIntEans = new List<string>();
            OkDayEans = new List<string>();
            OkRcyEans = new List<string>();
            RejDayEans = new List<string>();
            RejIntEans = new List<string>();
            RejRcyEans = new List<string>();
        }

        public ResponseRawCall(ObjectId callId, string requestContent, DateTime? requestCreatedOn = null) : base(callId, requestContent, requestCreatedOn)
        {
            OkIntEans = new List<string>();
            OkDayEans = new List<string>();
            OkRcyEans = new List<string>();
            RejDayEans = new List<string>();
            RejIntEans = new List<string>();
            RejRcyEans = new List<string>();
        }

        public List<string> OkIntEans { get; set; }
        public List<string> OkDayEans { get; set; }
        public List<string> OkRcyEans { get; set; }

        public List<string> RejIntEans { get; set; }
        public List<string> RejDayEans { get; set; }
        public List<string> RejRcyEans { get; set; }
    }
}