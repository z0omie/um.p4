﻿namespace UM.P4.Mongo.Models.RR
{
    public class ODAEmailSettings
    {
        public string CSVNameFormat;//"{{ean_oda}}{{date}}{{seqnumber}}.odar"
        public string CSVNameDateFormat;//{{ddMMyyyy}}
        public string CSVHeader; //"EAN ODA,EAN Aansluiting,Begindatum,Einddatum,Klantnaam";
        public string CSVLineFormat; //"{{ean_oda}},{{ean_connection}},{{mandate_start_date}},{{mandate_end_date}},{{client_name}}";
        public string CSVLineDateFormat; //"dd-MM-yyyy";
        public string EmailTo;
        public string EmailCc;
        public string EmailSubject;
        public string EmailBody;
    }
}