﻿using System;
using System.Linq;
using System.Xml.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace UM.P4.Mongo.Models.RR
{
    public enum RawCallStatus
    {
        ToSubmit,
        SubmitFailed,
        ResponseReceived,
        Fault
    }

    //[BsonDiscriminator(RootClass = true)]
    //[BsonKnownTypes(typeof(RequestRawCall), typeof(ResponseRawCall))]
    public abstract class RawCallBase
    {

        protected const string RejectionNode = "EDSN_SimpleRejection";
        protected const string MpNode = "P4MeteringPoint";
        protected const string ReceiverNode = "ReceiverID";
        private const string SoapFaultNode = "Fault";

        protected RawCallBase(ObjectId id, string requestContent, DateTime? requestCreatedOn = null)
        {
            _id = id;
            RequestXML = requestContent;
            RequestCreatedOn = requestCreatedOn ?? DateTime.UtcNow;
            Status = RawCallStatus.ToSubmit;

            GoId = DocReq.Descendants().Single(n => n.Name.LocalName == ReceiverNode).Value;
        }

        [BsonId]
        public ObjectId _id { get; private set; }

        public string OldCallId { get; set; }
        public string GoId { get; set; }
        public RawCallStatus Status { get; set; }
        public string SubmitErrorDetails { get; set; }

        public string RequestXML { get; set; }

        public DateTime RequestCreatedOn { get; set; }

        public string ResponseXML { get; set; }

        public DateTime ResponseCreatedOn { get; set; }

        [BsonIgnore]
        public XDocument DocResp => XDocument.Parse(ResponseXML);

        [BsonIgnore]
        public XDocument DocReq => XDocument.Parse(RequestXML);

        public virtual void AttachResponse(string responseContent, DateTime? responseCreatedOn = null)
        {
            ResponseXML = responseContent;
            ResponseCreatedOn = responseCreatedOn ?? DateTime.UtcNow;

            var isFault = DocResp.Descendants().Any(n => n.Name.LocalName == SoapFaultNode);
            Status = isFault ? RawCallStatus.Fault : RawCallStatus.ResponseReceived;
        }

        public virtual void MarkSubmitFailed(string responseContent, DateTime? responseCreatedOn = null)
        {
            Status = RawCallStatus.SubmitFailed;
        }
    }
}