﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using MongoDB.Bson;

namespace UM.P4.Mongo.Models.RR
{
    public class RequestRawCall : RawCallBase
    {
        public RequestRawCall(ObjectId callId, string requestContent)
            : this(callId, requestContent, null)
        {}


        public RequestRawCall(ObjectId callId, string requestContent, DateTime? requestCreatedOn = null) : base(callId, requestContent, requestCreatedOn)
        {
            DayEans = new List<string>();
            IntEans = new List<string>();
            RcyEans = new List<string>();

            // save how many requests were sent
            foreach (var i in DocReq.Descendants().Where(n =>
                 n.Name.LocalName == "QueryReason"
                 && n.Value == "DAY"))
            {
                DayEans.Add(GetData(i));
            }

            foreach (var i in DocReq.Descendants().Where(n =>
                 n.Name.LocalName == "QueryReason"
                 && n.Value == "INT"))
            {
                IntEans.Add(GetData(i));
            }

            foreach (var i in DocReq.Descendants().Where(n =>
                 n.Name.LocalName == "QueryReason"
                 && n.Value == "RCY"))
            {
                RcyEans.Add(GetData(i));
            }
        }

        public List<string> DayEans { get; set; }
        public List<string> IntEans { get; set; }
        public List<string> RcyEans { get; set; }

        private static string GetData(XElement i)
        {
            return $"{((XElement)i.PreviousNode.PreviousNode.PreviousNode).Value}:{((XElement)i.PreviousNode).Value.Replace("-", "")}";
        }
    }
}