﻿using System;

namespace UM.P4.Mongo.Models
{
    public interface ICreationAuditable
    {
        DateTime CreatedOn { get; set; }
    }

    public interface IModifyAuditable
    {
        DateTime? ModifiedOn { get; set; }
    }
}