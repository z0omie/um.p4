﻿using System;
using System.Configuration;
using Autofac;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using UM.P4.App;
using UM.P4.CoreTypes;
using UM.P4.Mongo;
using UM.P4.Mongo.Contexts;
using UM.P4.Mongo.Models.Requests;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.WinService
{
    internal static class ContainerConfig
    {
        private static MongoClient CreateMongoClient()
        {
            BsonSerializer.RegisterSerializer(new EnumSerializer<RegisterId>(BsonType.String));
            BsonSerializer.RegisterSerializer(typeof(DateTime), new DateTimeSerializer(DateTimeKind.Utc));


            var pack = new ConventionPack
            {
                new CamelCaseElementNameConvention(),
                new EnumRepresentationConvention(BsonType.String)
            };

            ConventionRegistry.Register("CamelCaseConvensions", pack, t => true);

            MongoDefaults.MaxConnectionIdleTime = TimeSpan.FromMinutes(1);

            var mongoUrlBuilder = new MongoUrlBuilder(ConfigurationManager.ConnectionStrings["MongoServer"].ConnectionString);

            var connectionString = mongoUrlBuilder.ToMongoUrl();

            return new MongoClient(connectionString);
        }

        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(CreateMongoClient()).As<IMongoClient>().SingleInstance();

            builder.RegisterType<MongoP4Context>().As<IMongoP4Context>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(MongoRequestContext<>)).As(typeof(IMongoRequestContext<>)).InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(MongoRawCallContext<>)).As(typeof(IMongoRawCallContext<>)).InstancePerLifetimeScope();


            builder.RegisterGeneric(typeof(P4Repository<>)).InstancePerLifetimeScope();
            builder.RegisterType<RawCallRepository>();


            builder.RegisterType<Receiver>();
            builder.RegisterType<Requester>();
            builder.RegisterType<DayRequestService>();
            builder.RegisterType<DetailRequestService>();
            builder.RegisterType<GORepository>();
            builder.RegisterType<MeteringPointRepository>();
            builder.RegisterType<RequestsRepository<DayRequests>>();
            builder.RegisterType<RequestsRepository<DetailRequests>>();
            builder.RegisterType<ODAEmailService>();
            

            builder.RegisterType<P4DetailReadingService>();
            builder.RegisterType<P4DayReadingService>();
            



            builder.RegisterType<RawCallTypedRepository<ResponseRawCall>>();
            builder.RegisterType<RawCallTypedRepository<RequestRawCall>>();




            return builder.Build();
        }
    }
}