using System;
using Autofac;
using NLog;
using Quartz;
using UM.P4.App;

namespace UM.P4.WinService.Jobs
{
    public class SendODAEmail : IJob
    {
        private IContainer _container;

        public void Execute(IJobExecutionContext context)
        {
            Logger logger = LogManager.GetCurrentClassLogger();

            var schedulerContext = context.Scheduler.Context;
            _container = (IContainer)schedulerContext.Get("container");

            doJob(context, logger);
        }

        private void doJob(IJobExecutionContext context, Logger logger)
        {
            try
            {
                var odaEmail = _container.Resolve<ODAEmailService>();
                odaEmail.GenerateFilesToAllGOs();
            }
            catch (Exception e)
            {
                logger.Error(e, "FAILED ODAEmailService job! \n\n Exception: " + e);
            }
        }

        public void Execute(IContainer container)
        {
            _container = container;
            doJob(null, null);
        }
    }
}