﻿using System;
using Autofac;
using NLog;
using Quartz;
using UM.P4.App;
using UM.P4.Mongo;

namespace UM.P4.WinService.Jobs
{
    public class SendReadingRequest : IJob
    {
        private IContainer _container;

        public void Execute(IJobExecutionContext context)
        {
            Logger logger = LogManager.GetCurrentClassLogger();

            var schedulerContext = context.Scheduler.Context;
            _container = (IContainer)schedulerContext.Get("container");

            doJob(context, logger);
        }

        private void doJob(IJobExecutionContext context, Logger logger)
        {
            try
            {
                var c = _container.Resolve<Requester>();

                var countRequestsGenerated = c.GenerateNewRequests();
                if (context != null)
                {
                    context.Result += $"Generated {countRequestsGenerated} new requests (DAY and INT)\n";
                }

                var countNotAnsweredReRequest = c.ReRequestNotAnswered();
                if (context != null)
                {
                    context.Result += $"Generated {countNotAnsweredReRequest} new requests (DAY and INT)\n";
                }

                var countRejectionsReRequested = c.ReRequestRejections("008", "012");
                if (context != null)
                {
                    context.Result += $"Rejections ReRequested {countRejectionsReRequested} (DAY and INT)\n";
                }

                var countFailedReRequested = c.ReRequestFailed();
                if (context != null)
                {
                    context.Result += $"SubmitFailed ReRequested {countFailedReRequested} (DAY and INT)\n";
                }

                var countRequestsMade = c.MakeRequest();
                if (context != null)
                {
                    context.Result += $"Sent {countRequestsMade} requests (DAY and INT)\n";
                }
            }
            catch (Exception e)
            {
                logger.Error(e, "FAILED SendReadingRequest job! \n\n Exception: " + e);
            }
        }

        public void Execute(IContainer container)
        {
            _container = container;
            doJob(null, null);
        }
    }
}