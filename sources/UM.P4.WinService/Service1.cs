﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using Autofac;
using NLog;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using UM.P4.WinService.Jobs;
using UM.P4.WinService.Listeners;
using UM.P4.WinService.WcfServices;
using System.Configuration;
using UM.P4.RabbitMQ;

namespace UM.P4.WinService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();

            var properties = new NameValueCollection();
            properties["quartz.scheduler.instanceName"] = "RemoteServerSchedulerClient";

            // set thread pool info
            properties["quartz.threadPool.type"] = "Quartz.Simpl.SimpleThreadPool, Quartz";
            properties["quartz.threadPool.threadCount"] = "5";
            properties["quartz.threadPool.threadPriority"] = "Normal";

            // set remoting expoter
            properties["quartz.scheduler.exporter.type"] = "Quartz.Simpl.RemotingSchedulerExporter, Quartz";
            properties["quartz.scheduler.exporter.port"] = "555";
            properties["quartz.scheduler.exporter.bindName"] = "QuartzScheduler";
            properties["quartz.scheduler.exporter.channelType"] = "tcp";


            var schedulerFactory = new StdSchedulerFactory(properties);
            _scheduler = schedulerFactory.GetScheduler();
        }

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        readonly IScheduler _scheduler;
        public static IContainer c = ContainerConfig.Configure();

        private static readonly object LockObject = new object();
        public List<ServiceHost> ServiceHosts = null;


        protected override void OnStart(string[] args)
        {
            //Thread.Sleep(10000);


            _logger.Trace("WindowsService started!");




            if (ConfigurationManager.AppSettings["StartJobs"] != null && ConfigurationManager.AppSettings["StartJobs"] == "true")
            {
                RbtMq.EnsureCompletePublisherInfrastructure();
                StartBackgroundJobs();
            }


            _logger.Trace("WindowsService OnStart finished!");

            //starting services
            lock (LockObject)
            {
                if (ServiceHosts != null && ServiceHosts.Count > 0)
                {
                    foreach (var serviceHost in ServiceHosts)
                    {
                        try
                        {
                            serviceHost?.Close();
                        }
                        catch (Exception e)
                        {
                            _logger.Error(e, "Error during stopping old WCF services");
                        }
                    }
                }

                ServiceHosts = new List<ServiceHost>
                                        {
                                            CreateServiceHost<P4Service>(),
                                        };
            }
        }

        protected override void OnStop()
        {
            lock (LockObject)
            {
                try
                {
                    if (ServiceHosts == null) return;

                    foreach (var serviceHost in ServiceHosts.Where(serviceHost => serviceHost != null))
                    {
                        serviceHost.Close();
                    }

                    ServiceHosts = null;
                }
                catch (Exception e)
                {
                    _logger.Error(e, "Error during stop windows service");
                }

            }
        }


        private ServiceHost CreateServiceHost<T>()
        {
            try
            {
                // Create a ServiceHost for the specified type and provide the base address.
                var sh = new ServiceHost(typeof(T));

                // Open the ServiceHostBase to create listeners and start listening for messages.
                sh.Open();
                _logger.Info($"'{typeof(T).Name}' service was started");
                return sh;
            }
            catch (Exception e)
            {
                _logger.Fatal(e, $"Error during starting '{typeof(T).Name}' service");
                return null;
            }
        }

        private void StartBackgroundJobs()
        {
            var mySchedulerListener = new SchedulerListener();
            _scheduler.ListenerManager.AddSchedulerListener(mySchedulerListener);

            _scheduler.Start();

            var myJobListener = new JobListener();
            _scheduler.ListenerManager.AddJobListener(myJobListener, GroupMatcher<JobKey>.AnyGroup());

            var myTriggerListener = new TriggerListener();
            _scheduler.ListenerManager.AddTriggerListener(myTriggerListener, GroupMatcher<TriggerKey>.AnyGroup());

            _scheduler.Context.Put("container", c);


            ScheduleJob<GetReadingResponse>(CronScheduleBuilder.CronSchedule(AppSettings.Schedule_GetReadingResponse)
                .InTimeZone(TimeZoneInfo.Utc));

            //ScheduleJob<SendODAEmail>(CronScheduleBuilder.CronSchedule(AppSettings.Schedule_SendODAEmail)
            //    .InTimeZone(TimeZoneInfo.Utc));

            ScheduleJob<SendReadingRequest>(CronScheduleBuilder.CronSchedule(AppSettings.Schedule_SendReadingRequest)
                .InTimeZone(TimeZoneInfo.Utc));
        }

        private void ScheduleJob<T>(IScheduleBuilder schedule) where T : IJob
        {
            string jobName = typeof(T).Name;
            try
            {
                var jobKey = new JobKey(jobName, "group1");
                IJobDetail jobDetail = JobBuilder.Create<T>()
                                    .WithIdentity(jobKey)
                                    .Build();

                _scheduler.AddJob(jobDetail, true, true); // this gives us possibility to create multiple triggers for the same job

                var triggerKey = new TriggerKey(jobName + "__" + Guid.NewGuid(), "group1");
                ITrigger trigger = TriggerBuilder.Create()
                                                     .WithIdentity(triggerKey)
                                                     .ForJob(jobDetail)
                                                     .StartAt(DateTime.UtcNow.AddSeconds(30))
                                                     .WithSchedule(schedule)
                                                     .Build();

                _scheduler.ScheduleJob(trigger);
            }
            catch (Exception e)
            {
                _logger.Fatal(e, jobName + " FAILED on scheduling. Exception: " + e);
                throw;
            }

            //https://github.com/adometry/QuartzNetManager
        }
    }

    public static class AppSettings
    {
        public static string Schedule_GetReadingResponse
        {
            get { return ConfigurationManager.AppSettings["Schedule_GetReadingResponse"]; }
        }

        public static string Schedule_SendODAEmail
        {
            get { return ConfigurationManager.AppSettings["Schedule_SendODAEmail"]; }
        }

        public static string Schedule_SendReadingRequest
        {
            get { return ConfigurationManager.AppSettings["Schedule_SendReadingRequest"]; }
        }
    }
}