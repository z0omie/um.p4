﻿using NLog;
using Quartz;

namespace UM.P4.WinService.Listeners
{
    public class TriggerListener : ITriggerListener
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public void TriggerFired(ITrigger trigger, IJobExecutionContext context)
        {
            //_logger.Trace("TriggerFired! JobName: {0}; Trigger: {1}", context.JobDetail.Key.Name, trigger.Key.Name);
        }

        public bool VetoJobExecution(ITrigger trigger, IJobExecutionContext context)
        {
            //_logger.Trace("VetoJobExecution! JobName: {0}; Trigger: {1}", context.JobDetail.Key.Name, trigger.Key.Name);
            return false;
        }

        public void TriggerMisfired(ITrigger trigger)
        {
            _logger.Trace("TriggerMisfired! Trigger: {0}", trigger.Key.Name);
        }

        public void TriggerComplete(ITrigger trigger, IJobExecutionContext context, SchedulerInstruction triggerInstructionCode)
        {
            //_logger.Trace("TriggerComplete! JobName: {0}; Trigger: {1}", context.JobDetail.Key.Name, trigger.Key.Name);
        }

        public string Name
        {
            get { return "trigger soulListener"; }
        }
    }
}