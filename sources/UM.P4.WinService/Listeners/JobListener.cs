﻿using System.Text;
using NLog;
using Quartz;

namespace UM.P4.WinService.Listeners
{
    public class JobListener : IJobListener
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public void JobToBeExecuted(IJobExecutionContext context)
        {
            //_logger.Trace("JobToBeExecuted! JobName: {0}", context.JobDetail.Key.Name);
        }

        public void JobExecutionVetoed(IJobExecutionContext context)
        {
            _logger.Trace("JobExecutionVetoed! JobName: {0}", context.JobDetail.Key.Name);
        }

        public void JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException)
        {
            var sb = new StringBuilder(string.Format("JobWasExecuted! JobName: {0}", context.JobDetail.Key.Name));

            if (context.Result != null)
            {
                sb.Append(string.Format("JobResult: {0}", context.Result));
            }

            _logger.Trace(sb.ToString);
        }

        public string Name {
            get { return "job soulListener"; }
        }
    }
}