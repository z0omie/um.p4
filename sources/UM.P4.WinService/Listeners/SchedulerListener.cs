﻿using NLog;
using Quartz;

namespace UM.P4.WinService.Listeners
{
    public class SchedulerListener : ISchedulerListener
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();


        public void JobScheduled(ITrigger trigger)
        {
            _logger.Trace("JobScheduled! TriggerName: {0}", trigger.Key.Name);
        }

        public void JobUnscheduled(TriggerKey triggerKey)
        {
            _logger.Trace("JobUnscheduled! TriggerName: {0}", triggerKey.Name);
        }

        public void TriggerFinalized(ITrigger trigger)
        {
            _logger.Trace("TriggerFinalized! TriggerName: {0}", trigger.Key.Name);
        }

        public void TriggerPaused(TriggerKey triggerKey)
        {
            _logger.Trace("TriggerPaused! TriggerName: {0}", triggerKey.Name);
        }

        public void TriggersPaused(string triggerGroup)
        {
            _logger.Trace("TriggersPaused! triggerGroup:{0}", triggerGroup);
        }

        public void TriggerResumed(TriggerKey triggerKey)
        {
            _logger.Trace("TriggerResumed! TriggerName: {0}", triggerKey.Name);
        }

        public void TriggersResumed(string triggerGroup)
        {
            _logger.Trace("TriggersResumed! triggerGroup:{0}", triggerGroup);
        }

        public void JobAdded(IJobDetail jobDetail)
        {
            _logger.Trace("JobAdded! JobName: {0}", jobDetail.Key.Name);
        }

        public void JobDeleted(JobKey jobKey)
        {
            _logger.Trace("JobDeleted! JobName: {0}", jobKey.Name);
        }

        public void JobPaused(JobKey jobKey)
        {
            _logger.Trace("JobPaused! JobName: {0}", jobKey.Name);
        }

        public void JobsPaused(string jobGroup)
        {
            _logger.Trace("JobsPaused! jobGroup:{0}", jobGroup);
        }

        public void JobResumed(JobKey jobKey)
        {
            _logger.Trace("JobResumed! JobName: {0}", jobKey.Name);
        }

        public void JobsResumed(string jobGroup)
        {
            _logger.Trace("JobsResumed! jobGroup:{0}", jobGroup);
        }

        public void SchedulerError(string msg, SchedulerException cause)
        {
            _logger.Trace("SchedulerError! Msg:{0}.\n\n Cuase: {1}", msg, cause);
        }

        public void SchedulerInStandbyMode()
        {
            _logger.Trace("SchedulerInStandbyMode!");
        }

        public void SchedulerStarted()
        {
            _logger.Trace("SchedulerStarted!");
        }

        public void SchedulerStarting()
        {
            _logger.Trace("SchedulerStarting!");
        }

        public void SchedulerShutdown()
        {
            _logger.Trace("SchedulerShutdown!");
        }

        public void SchedulerShuttingdown()
        {
            _logger.Trace("SchedulerShuttingdown!");
        }

        public void SchedulingDataCleared()
        {
            _logger.Trace("SchedulingDataCleared!");
        }
    }
}
