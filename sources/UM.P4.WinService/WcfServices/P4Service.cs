﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using MongoDB.Driver;
using UM.P4.App;
using UM.P4.Contract;
using UM.P4.CoreTypes;
using UM.P4.Mongo;
using UM.P4.Mongo.Contexts;
using UM.P4.Mongo.Models.Requests;
using UM.P4.Mongo.Models.RR;
using MeteringPoint = UM.P4.Contract.MeteringPoint;
using GridOperator = UM.P4.Contract.GridOperator;
using P4DayReadings = UM.P4.Contract.P4DayReadings;
using P4DetailReadings = UM.P4.Contract.P4DetailReadings;

namespace UM.P4.WinService.WcfServices
{
    class P4Service : IP4Service
    {
        private readonly MeteringPointRepository _mpRepo;
        private readonly GORepository _goRepo;
        private readonly P4Repository<Mongo.Models.P4.P4DayReadings> _p4DayRepo;
        private readonly P4Repository<Mongo.Models.P4.P4DetailReadings> _p4DetailRepo;
        private readonly RequestsRepository<DayRequests> _dayRequestRepo;

        public P4Service()
        {
            _mpRepo = Service1.c.Resolve<MeteringPointRepository>();
            _goRepo = Service1.c.Resolve<GORepository>();
            _p4DetailRepo = Service1.c.Resolve<P4Repository<Mongo.Models.P4.P4DetailReadings>>();
            _p4DayRepo = Service1.c.Resolve<P4Repository<Mongo.Models.P4.P4DayReadings>>();
            _dayRequestRepo = Service1.c.Resolve<RequestsRepository<DayRequests>>();
        }

        // todo: дату старта и конца связать с имененм клиента (что б можно было нескольких сапплаить по одному адресу)
        public async Task StartEans(ICollection<EanBoekItem> eanBookItems, string clientName, DateTime? startAt = null)
        {
            try
            {
                if (!startAt.HasValue || startAt == DateTime.MinValue)
                {
                    startAt = DateTime.Today;
                }

                foreach (var eanBookItem in eanBookItems)
                {
                    var mp = await _mpRepo.GetMP(eanBookItem.EanCode);
                    if (mp == null)
                    {
                        var stateLatest = new StateHistory { State = MPState.Started, CreatedOn = DateTime.Now };

                        mp = new Mongo.Models.RR.MeteringPoint
                        {
                            BuildingNr =
                                string.IsNullOrEmpty(eanBookItem.BuildingNr)
                                    ? (int?)null
                                    : Convert.ToInt32(eanBookItem.BuildingNr),
                            BuildingNrEx = eanBookItem.BuildingNrEx,
                            City = eanBookItem.City,
                            MonitoredFrom = startAt.Value,
                            MonitoredTill = null,
                            EanId = eanBookItem.EanCode,
                            GridOperatorId = eanBookItem.GridOperatorEan,
                            ProdType = eanBookItem.ProductType,
                            IsSpecialConnection = eanBookItem.IsSpecialConnection,
                            Street = eanBookItem.Street,
                            ZipCode = eanBookItem.ZipCode,
                            ClientName = clientName,
                            State = MPState.Started,
                            StatusUpdatedOn = DateTime.Now,
                            Meters = new List<EnergyMeter>(),

                            StateLatest = stateLatest,
                            StateHistory = new List<StateHistory> { stateLatest }
                        };

                        //await _mpRepo.AddMeteringPoint(mp);
                        //await _goRepo.AddGO(eanBookItem.GridOperatorName, eanBookItem.GridOperatorEan);
                        //await _goRepo.NeedSendODAFile(mp.GridOperatorId, true);

                        await Task.WhenAll(
                            _mpRepo.InsertMP(mp),
                            _goRepo.AddGO(eanBookItem.GridOperatorName, eanBookItem.GridOperatorEan),
                            _goRepo.NeedSendODAFile(mp.GridOperatorId, true)
                        );
                    }

                    if (mp.State != MPState.Started)
                    {
                        await Task.WhenAll(
                            _mpRepo.StartMP(mp.EanId, startAt.Value),
                            _goRepo.NeedSendODAFile(mp.GridOperatorId, true)
                        );
                    }
                }
            }
            catch (Exception e)
            {
                var i = 0;
                i++;
            }
        }

        public async Task StopEans(ICollection<string> eans)
        {
            foreach (var ean in eans)
            {
                var mp = await _mpRepo.GetMP(ean);

                // Monitored till?
                await Task.WhenAll(
                    _mpRepo.StopMP(mp.EanId),
                    _goRepo.NeedSendODAFile(mp.GridOperatorId, true)
                  );
            }
        }

        public async Task<ICollection<P4DetailReadings>> GetDetailReadings(string ean, ICollection<DateTime> dates)
        {
            var readings = (await _p4DetailRepo.GetReadings(ean, dates));
            return
                readings.Select(
                    t => new P4DetailReadings
                    {
                        Ean = t.Ean,
                        QueryDate = t.QueryDate,
                        Readings = t.Readings.ToDictionary(k => k.DateTime, v => v.Value)
                    }).ToList();
        }

        public async Task<ICollection<P4DetailReadings>> GetDetailReadingsInInterval(string ean, DateTime dateFrom, DateTime dateTo)
        {
            var readings = (await _p4DetailRepo.GetReadings(ean, dateFrom, dateTo));
            return
                readings.Select(
                    t => new P4DetailReadings
                    {
                        Ean = t.Ean,
                        QueryDate = t.QueryDate,
                        Readings = t.Readings.ToDictionary(k => k.DateTime, v => v.Value)
                    }).ToList();
        }

        public async Task<ICollection<P4DayReadings>> GetDayReadings(string ean, ICollection<DateTime> dates)
        {
            return (await _p4DayRepo.GetReadings(ean, dates)).Select(
                t => new P4DayReadings
                {
                    Ean = t.Ean,
                    QueryDate = t.QueryDate,
                    Readings = t.Readings
                }).ToList();
        }

        public async Task<ICollection<GridOperator>> GetGridOperators()
        {
            var allGos = await _goRepo.GetAllGOs();

            return allGos.Select(go => new GridOperator { Eans = go.Eans, Name = go.Name }).ToList();
        }

        public async Task<DateTime?> GetFirstReadingDate(string ean)
        {
            return await _p4DayRepo.GetFirstReadingDate(ean);
        }

        public DateTime GetFirstRejectionDate(string ean, string rejectionCode)
        {
            return _dayRequestRepo.GetFirstRejection(ean, rejectionCode).QueryDate;
        }

        public async Task<MeteringPoint> GetMeteringPoint(string ean)
        {
            var mp = await _mpRepo.GetMP(ean);

            return new MeteringPoint
            {
                EanId = mp.EanId,
                GridOperatorId = mp.GridOperatorId,
                ProdType = mp.ProdType,
                State = mp.State,
                Meters = mp.Meters,
                PeakHourEndsWith = mp.PeakHourEndsWith ?? 22
            };
        }

        public async Task PauseEans(ICollection<string> eans)
        {
            foreach (var ean in eans)
            {
                var mp = await _mpRepo.GetMP(ean);

                // Monitored till?
                await Task.WhenAll(_mpRepo.PauseMP(mp.EanId));
            }
        }

        public async Task<P4DetailReadings> GetLastDetailReading(string ean)
        {
            var r = await _p4DetailRepo.GetLastReading(ean);
            return
                new P4DetailReadings
                {
                    Ean = r.Ean,
                    Readings = r.Readings.ToDictionary(k => k.DateTime, v => v.Value),
                    QueryDate = r.QueryDate
                };
        }

        public async Task<P4DayReadings> GetLastDayReading(string ean)
        {
            var r = await _p4DayRepo.GetLastReading(ean);
            return
                new P4DayReadings
                {
                    Ean = r.Ean,
                    Readings = r.Readings,
                    QueryDate = r.QueryDate
                };
        }

        public ICollection<DayRequest> GetLastDayRequest(string ean, int limit = 10)
        {
            return _dayRequestRepo.GetLast(ean, limit)
                .Select(t => new DayRequest
                {
                    CreatedOn = t.CreatedOn,
                    Ean = t.Ean,
                    Date = t.QueryDate,
                    RejectionCode = t.ResponseLast?.RejectionCode,
                    DayRequestType = t.DayRequestType,
                    ErrorDetails = t.ResponseLast?.ErrorDetails,
                    RejectionText = t.ResponseLast?.RejectionText,
                    ReqHistory = t.RequestHistory.Count,
                    RespHistory = t.ResponseHistory.Count,
                    ReqLastCallCreatedOn = t.RequestLast.CreatedOn,
                    ReqLastCallId = t.RequestLast.CallId,
                    ReqLastCallStatus = t.RequestLast.Status,
                    RespLastCallCreatedOn = t.ResponseLast?.CreatedOn,
                    RespLastCallId = t.ResponseLast?.CallId,
                    RespLastCallStatus = t.ResponseLast?.Status
                }).ToList();
        }

        public Task<int> GenerateRecoveryRequests(ICollection<string> eans)
        {
            throw new NotImplementedException();
        }
    }
}