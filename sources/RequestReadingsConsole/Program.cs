﻿using Autofac;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UM.P4.App;
using UM.P4.RabbitMQ;

namespace RequestReadingsConsole
{

    class Program
    {
        static void Main(string[] args)
        {
            ILoggerFactory loggerFactory = new LoggerFactory().AddConsole();
            var log = loggerFactory.CreateLogger("Program");

            IContainer c = ContainerConfig.Configure(log);

            var requester = c.Resolve<Requester>();

            RbtMq.EnsureCompletePublisherInfrastructure();

            log.LogInformation("starting...");
            long result = 0;
            //result += requester.ReRequestMandateRenewal("871687110002993268");

            //result += requester.GenerateNewRequests();
            //result += requester.ReRequestNotAnswered();
            result += requester.ReRequestRejections("008", "012", "014");

            log.LogInformation($"done generating new requests. {result}");


            //var countFailedReRequested = requester.ReRequestFailed();
            //log.LogInformation($"SubmitFailed ReRequested {countFailedReRequested} (DAY and INT)\n");

            //log.LogInformation($"Making requests...");
            var reqResult = requester.MakeRequest();
        }
    }
}
