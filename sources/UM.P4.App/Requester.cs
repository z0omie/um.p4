﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using UM.P4.CoreTypes;
using UM.P4.EDSN;
using UM.P4.Mongo;
using UM.P4.Mongo.Models.P4;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.App
{
    public class Requester
    {

        private readonly DayRequestService _dayRequestService;
        private readonly DetailRequestService _detailRequestService;
        private readonly MeteringPointRepository _mpRepo;
        private readonly ILogger _log;

        public Requester(DetailRequestService detailRequestService, DayRequestService dayRequestService, MeteringPointRepository mpRepo, ILogger log)
        {
            _detailRequestService = detailRequestService;
            _dayRequestService = dayRequestService;
            _mpRepo = mpRepo;
            _log = log;
        }

        //create requests to EDSN
        public Tuple<int, int> MakeRequest()
        {
            _log.LogInformation($"Making DET requests...");
            var req1 = _detailRequestService.MakeRequest();
            
            _log.LogInformation($"Making DAY requests...");
            var req2 = _dayRequestService.MakeRequest();

            return new Tuple<int, int>(req1.Item1 + req2.Item1, req1.Item2 + req2.Item2);
        }


        public long GenerateNewRequests(string ean, List<DateTime> queryDates)
        {
            long result = 0;
            result += _dayRequestService.RequestAgain(ean, queryDates);
            result += _detailRequestService.RequestAgain(ean, queryDates);

            return result;
        }


        public long GenerateNewRequests()
        {
            _log.LogInformation($"START GenerateNewRequests");
            List<MeteringPoint> monitoredEans = _mpRepo.GetMPsByStatus(MPState.Started).Result;
            _log.LogInformation($"total {monitoredEans.Count} monitored EANs");

            long result = 0;
            _log.LogInformation($"Start generating DAY requestst");
            var dayRequests = _dayRequestService.GenerateNewRequests(monitoredEans);
            _log.LogInformation($"total {dayRequests} day requests generated");
            result += dayRequests;

            _log.LogInformation($"Start generating INT requestst");
            var detailRequests = _detailRequestService.GenerateNewRequests(monitoredEans);
            _log.LogInformation($"total {detailRequests} INT requests generated");
            result += detailRequests;

            //result += _dayRequestService.GenerateRecoveryRequests(monitoredEans);

            _log.LogInformation($"DONE GenerateNewRequests");
            return result;
        }
        public long GenerateNewRequestsForDate(DateTime date)
        {
            _log.LogInformation($"START GenerateNewRequests");
            List<MeteringPoint> monitoredEans = _mpRepo.GetMPsByStatus(MPState.Started).Result;
            _log.LogInformation($"total {monitoredEans.Count} monitored EANs");

            long result = 0;
            _log.LogInformation($"Start generating DAY requestst");
            var dayRequests = _dayRequestService.GenerateNewRequestsForDate(monitoredEans, date);
            _log.LogInformation($"total {dayRequests} day requests generated");
            result += dayRequests;

            _log.LogInformation($"Start generating INT requestst");
            var detailRequests = _detailRequestService.GenerateNewRequestsForDate(monitoredEans, date);
            _log.LogInformation($"total {detailRequests} INT requests generated");
            result += detailRequests;

            //result += _dayRequestService.GenerateRecoveryRequests(monitoredEans);

            _log.LogInformation($"DONE GenerateNewRequests");
            return result;
        }

        public long ReRequestRejections(params string[] rejectionsToReQuest)
        {
            
            long result = 0;

            foreach (var rejCode in rejectionsToReQuest)
            {
                _log.LogInformation($"START ReRequestRejections {rejCode}");

                var dayRequests = _dayRequestService.RequestRejectionAgain(rejCode);
                result += dayRequests;
                _log.LogInformation($"total {dayRequests} day requests generated");

                var detailRequests = _detailRequestService.RequestRejectionAgain(rejCode);
                result += detailRequests;
                _log.LogInformation($"total {detailRequests} detail requests generated");

                _log.LogInformation($"DONE ReRequestRejections {rejCode}");
            }

            return result;
        }

        public long ReRequestMandateRenewal(string ean)
        {
            long result = 0;
            var rejcode = "040";

            result += _dayRequestService.RequestRejectionAgain(rejcode, ean);
            result += _detailRequestService.RequestRejectionAgain(rejcode, ean);

            return result;
        }

        public long ReRequestInvalid()
        {
            _log.LogInformation($"START ReRequestInvalid");

            long result = 0;

            var dayRequests = _dayRequestService.RequestInvalidAgain();
            result += dayRequests;
            _log.LogInformation($"total {dayRequests} day requests generated");

            var detailRequests = _detailRequestService.RequestInvalidAgain();
            result += detailRequests;
            _log.LogInformation($"total {detailRequests} detail requests generated");

            _log.LogInformation($"DONE ReRequestInvalid");
            return result;
        }

        public long ReRequestNotAnswered()
        {
            _log.LogInformation($"START Not answered");

            long result = 0;

            var dayRequests = _dayRequestService.ReRequestNotAnswered(41);
            _log.LogInformation($"total {dayRequests} day requests generated");
            result += dayRequests;

            var detailRequests = _detailRequestService.ReRequestNotAnswered(11);
            _log.LogInformation($"total {detailRequests} day requests generated");
            result += detailRequests;

            _log.LogInformation($"DONE Not answered");
            return result;
        }

        //RE processing EDSN response
        public void ProcessEDSNRequests(string content, ObjectId callId)
        {
            content = content.Replace("standard", "draft");

            var xml = new XmlDocument();
            xml.LoadXml(content);
            var nsmgr = new XmlNamespaceManager(xml.NameTable);
            nsmgr.AddNamespace("ns4", "urn:edsn:edsn:data:p4collecteddatabatchrequestenvelope:1:draft");

            var p4Envelope = xml.SelectSingleNode("//ns4:P4CollectedDataBatchRequestEnvelope", nsmgr);

            XmlSerializer ser = new XmlSerializer(typeof(P4CollectedDataBatchRequestEnvelope), "urn:edsn:edsn:data:p4collecteddatabatchrequestenvelope:1:draft");
            var resp = (P4CollectedDataBatchRequestEnvelope)ser.Deserialize(new StringReader(p4Envelope.OuterXml));

            ProcessEDSNRequest(resp, callId);
        }

        private void ProcessEDSNRequest(P4CollectedDataBatchRequestEnvelope req, ObjectId callId)
        {
            foreach (var pc in req.P4Content)
            {
                var mp = _mpRepo.GetMP(pc.EANID).Result;
                //choosing right reading service
                switch (pc.QueryReason)
                {
                    case P4CollectedDataBatchRequestEnvelope_QueryReasonTypeCode.INT:
                        _detailRequestService.AddNewRequest(mp, pc.QueryDate, req.EDSNBusinessDocumentHeader.CreationTimestamp);
                        _detailRequestService.RegisterRequestSent(new List<string> { P4Readings.BuildId(pc.EANID, pc.QueryDate) }, callId);
                        break;
                    case P4CollectedDataBatchRequestEnvelope_QueryReasonTypeCode.RCY:
                    case P4CollectedDataBatchRequestEnvelope_QueryReasonTypeCode.DAY:
                        _dayRequestService.AddNewRequest(mp, pc.QueryDate, req.EDSNBusinessDocumentHeader.CreationTimestamp);
                        _dayRequestService.RegisterRequestSent(
                            new List<string> { P4Readings.BuildId(pc.EANID, pc.QueryDate) }, callId);
                        break;
                }
            }
        }


        public long ReRequestFailed()
        {
            long result = 0;

            _log.LogInformation($"ReRequesting Day");
            result += _dayRequestService.RequestFailedAgain();

            _log.LogInformation($"ReRequesting Det");
            result += _detailRequestService.RequestFailedAgain();

            return result;
        }
    }
}