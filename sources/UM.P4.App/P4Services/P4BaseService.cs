﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UM.P4.EDSN;
using UM.P4.Mongo;
using UM.P4.Mongo.Models.P4;
using UM.P4.ReadingsQ;

namespace UM.P4.App
{
    public abstract class P4BaseService<T>
        where T : P4Readings
    {
        protected readonly P4Repository<T> repo;
        private DetailReadingsSender readingsSender;

        protected P4BaseService(P4Repository<T> repo)
        {
            this.repo = repo;
            readingsSender = new DetailReadingsSender();
        }


        public async Task<List<T>> GetReadings(string ean, IEnumerable<DateTime> queryDates)
        {
            return await repo.GetReadings(ean, queryDates);
        }

        public void ProcessAndSaveEanTest(P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint p4MP)
        {
            // start processing
            var meter = ((P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter)p4MP.Items[0]);
            var registers = meter.P4Register;

            // order reading by reading date
            foreach (var register in registers)
            {
                register.P4Reading = register.P4Reading.OrderBy(r => r.ReadingDateTime).ToArray();
            }

            var document = repo.Get(p4MP.EANID, p4MP.QueryDate).Result ?? (T)Activator.CreateInstance(typeof(T), p4MP.EANID, p4MP.QueryDate);

            // calling implementation to extract readings
            ReadReadings(document, registers);

            repo.ReplaceP4Readings(document);
            //readingsSender.Send(document).GetAwaiter().GetResult();
        }

        protected abstract void ReadReadings(T document, P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register[] registers);
    }
}