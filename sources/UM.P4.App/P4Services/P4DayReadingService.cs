﻿using System.Collections.Generic;
using System.Linq;
using UM.P4.CoreTypes;
using UM.P4.EDSN;
using UM.P4.Mongo;
using UM.P4.Mongo.Models.P4;

namespace UM.P4.App
{
    public class P4DayReadingService : P4BaseService<P4DayReadings>
    {
        public P4DayReadingService(P4Repository<P4DayReadings> monCon) : base(monCon) { }

        protected override void ReadReadings(P4DayReadings document, P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register[] registers)
        {
            // if readings already exists TODO: would be nice to compare them first.
            // if (document.DayReadings != null) return;

            document.Readings = new Dictionary<RegisterId, double>();

            foreach (P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register t in registers)
            {
                P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register_P4Reading readingToProcess = null;
                readingToProcess = t.P4Reading.SingleOrDefault();

                if (readingToProcess == null)
                {
                    throw new MPValidationException("cannot find reading for the requested date");
                }

                if (readingToProcess.Reading < 0)
                {
                    throw new MPValidationException("reading is less then 0.");
                }

                var registerId = C.RegisterMapping[t.ID];
                if (!document.Readings.ContainsKey(registerId))
                {
                    document.Readings.Add(registerId, (double)readingToProcess.Reading);
                }
            }
        }
    }
}