using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UM.P4.CoreTypes;
using UM.P4.EDSN;
using UM.P4.Mongo;
using UM.P4.Mongo.Models.P4;

namespace UM.P4.App
{
    public class P4DetailReadingService : P4BaseService<P4DetailReadings>
    {
        public P4DetailReadingService(P4Repository<P4DetailReadings> monCon) : base(monCon) { }

        protected override void ReadReadings(P4DetailReadings document, P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register[] registers)
        {
            document.Readings = new List<P4DetailReadings.DetailReading>();

            foreach (var register in registers)
            {
                foreach (var registerReading in register.P4Reading)
                {
                    var r = document.Readings.SingleOrDefault(t => t.DateTime == registerReading.ReadingDateTime);
                    if (r == null)
                    {
                        r = new P4DetailReadings.DetailReading(registerReading.ReadingDateTime, new Dictionary<RegisterId, double>
                            {
                                {C.RegisterMapping[register.ID], (double) registerReading.Reading}
                            });
                        document.Readings.Add(r);
                    }
                    else
                    {
                        r.Value.Add(C.RegisterMapping[register.ID], (double)registerReading.Reading);
                    }
                }
            }


            document.Readings = document.Readings.OrderBy(r => r.DateTime).ToList();

            document.UsageMax = document.Readings.First().Value.ToDictionary(k => k.Key, v => new UsageMax
            {
                Usage = 0,
                From = default(DateTime),
                To = default(DateTime),
            });
            for (int i = 0; i < document.Readings.Count() - 1; i++)
            {
                foreach (var registerId in document.Readings[i].Value.Keys)
                {
                    var uMax = document.Readings[i + 1].Value[registerId] - document.Readings[i].Value[registerId];

                    if (uMax > document.UsageMax[registerId].Usage)
                    {
                        document.UsageMax[registerId] = new UsageMax
                        {
                            Usage = uMax,
                            From = document.Readings[i].DateTime,
                            To = document.Readings[i + 1].DateTime,
                        };
                    }
                }

            }


            // TODO:
            // ������� ��� ����� ������ ������
            //FillLastReadingOfPrevDay(document);
            //FillLastReadingOfCurrentDay(document);

            //document.ReCalculateDetailUsage(22);
            //document.ReCalculateDayUsage();
        }


        protected void FillLastReadingOfPrevDay(P4DetailReadings currentDoc)
        {
            var prevDoc = repo.Get(currentDoc.GetPrevId()).Result;
            if (prevDoc?.Readings == null || !prevDoc.Readings.Any()) return;

            var firstThisReading = currentDoc.Readings.OrderBy(dr => dr.DateTime).First();
            var lastPrevReading = prevDoc.Readings.OrderByDescending(dr => dr.DateTime).First();

            if (firstThisReading.DateTime.ToUniversalTime() == lastPrevReading.DateTime.ToUniversalTime()) return;

            prevDoc.Readings.Add(firstThisReading);

            repo.ReplaceP4Readings(prevDoc);
        }

        protected void FillLastReadingOfCurrentDay(P4DetailReadings currentDoc)
        {
            var nextDoc = repo.Get(currentDoc.GetNextId()).Result;
            if (nextDoc?.Readings == null || !nextDoc.Readings.Any()) return;

            var firstNextReading = nextDoc.Readings.OrderBy(dr => dr.DateTime).First();
            var lastThisReading = currentDoc.Readings.OrderByDescending(dr => dr.DateTime).First();

            if (firstNextReading.DateTime.ToUniversalTime() == lastThisReading.DateTime.ToUniversalTime()) return;

            currentDoc.Readings.Add(firstNextReading);
        }
    }
}