using System;

namespace UM.P4.App
{
    public class MPValidationException : Exception
    {
        public MPValidationException(string message)
            : base(message)
        {
        }
    }
}