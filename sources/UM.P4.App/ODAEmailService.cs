﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using MongoDB.Driver;
using UM.P4.Mongo;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.App
{
    public class ODAEmailService
    {
        public static readonly DateTime DefaultMonitoringTill = new DateTime(2099, 11, 28);
        public static readonly string CSVFilesPath = @"c:\Tresor\UberMeter\ODAEmails\Test";
        public static readonly string ODAEAN = "8712423035904";

        private static List<string> GONamesToAttachToLiander = new List<string> { "Enduris B.V.", "Enexis NB", "Westland NB", "Rendo NB", "Endinet NB", "Cogas NB" };

        private readonly MeteringPointRepository _mpRepository;
        private readonly GORepository _goRepo;

        public ODAEmailService(MeteringPointRepository mpRepository, GORepository goRepo)
        {
            _mpRepository = mpRepository;
            _goRepo = goRepo;
        }

        public void GenerateFilesToAllGOs()
        {
            var allGOs = _goRepo.GetAllGOs().Result;
            foreach (var go in allGOs)
            {
                if (go.ODACSVSettigns == null || GONamesToAttachToLiander.Contains(go.Name))
                {
                    // no need to generate file for "attach to Liander" nbs
                    continue;
                }

                GenerateAndSendOdaFileTo(go);
            }
        }

        public string GenerateAndSendOdaFileTo(GridOperator go)
        {
            var filePath = GenerateAndSaveCSVFile(go);

            var mail = new MailMessage { From = new MailAddress("oleg@ubermeter.nl", "Oleg Polovets") };

            //mail.To.Add(new MailAddress("o.polovets@gmail.com"));
            if (string.IsNullOrWhiteSpace(go.ODACSVSettigns.EmailTo))
            {
                return "email NOT sent";
            }
            mail.To.Add(new MailAddress(go.ODACSVSettigns.EmailTo));

            if (go.ODACSVSettigns.EmailCc != null && go.ODACSVSettigns.EmailCc.Any())
            {
                mail.To.Add(new MailAddress(go.ODACSVSettigns.EmailCc));
            }
            mail.Bcc.Add(new MailAddress("oleg@ubermeter.nl"));

            mail.Subject = go.ODACSVSettigns.EmailSubject;
            mail.Body = go.ODACSVSettigns.EmailBody;
            mail.Attachments.Add(new Attachment(filePath));

            var client = new SmtpClient
            {
                Port = 587,
                Host = "smtp.zoho.com",
                EnableSsl = true,
                Timeout = 30000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                // this is using app password from zoho
                Credentials = new System.Net.NetworkCredential("oleg@ubermeter.nl", "AnXKmeZKuWLI")
            };

            client.Send(mail);

            UpdateResult updateResult = _goRepo.NeedSendODAFileByName(go.Name, false).Result;

            return "email sent!";
        }


        public string GenerateAndSaveCSVFile(GridOperator go)
        {
            string csv = GenerateCSVContentFor(go);

            if (go.Name == "Liander NB")
            {
                foreach (var goName in GONamesToAttachToLiander)
                {
                    // also attachEnduris clients
                    var realGo = _goRepo.GetGO(goName);

                    string csvEnduris = GenerateCSVContentFor(realGo);

                    // skipping header line
                    string[] lines = csvEnduris
                        .Split(Environment.NewLine.ToCharArray())
                        .Skip(1)
                        .ToArray();

                    csvEnduris = string.Join(Environment.NewLine, lines);

                    csv += csvEnduris;
                }
            }

            string folderToStore = Path.Combine(CSVFilesPath, go.Name, DateTime.Now.ToString("dd-MM-yyy"));
            string fileName = go.ODACSVSettigns.CSVNameFormat
                .Replace("{{ean_oda}}", ODAEAN)
                .Replace("{{date}}", DateTime.Now.Date.ToString(go.ODACSVSettigns.CSVNameDateFormat))
                .Replace("{{seq}}", "0000");

            var pathToFile = Path.Combine(folderToStore, fileName);


            Directory.CreateDirectory(Path.GetDirectoryName(pathToFile));
            File.WriteAllText(pathToFile, csv);

            return pathToFile;
        }

        public string GenerateCSVContentFor(GridOperator go)
        {
            var result = new StringBuilder();

            if (!string.IsNullOrEmpty(go.ODACSVSettigns.CSVHeader))
            {
                result.AppendLine(go.ODACSVSettigns.CSVHeader);
            }

            foreach (var goEan in go.Eans)
            {
                //get all connections of this GO
                var goConnections = _mpRepository.GetMPsForGO(goEan).Result.Where(s=>s.State != CoreTypes.MPState.Stopped).ToList();
                foreach (var mp in goConnections)
                {
                    result.AppendLine(ReplacePlaceholders(go.ODACSVSettigns.CSVLineFormat, prepareSubs(mp, go.ODACSVSettigns)));

                    if (mp.InODAEmailOn.HasValue) continue;

                    mp.InODAEmailOn = DateTime.Now;
                    _mpRepository.ReplaceMP(mp).Wait();
                }
            }

            return result.ToString().Replace("\r", "");
        }

        private Dictionary<string, string> prepareSubs(MeteringPoint mp, ODAEmailSettings es)
        {
            return new Dictionary<string, string>
            {
                {"ean_oda",ODAEAN},
                {"ean_connection",mp.EanId},
                {"mandate_start_date",mp.MonitoredFrom.ToString(es.CSVLineDateFormat)},
                {"mandate_end_date",mp.MonitoredTill?.ToString(es.CSVLineDateFormat) ?? DefaultMonitoringTill.ToString(es.CSVLineDateFormat)},
                {"client_name",mp.ClientName},
            };
        }

        private string ReplacePlaceholders(string template, Dictionary<string, string> subs)
        {
            return subs.Aggregate(template, (current, sub) => current.Replace("{{" + sub.Key + "}}", sub.Value));
        }
    }
}