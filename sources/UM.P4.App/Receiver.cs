﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using MongoDB.Bson;
using UM.P4.CoreTypes;
using UM.P4.CoreTypes.Messages;
using UM.P4.EDSN;
using UM.P4.Mongo;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.App
{
    public class Receiver
    {
        private readonly DayRequestService _dayRequestService;
        private readonly DetailRequestService _detailRequestService;
        private readonly GORepository _goRepo;

        public Receiver(DayRequestService dayRequestService, DetailRequestService detailRequestService, GORepository goRepo, RawCallTypedRepository<ResponseRawCall> rawCallTypedRepo)
        {
            _dayRequestService = dayRequestService;
            _detailRequestService = detailRequestService;
            _goRepo = goRepo;
        }

        public int GetResponse()
        {
            //iterate on GridOperators present in DB
            //List<string> gos = _goRepo.GetAllGOs().Result.SelectMany(e => e.Eans).ToList();
            List<string> gos = new List<string> { "8712423010208" };

            return gos.Sum(GetResponseGO);
        }

        public int GetResponseGO(string go)
        {
            var totalCollectedFromThisGO = 0;

            while (true)
            {
                var callId = ObjectId.GenerateNewId();

                var callResult = EdsnProxy.GetDataResponse(go, callId);

                //TODO: log it.
                if (!callResult.IsSuccess) break;

                ProcessEDSNResponse(
                    ((P4CollectedDataBatchResultRequestResponse)callResult.Response)
                        .P4CollectedDataBatchResultResponseEnvelope, callId).GetAwaiter().GetResult();

                var collectedOnThisIteration = ((P4CollectedDataBatchResultRequestResponse)callResult.Response).P4CollectedDataBatchResultResponseEnvelope.P4Content.Length;
                totalCollectedFromThisGO += collectedOnThisIteration;

                if (collectedOnThisIteration == 0)
                {
                    break;
                }
            }

            Console.WriteLine($"{DateTime.Now} Processed {totalCollectedFromThisGO}");

            return totalCollectedFromThisGO;
        }

        //RE processing EDSN response
        public void ProcessEDSNResponse(string content, ObjectId callId, DateTime createdOn)
        {
            content = content.Replace("draft", "standard");

            var xml = new XmlDocument();
            xml.LoadXml(content);
            var nsmgr = new XmlNamespaceManager(xml.NameTable);
            nsmgr.AddNamespace("ns3", "urn:nedu:edsn:data:p4collecteddatabatchresultresponse:2:standard");

            var p4Envelope = xml.SelectSingleNode("//ns3:P4CollectedDataBatchResultResponseEnvelope", nsmgr);

            XmlSerializer ser = new XmlSerializer(typeof(P4CollectedDataBatchResultResponseEnvelope), "urn:nedu:edsn:data:p4collecteddatabatchresultresponse:2:standard");
            var resp = (P4CollectedDataBatchResultResponseEnvelope)ser.Deserialize(new StringReader(p4Envelope.OuterXml));

            ProcessEDSNResponse(resp, callId).Wait();
        }

        private async Task ProcessEDSNResponse(P4CollectedDataBatchResultResponseEnvelope resp, ObjectId callId)
        {
            //iterate per MeteringPoint
            Parallel.ForEach(resp.P4Content, new ParallelOptions { MaxDegreeOfParallelism = 20 }, pc =>
              {

                  //foreach (P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint pc in resp.P4Content)
                  //{
                  //choosing right reading service
                  switch (pc.QueryReason)
                  {
                      case P4CollectedDataBatchResultResponseEnvelope_QueryReasonTypeCode.INT:

                          var request = _detailRequestService.RegisterResponseReceived(pc.EANID, pc.QueryDate, callId, resp.EDSNBusinessDocumentHeader.CreationTimestamp).GetAwaiter().GetResult();

                          try
                          {
                              _detailRequestService.ProcessAndSaveEan(pc, request).GetAwaiter().GetResult();

                              if (request.ResponseLast.Status == EdsnResponseCallStatus.Ok)
                              {
                                  _detailRequestService.RaiseReadingsReceived(pc.EANID, pc.QueryDate).GetAwaiter().GetResult();
                              }
                              else if (request.ResponseLast.Status == EdsnResponseCallStatus.Rejected)
                              {
                                  _detailRequestService.RaiseRejectionReceived(new RejectionMessage
                                  {
                                      Ean = pc.EANID,
                                      QueryDate = pc.QueryDate.ToString("yyyyMMdd"),
                                      RejectionCode = request.ResponseLast.RejectionCode,
                                      RejectionText = request.ResponseLast.RejectionText,
                                      QueryReason = "DET"
                                  });
                              }
                              //_detailRequestService.RaiseMpStatusReceived(
                              //    new MpStatusQMsg
                              //    {
                              //        QueryDate = pc.QueryDate.ToString("yyyyMMdd"),
                              //        Ean = pc.EANID,
                              //        StateCode = request.ResponseLast.Status == EdsnResponseCallStatus.Ok ? "OK" : request.ResponseLast.RejectionCode,
                              //        StateText = request.ResponseLast.Status == EdsnResponseCallStatus.Ok ? "" : request.ResponseLast.RejectionText,
                              //        RaisedOn = DateTime.UtcNow
                              //    });
                          }
                          catch (Exception e)
                          {
                              request.LogResponseException(e);
                          }
                          finally
                          {
                              _detailRequestService.Replace(request);
                          }

                          break;
                      case P4CollectedDataBatchResultResponseEnvelope_QueryReasonTypeCode.RCY:
                      case P4CollectedDataBatchResultResponseEnvelope_QueryReasonTypeCode.DAY:

                          var request1 = _dayRequestService.RegisterResponseReceived(pc.EANID, pc.QueryDate, callId, resp.EDSNBusinessDocumentHeader.CreationTimestamp).GetAwaiter().GetResult();


                          try
                          {
                              _dayRequestService.ProcessAndSaveEan(pc, request1).GetAwaiter().GetResult();

                              if (request1.ResponseLast.Status == EdsnResponseCallStatus.Ok)
                              {
                                  _dayRequestService.RaiseReadingsReceived(pc.EANID, pc.QueryDate).GetAwaiter().GetResult();
                              }
                              else if (request1.ResponseLast.Status == EdsnResponseCallStatus.Rejected)
                              {
                                  _detailRequestService.RaiseRejectionReceived(new RejectionMessage
                                  {
                                      Ean = pc.EANID,
                                      QueryDate = pc.QueryDate.ToString("yyyyMMdd"),
                                      RejectionCode = request1.ResponseLast.RejectionCode,
                                      RejectionText = request1.ResponseLast.RejectionText,
                                      QueryReason = "DAY"
                                  });
                              }

                              //_dayRequestService.RaiseMpStatusReceived(
                              //    new MpStatusQMsg
                              //    {
                              //        QueryDate = pc.QueryDate.ToString("yyyyMMdd"),
                              //        Ean = pc.EANID,
                              //        StateCode = request1.ResponseLast.Status == EdsnResponseCallStatus.Ok ? "OK" : request1.ResponseLast.RejectionCode,
                              //        StateText = request1.ResponseLast.Status == EdsnResponseCallStatus.Ok ? "" : request1.ResponseLast.RejectionText,
                              //        RaisedOn = DateTime.UtcNow
                              //    });
                          }
                          catch (Exception e)
                          {
                              request1.LogResponseException(e);
                          }
                          finally
                          {
                              _dayRequestService.Replace(request1);
                          }
                          break;
                      default:
                          throw new Exception("pc.QueryReason is not DAY or INT or RCY");
                  }


                  //}

              });
        }
    }
}