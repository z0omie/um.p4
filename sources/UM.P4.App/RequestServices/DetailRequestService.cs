using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UM.P4.CoreTypes;
using UM.P4.CoreTypes.Messages;
using UM.P4.EDSN;
using UM.P4.Mongo;
using UM.P4.Mongo.Models.P4;
using UM.P4.Mongo.Models.Requests;

namespace UM.P4.App
{
    public class DetailRequestService : RequestServiceBase<DetailRequests>
    {

        private readonly P4DetailReadingService _p4Service;
        public DetailRequestService(RequestsRepository<DetailRequests> requestsRepo, MeteringPointRepository mpRepo,
            P4DetailReadingService readingsService, ILogger log)
            : base(requestsRepo, mpRepo, log)
        {
            _p4Service = readingsService;
        }


        protected override bool ValidateMeter(P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter meter, DetailRequests request)
        {
            var nrOfRegisters = meter.P4Register.Length;
            bool isElectra;
            switch (nrOfRegisters)
            {
                case 2:
                    isElectra = true;
                    break;
                case 1:
                    isElectra = false;
                    break;
                default:
                    {
                        request.LogResponseInvalid("nrOfRegisters is not 1 nor 2");
                        return false;
                    }
            }

            var numberOfreadings = meter.P4Register[0].P4Reading.Length;

            var allowedElecAmount = new List<int> { 93, 92, 97, 96, 101, 100 };
            var allowedGasAmount = new List<int> { 25, 24, 23, 26 };

            if (
                (isElectra && !allowedElecAmount.Contains(numberOfreadings))
                ||
                (!isElectra && !allowedGasAmount.Contains(numberOfreadings))
               )
            {
                request.LogResponseInvalid("not enough values!");
                return false;
            }

            return true;
        }


        //protected override void ReadReadings(DetailRequests document, P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register[] registers)
        //{
        //    var numberOfreadings = registers[0].P4Reading.Length;

        //    document.Readings = new List<DetailValues>();
        //    document.Volumes = new List<DetailValues>();

        //    var nrOfRegisters = registers.Length;

        //    for (var i = 0; i < numberOfreadings; i++)
        //    {
        //        var volumesElem = new DetailValues
        //        {
        //            Date = registers[0].P4Reading[i].ReadingDateTime,
        //            TotalValues = new double[nrOfRegisters]
        //        };

        //        var readingsElem = new DetailValues
        //        {
        //            Date = registers[0].P4Reading[i].ReadingDateTime,
        //            TotalValues = new double[nrOfRegisters]
        //        };
        //        for (var j = 0; j < nrOfRegisters; j++)
        //        {
        //            //extract reading
        //            readingsElem.TotalValues[j] = (double)registers[j].P4Reading[i].Reading;

        //            if (readingsElem.TotalValues[j] < 0)
        //            {
        //                throw new MPValidationException("volumes less then 0.");
        //            }

        //            //extract value
        //            volumesElem.TotalValues[j] = i < numberOfreadings - 1
        //                ? (double)(registers[j].P4Reading[i + 1].Reading - registers[j].P4Reading[i].Reading)
        //                : 0;

        //            //validate volume
        //            if (volumesElem.TotalValues[j] < 0)
        //            {
        //                throw new MPValidationException("volumes less then 0.");
        //            }

        //        }
        //        document.Volumes.Add(volumesElem);
        //        document.Readings.Add(readingsElem);
        //    }

        //    FillLastUsageOfPrevDay(document);
        //    FillLastUsageOfCurrentDay(document);
        //}

        public async override Task RaiseReadingsReceived(string eanid, DateTime queryDate)
        {
            var p4Data = await _p4Service.GetReadings(eanid, new List<DateTime> { queryDate });
            if (p4Data.Any())
            {
                var readings = MapFromDb(p4Data.Single());
                RaiseReadingsReceived(readings);
            }
        }

        public async Task RaiseReadingsReceived(string eanid, List<DateTime> queryDates)
        {
            var p4Data = await _p4Service.GetReadings(eanid, queryDates);

            foreach (var p4d in p4Data)
            {
                var readings = MapFromDb(p4d);
                RaiseReadingsReceived(readings);
            }
        }

        public static DetailReadingsMessage MapFromDb(P4DetailReadings dbReading)
        {
            var result = new DetailReadingsMessage
            {
                Ean = dbReading.Ean,
                QueryDate = dbReading.QueryDate.ToString("yyyyMMdd"),
                Readings = new Dictionary<RegisterId, Dictionary<DateTime, double>>()
            };

            foreach (var registers in dbReading.Readings.First().Value)
            {
                result.Readings[registers.Key] =
                    dbReading.Readings.ToDictionary(t => t.DateTime, t => t.Value.Single(v => v.Key == registers.Key).Value);
            }

            return result;
        }

        public override List<DataRequest> GetDataRequestsToSend()
        {
            return RequestsRepo.GetRequestsToSend().Result
            .Select(rs => new DataRequest
            {
                Ean = rs.Ean,
                GridOperator = rs.GridOperatorEan,
                QueryDate = rs.QueryDate,
                QueryReasonId = 1,
                Reference = "123",
                RsId = rs._id
            }).ToList();
        }

        public override int DaysHistory => 11;

        protected override void ProcessAndSaveEanTest(P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint p4MP)
        {
            _p4Service.ProcessAndSaveEanTest(p4MP);
        }



        //private void FillLastUsageOfCurrentDay(DetailRequests currentDoc)
        //{
        //    var nextDoc = ReadingsRepo.Get(currentDoc.GetNextId());
        //    if (nextDoc == null || nextDoc.Readings == null) return;

        //    var currLastVolume = currentDoc.Volumes.Last();
        //    for (var i = 0; i < currLastVolume.TotalValues.Length; i++)
        //    {
        //        currLastVolume.TotalValues[i] = nextDoc.Readings.First().TotalValues[i] - currentDoc.Readings.Last().TotalValues[i];
        //    }

        //    //currentDoc.CalculateDayTotal(isGas);
        //    //nextDoc.VolumesPrevDayTotal = currentDoc.VolumesDayTotal;

        //    //TODO: make update for only one field
        //    ReadingsRepo.Replace(nextDoc);
        //}

        ////TODO: move this to DayVolume Service to insert method. So it is reusable
        //private void FillLastUsageOfPrevDay(DetailRequests currentDoc)
        //{
        //    var prevDoc = ReadingsRepo.Get(currentDoc.GetPrevId());


        //    if (prevDoc == null || prevDoc.Volumes == null || !prevDoc.Volumes.Any()) return;

        //    var prevLastVolume = prevDoc.Volumes.Last();


        //    for (var i = 0; i < prevLastVolume.TotalValues.Length; i++)
        //    {
        //        prevLastVolume.TotalValues[i] = currentDoc.Readings.First().TotalValues[i] - prevDoc.Readings.Last().TotalValues[i];
        //    }

        //    //TODO: make update for only one field
        //    ReadingsRepo.Replace(prevDoc);
        //}

    }
}