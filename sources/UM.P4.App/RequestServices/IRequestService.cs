using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using UM.P4.EDSN;

namespace UM.P4.App
{
    public interface IRequestService<T>
    {
        Task<T> RegisterResponseReceived(string eanid, DateTime queryDate, ObjectId callId, DateTime? createdOn);
        void RegisterRequestSent(List<string> toList, ObjectId edsnResponseCall);

        //void RegisterRequestSent(string toList, ObjectId edsnResponseCall);
        List<DataRequest> GetDataRequestsToSend();
        Tuple<int, int> MakeRequest();

        void AddNewRequest(string ean, string go, DateTime queryDate, DateTime createdOn);


        Task ProcessAndSaveEan(P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint pc, T request);
        //void LogRejection(string eanid, DateTime queryDate, string rejectionCode, string rejectionText);
        //void LogInvalidMP(string eanid, DateTime queryDate, string message);
        //void LogError(string eanid, DateTime queryDate, string toString);
        Task RaiseReadingsReceived(string eanid, DateTime queryDate);
    }
}