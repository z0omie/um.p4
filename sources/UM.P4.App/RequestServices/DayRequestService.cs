﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using UM.P4.CoreTypes;
using UM.P4.CoreTypes.Messages;
using UM.P4.EDSN;
using UM.P4.Mongo;
using UM.P4.Mongo.Models.P4;
using UM.P4.Mongo.Models.Requests;
using UM.P4.Mongo.Models.RR;
using UM.P4.RabbitMQ;

namespace UM.P4.App
{
    public class DayRequestService : RequestServiceBase<DayRequests>
    {
        private readonly P4DayReadingService _p4Service;
        public DayRequestService(RequestsRepository<DayRequests> requestsRepo, MeteringPointRepository mpRepo, P4DayReadingService readingsService, ILogger log) : base(requestsRepo, mpRepo, log)
        {
            _p4Service = readingsService;
        }

        protected override bool ValidateMeter(P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter meter, DayRequests request)
        {
            var nrOfRegisters = meter.P4Register.Length;

            if (nrOfRegisters != 1 && nrOfRegisters != 4)
            {
                request.LogResponseInvalid("nrOfRegisters is not 1 nor 4");
                return false;
            }

            if (meter.P4Register.Any(register => register.P4Reading.Length < 1))
            {
                request.LogResponseInvalid("not enough values!");
                return false;
            }

            return true;
        }


        public async override Task RaiseReadingsReceived(string eanid, DateTime queryDate)
        {
            var p4Data = await _p4Service.GetReadings(eanid, new List<DateTime> { queryDate });
            var readings = MapFromDb(p4Data.Single());
            RaiseReadingsReceived(readings);
        }

        public static DayReadingsMessage MapFromDb(P4DayReadings dbReading)
        {
            var result = new DayReadingsMessage
            {
                Ean = dbReading.Ean,
                QueryDate = dbReading.QueryDate.ToString("yyyyMMdd"),
                Readings = dbReading.Readings
            };

            return result;
        }

        public override List<DataRequest> GetDataRequestsToSend()
        {
            return RequestsRepo.GetRequestsToSend().Result
            .Select(rs => new DataRequest
            {
                Ean = rs.Ean,
                GridOperator = rs.GridOperatorEan,
                QueryDate = rs.QueryDate,
                QueryReasonId = (byte)rs.DayRequestType,
                Reference = rs._id,
                RsId = rs._id
            }).ToList();
        }

        public override int DaysHistory => 41;


        public override DayRequests AddNewRequest(MeteringPoint mp, DateTime queryDate, DateTime? createdOn = null)
        {
            var dr = new DayRequests
            {
                GridOperatorEan = mp.GridOperatorId,
                RequestHistory = new List<EdsnRequestCall>(),
                ResponseHistory = new List<EdsnResponseCall>(),
                _id = RequestBase.BuildId(mp.EanId, queryDate),
                DayRequestType = queryDate > DateTime.Today.AddDays(-40) ? DayRequestType.Day : DayRequestType.Rcy,
                CreatedOn = createdOn ?? DateTime.Now
            };

            dr.AppendNewRequest(dr.CreatedOn);

            RequestsRepo.InsertNewRequest(dr);

            return dr;
        }

        public int GenerateRecoveryRequests(List<MeteringPoint> mps)
        {
            int totalCount = 0;
            Parallel.ForEach(mps, new ParallelOptions { MaxDegreeOfParallelism = 20 }, mp =>
            {
                //    foreach (var mp in mps)
                //{
                //check what we have missed from last 13 months
                //MonitoredFrom should be the same as MoveInDate on address
                var startDate = mp.MonitoredFrom;

                var datesShouldBe = new List<DateTime>();

                var currentStartMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                for (int i = 1; i < 15 && currentStartMonth.AddMonths(-i) > startDate; i++)
                {
                    datesShouldBe.Add(currentStartMonth.AddMonths(-i));
                }

                var datesInDb = GetRequestDates(mp.EanId, datesShouldBe);

                var datesMissed = datesShouldBe.Where(d => !datesInDb.Contains(d)).ToList();

                //add missing requests from last requested day till now. To the Db and to "request collection"
                datesMissed.ForEach(dateMissed => AddNewRequest(mp, dateMissed));

                totalCount += datesMissed.Count();
                //}
            });

            return totalCount;
        }

        protected override void ProcessAndSaveEanTest(P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint p4MP)
        {
            _p4Service.ProcessAndSaveEanTest(p4MP);
        }
    }
}