﻿using UM.P4.CoreTypes;
using UM.P4.EDSN;
using UM.P4.Mongo;
using UM.P4.Mongo.Models.RR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using RabbitMQ.Client;
using UM.P4.Mongo.Models.P4;
using UM.P4.Mongo.Models.Requests;
using UM.P4.RabbitMQ;
using Newtonsoft.Json;
using System.Text;
using UM.P4.CoreTypes.Messages;
using System.Threading;
using Microsoft.Extensions.Logging;

namespace UM.P4.App
{
    public abstract class RequestServiceBase<T> : IRequestService<T>
        where T : RequestBase, new()
    {
        protected readonly RequestsRepository<T> RequestsRepo;
        protected readonly MeteringPointRepository MpRepo;
        protected readonly ILogger _logger;

        protected RequestServiceBase(RequestsRepository<T> requestsRepo, MeteringPointRepository mpRepo, ILogger logger)
        {
            RequestsRepo = requestsRepo;
            MpRepo = mpRepo;
            _logger = logger;
        }

        protected abstract bool ValidateMeter(P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter meter, T request);

        private bool ValidateMP(P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint p4MP, T request)
        {
            if (p4MP.Items.Count() > 1)
            {
                request.LogResponseInvalid("More then 2 meters");
                return false;
            }

            var meter = (P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter)p4MP.Items[0];

            return ValidateMeter(meter, request);
        }


        public Tuple<int, int> MakeRequest()
        {
            _logger.LogInformation("Collecting requests to send...");
            var okResultCount = 0;
            var failedResultCount = 0;
            //collect requests To Send
            List<DataRequest> toSend = GetDataRequestsToSend();

            _logger.LogInformation($"Total to send {toSend.Count()}");

            // Make requests per 4k eans. 
            // According to EDSN 5k - is the max. There is also some size limitations.
            // Well, with 4k it is working already for a long time ok.
            // So please do not change it if it is working ok.
            var bacthes = toSend.Select((item, inx) => new { item, inx })
                .GroupBy(x => x.inx / 4000)
                .Select(g => g.Select(x => x.item))
                .ToList();


            _logger.LogInformation($"Total batches {bacthes.Count()}");

            foreach (var batch in bacthes)
            {
                IResponseInfo t = null;
                ObjectId callId = ObjectId.GenerateNewId();

                // sending all requests to the same GO. As requested by Rens
                t = EdsnProxy.CreateDataRequest(batch, "8712423010208", callId);

                if (t.IsSuccess)
                {
                    RegisterRequestSent(batch.Select(e => e.RsId).ToList(), callId);
                    okResultCount += batch.Count();
                }
                else
                {
                    RegisterSendFailed(batch.Select(e => e.RsId).ToList(), callId);
                    failedResultCount += batch.Count();
                }

            }

            return new Tuple<int, int>(okResultCount, failedResultCount);
        }

        public void RestoreRequests(string xmlContent)
        {





        }




        public async Task ProcessAndSaveEan(P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint p4MP, T request)
        {
            //check for rejection first
            var rejection = p4MP.Items[0] as P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4Rejection;
            if (rejection != null)
            {
                request.LogResponseRejection(rejection.Rejection.RejectionCode.Replace("Code", ""), rejection.Rejection.RejectionText);
                return;
            }

            //validate MP
            if (!ValidateMP(p4MP, request)) return;

            var document = await RequestsRepo.Get(p4MP.EANID, p4MP.QueryDate) ?? new T { _id = RequestBase.BuildId(p4MP.EANID, p4MP.QueryDate) };

            // calling implementation to extract readings
            ProcessAndSaveEanTest(p4MP);

            request.LogResponseOk();

            RequestsRepo.Replace(document);

            var meter = (P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter)p4MP.Items[0];
            await MpRepo.AddMeter(p4MP.EANID, new EnergyMeter { CreatedOn = DateTime.Now, Id = meter.ID });
        }

        protected abstract void ProcessAndSaveEanTest(P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint p4MP);


        public void RaiseReadingsReceived(object readings)
        {
            PushToX(readings, RbtMq.Exchanges.P4Readings);
        }

        public void RaiseRejectionReceived(RejectionMessage readings)
        {
            PushToX(readings, RbtMq.Exchanges.P4Rejection);
        }

        private void PushToX(object objToPush, RbtMq.Exchanges x)
        {
            using (var channel = RbtMq.CreateModel())
            {
                var message = JsonConvert.SerializeObject(objToPush);

                var body = Encoding.UTF8.GetBytes(message);

                var props = channel.CreateBasicProperties();
                props.Type = objToPush.GetType().Name.Replace("Message", "");

                channel.BasicPublish(exchange: RbtMq.XNames[x],
                    routingKey: "",
                    basicProperties: props,
                    body: body);
            }
        }

        public abstract Task RaiseReadingsReceived(string eanid, DateTime queryDate);

        public abstract List<DataRequest> GetDataRequestsToSend();

        public void Replace(T mes)
        {
            RequestsRepo.Replace(mes);
        }


        public long RequestRejectionAgain(string rejectionCode, string ean = null)
        {
            var toReRequest = RequestsRepo.GetRejectedByCodeAndEan(ean, rejectionCode, DaysHistory);
            AppendNewRequests(toReRequest);

            return toReRequest.Count;
        }

        public long RequestInvalidAgain()
        {
            var toReRequest = RequestsRepo.GetInvalid();
            AppendNewRequests(toReRequest);

            return toReRequest.Count;
        }

        public long RequestFailedAgain()
        {
            var toReRequest = RequestsRepo.GetByStatus(EdsnRequestCallStatus.SubmitFailed);
            //var oneTioReRequest = RequestsRepo.Get("871687110002993268", new DateTime(2022, 05, 23)).GetAwaiter().GetResult();
            //var toReRequest = new List<T> { oneTioReRequest };
            _logger.LogInformation("total to rerequest: " + toReRequest.Count());
            AppendNewRequests(toReRequest);
            _logger.LogInformation("DONE");

            return toReRequest.Count;
        }

        public long RequestAgain(string ean, List<DateTime> queryDates)
        {
            var toReRequest = RequestsRepo.Get(ean, queryDates);
            AppendNewRequests(toReRequest);

            return toReRequest.Count;
        }

        public void AppendNewRequests(List<T> requests)
        {
            foreach (var rejectedRequest in requests)
            {
                rejectedRequest.AppendNewRequest();
                RequestsRepo.Replace(rejectedRequest);
            }
        }



        public long ReRequestNotAnswered(int daysInThePast)
        {
            var toReRequest = RequestsRepo.GetNotAnswered(daysInThePast);
            AppendNewRequests(toReRequest);

            return toReRequest.Count;
        }

        public int GenerateNewRequests(List<MeteringPoint> monitoredEans)
        {
            object sync = new object();
            int result = 0;
            //foreach (var mp in monitoredEans)
            //{
            Parallel.ForEach(monitoredEans, new ParallelOptions { MaxDegreeOfParallelism = 20 }, mp =>
            {
                //TODO: will monitoredFrom be set during Meter validation process

                // check what we have missed from last 40 days
                // MonitoredFrom should be the same as MoveInDate on address
                var startDate = mp.MonitoredFrom.Date;

                // if more then DaysHistory days
                if (DateTime.Today - startDate > new TimeSpan(DaysHistory, 0, 0, 0))
                {
                    startDate = DateTime.Today.AddDays(-DaysHistory + 1);
                }

                // compare with last daysHistory requests
                var datesShouldBe = Enumerable.Range(0, (DateTime.Today - startDate).Days + 1)
                    .Select(x => startDate.AddDays(x))
                    .ToList();

                // TODO: this should check readings of they available
                var datesInDb = GetRequestDates(mp.EanId, datesShouldBe);

                var datesMissed = datesShouldBe.Where(d => !datesInDb.Contains(d)).ToList();

                lock (sync)
                {
                    result += AddNewRequests(mp, datesMissed);
                }
            });

            return result;
        }


        public int GenerateNewRequestsForDate(List<MeteringPoint> monitoredEans, DateTime date)
        {
            object sync = new object();
            int result = 0;
            //foreach (var mp in monitoredEans)
            //{
            Parallel.ForEach(monitoredEans, new ParallelOptions { MaxDegreeOfParallelism = 20 }, mp =>
            {
                lock (sync)
                {
                    result += AddNewRequests(mp, new List<DateTime> { date });
                }
            });

            return result;
        }


        protected List<DateTime> GetRequestDates(string eanId, List<DateTime> datesShouldBe)
        {
            return RequestsRepo.GetRequestDates(eanId, datesShouldBe).Select(P4Readings.GetQueryDate).ToList();
        }

        public abstract int DaysHistory { get; }

        public virtual int AddNewRequests(MeteringPoint mp, List<DateTime> queryDates, DateTime? createdOn = null)
        {
            //List<T> reqs = new List<T>();

            int result = 0;

            foreach (var queryDate in queryDates)
            {
                //Parallel.ForEach(queryDates, new ParallelOptions { MaxDegreeOfParallelism = 1 }, queryDate =>
                //{
                var id = RequestBase.BuildId(mp.EanId, queryDate);
                var req = RequestsRepo.Get(id).Result ?? new T
                {
                    GridOperatorEan = mp.GridOperatorId,
                    RequestHistory = new List<EdsnRequestCall>(),
                    ResponseHistory = new List<EdsnResponseCall>(),
                    _id = RequestBase.BuildId(mp.EanId, queryDate),
                    CreatedOn = createdOn ?? DateTime.Now
                };

                req.AppendNewRequest(req.CreatedOn);
                //reqs.Add(req);

                RequestsRepo.Replace(req);

                Interlocked.Increment(ref result);
            }
            //});


            //}

            //RequestsRepo.ReplaceMany(reqs);

            return result;
        }

        public virtual T AddNewRequest(MeteringPoint mp, DateTime queryDate, DateTime? createdOn = null)
        {
            if (!createdOn.HasValue)
            {

            }
            var id = RequestBase.BuildId(mp.EanId, queryDate);
            var req = RequestsRepo.Get(id).Result ?? new T
            {
                GridOperatorEan = mp.GridOperatorId,
                RequestHistory = new List<EdsnRequestCall>(),
                ResponseHistory = new List<EdsnResponseCall>(),
                _id = RequestBase.BuildId(mp.EanId, queryDate),
                CreatedOn = createdOn ?? DateTime.Now
            };

            req.AppendNewRequest(req.CreatedOn);
            RequestsRepo.Replace(req);

            return req;
        }

        public virtual void AddNewRequest(string ean, string go, DateTime queryDate, DateTime createdOn)
        {
            var dr = new T
            {
                GridOperatorEan = go,
                RequestHistory = new List<EdsnRequestCall>(),
                ResponseHistory = new List<EdsnResponseCall>(),
                _id = RequestBase.BuildId(ean, queryDate),
                CreatedOn = createdOn
            };

            RequestsRepo.InsertNewRequest(dr);
        }

        public List<T> GetRejectedReadings(string ean, string rejectionCode)
        {
            return RequestsRepo.GetRejections(ean, rejectionCode);
        }

        public T GetFirstRejection(string ean, string rejectionCode)
        {
            return RequestsRepo.GetFirstRejection(ean, rejectionCode);
        }

        public bool Any(string ean)
        {
            return RequestsRepo.GetLast(ean, 1).Any();
        }

        public void RegisterRequestSent(List<string> ids, ObjectId callId)
        {
            var t = RequestsRepo.RegisterRequestSent(ids, callId).Result;
        }

        public void RegisterSendFailed(List<string> toList, ObjectId callId)
        {
            var t = RequestsRepo.RegisterSendFailed(toList, callId).Result;
        }

        public async Task<T> RegisterResponseReceived(string eanid, DateTime queryDate, ObjectId callId, DateTime? createdOn = null)
        {
            var id = RequestBase.BuildId(eanid, queryDate);
            var request = await RequestsRepo.Get(id);
            if (request == null)
            {
                request = new T { _id = id };

            }
            request.AppendNewResponse(callId, createdOn);
            RequestsRepo.Replace(request);
            return request;
        }
    }
}
