﻿using System;
using System.Configuration;
using RabbitMQ.Client;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UM.P4.RabbitMQ
{
	public static class RbtMq
	{
		private static readonly Lazy<IConnection> Connection =
			new Lazy<IConnection>(() =>
			(new ConnectionFactory
			{
				HostName = ConfigurationManager.AppSettings["Rmq_HostName"],
				UserName = ConfigurationManager.AppSettings["Rmq_UserName"],
				Password = ConfigurationManager.AppSettings["Rmq_Password"]
			}).CreateConnection());

		public enum Exchanges
		{
			P4Readings,
			P4Rejection,

			DayReadings,
			DetailReadings,
			DetailRejections
		}

		public static readonly ReadOnlyDictionary<Exchanges, string> XNames;

		static RbtMq()
		{
			var tempDic = new Dictionary<Exchanges, string>();
			foreach (Exchanges xChange in Enum.GetValues(typeof(Exchanges)))
			{
				var key = "Rmq_X_" + xChange;

				var xName = ConfigurationManager.AppSettings[key];

				if (string.IsNullOrWhiteSpace(xName))
				{
					throw new Exception($"Exchange '{xChange}' name is not defined in config file!");
				}

				if (!xName.EndsWith("_X"))
				{
					throw new Exception($"Exchange '{xChange}' name is not ending with '_X' !");
				}

				tempDic.Add(xChange, xName);
			}

			XNames = new ReadOnlyDictionary<Exchanges, string>(tempDic);
		}


		public static void EnsureCompletePublisherInfrastructure()
		{
			using (var channel = CreateModel())
			{
				foreach (var xChange in XNames)
				{
					// main x
					channel.ExchangeDeclare(
						exchange: xChange.Value,
						type: "fanout",
						durable: true,
						autoDelete: false,
						arguments: null);
				}
			}
		}

		public static IModel CreateModel()
		{
			return Connection.Value.CreateModel();
		}
	}
}
