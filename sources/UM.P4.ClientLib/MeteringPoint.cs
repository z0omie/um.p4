﻿using System.Collections.Generic;
using UM.P4.CoreTypes;


namespace UM.P4.ClientLib
{
    public class MeteringPoint
    {
        public string EanId;
        public ProductType ProdType;

        public string GridOperatorId;
        public MPState State;

        public List<EnergyMeter> Meters;
    }
}
