﻿using System.Threading.Tasks;
using Autofac;
using UM.P4.Mongo;

namespace UM.P4.ClientLib
{
    public static class P4Client
    {
        public static IContainer c = ContainerConfig.Configure();

        private static readonly MeteringPointRepository MPRepo;

        static P4Client()
        {
            MPRepo = c.Resolve<MeteringPointRepository>();
        }


        public static async Task<MeteringPoint> GetMeteringPoint(string ean)
        {
            Mongo.Models.RR.MeteringPoint mp = await MPRepo.GetMP(ean);

            return new MeteringPoint
            {
                EanId = mp.EanId,
                GridOperatorId = mp.GridOperatorId,
                ProdType = mp.ProdType,
                State = mp.State,
                Meters = mp.Meters
            };
        }
    }
}
