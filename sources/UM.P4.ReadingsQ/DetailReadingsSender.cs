﻿using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace UM.P4.ReadingsQ
{
    public class DetailReadingsSender
    {
        // TEST
        //Microsoft.ServiceBus.Messaging.QueueClient AzureClient = Microsoft.ServiceBus.Messaging.QueueClient.CreateFromConnectionString("Endpoint=sb://umeter-cd.servicebus.windows.net/;SharedAccessKeyName=ListenAndSend;SharedAccessKey=+dUkQaGb/u/Ug6HVlcvqKReZzkIRlFmMRydvag1VkyE=;EntityPath=emails-to-send-tst");

        // PROD
        QueueClient AzureClient = Microsoft.ServiceBus.Messaging.QueueClient.CreateFromConnectionString("Endpoint=sb://edsn-p4.servicebus.windows.net/;SharedAccessKeyName=Send;SharedAccessKey=9IE3Pi4Mnk3gGtzrbMeuSmbaZhO9TLpgXLnLLH5Bl54=;EntityPath=p4-detail-readings");


        public async Task Send(object qMsg)
        {
            string qMsgBody = JsonConvert.SerializeObject(qMsg);
            var WelcomQMessage = new BrokeredMessage(qMsgBody)
            {
                Label = qMsg.GetType().Name
            };

            await AzureClient.SendAsync(WelcomQMessage);
        }
    }
}
