﻿using System;
using MongoDB.Bson;
using UM.P4.CoreTypes;

namespace UM.P4.Contract
{
    public class DayRequest
    {
        public string Ean { get; set; }
        public DateTime Date { get; set; }

        public DayRequestType DayRequestType { get; set; }

        public ObjectId? ReqLastCallId { get; set; }

        public EdsnRequestCallStatus ReqLastCallStatus { get; set; }

        public DateTime ReqLastCallCreatedOn { get; set; }

        public int ReqHistory { get; set; }


        public ObjectId? RespLastCallId { get; set; }

        public EdsnResponseCallStatus? RespLastCallStatus { get; set; }

        public string RejectionCode { get; set; }
        public string RejectionText { get; set; }
        public string ErrorDetails { get; set; }

        public DateTime? RespLastCallCreatedOn { get; set; }

        public int RespHistory { get; set; }


        public DateTime CreatedOn { get; set; }
    }
}