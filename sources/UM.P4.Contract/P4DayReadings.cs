using System;
using System.Collections.Generic;
using UM.P4.CoreTypes;

namespace UM.P4.Contract
{
    public class P4DayReadings : P4Readings
    {
        public Dictionary<RegisterId, double> Readings;


        public static P4Usage operator -(P4DayReadings x, P4DayReadings y)
        {
            if (x.Readings.Count != y.Readings.Count)
            {
                throw new Exception("wtf! Readings.Count not equal");
            }

            if (x.QueryDate == y.QueryDate)
            {
                throw new Exception("wtf! QueryDate is equal");
            }

            var start = x.QueryDate < y.QueryDate ? x : y;
            var end = x.QueryDate < y.QueryDate ? y : x;

            var usage = new P4Usage(start.QueryDate, end.QueryDate, new Dictionary<RegisterId, double>());

            foreach (var startReading in start.Readings)
            {
                usage.RegistersUsage.Add(
                    key: startReading.Key,
                    value: Math.Round(end.Readings[startReading.Key] - startReading.Value, 3));
            }


            // ��� �� ���������, ��� ����� ��������, � �� ����� �� ��������. � ��������� ������.
            // ��� ����� ������ ��� �������������. ����� ������ ������ � ������� ��� ��������� ��� � �������������.
            // ���� ����� �������� � �����, ������������.
            //if (!usage.RegistersUsage.ContainsKey(RegisterId.R181)
            //    && !usage.RegistersUsage.ContainsKey(RegisterId.R182)
            //    && usage.RegistersUsage.ContainsKey(RegisterId.R180))
            //{
            //    //����� ��������� ��� � ��� �����
            //    if (C.IsPeakDate(start.UtcMoment) == C.IsPeakDate(end.UtcMoment.AddMinutes(-1)))
            //    {
            //        usage.RegistersUsage.Add(
            //            key: RegisterId.R181,
            //            value: C.IsPeakDate(start.UtcMoment) ? usage.RegistersUsage[RegisterId.R180] : 0);

            //        usage.RegistersUsage.Add(
            //            key: RegisterId.R182,
            //            value: C.IsPeakDate(start.UtcMoment) ? 0 : usage.RegistersUsage[RegisterId.R180]);
            //    }
            //    else
            //    {
            //        throw new Exception("�� ���, � �� �� ������???");
            //    }
            //}


            //if (!usage.RegistersUsage.ContainsKey(RegisterId.R281)
            //    && !usage.RegistersUsage.ContainsKey(RegisterId.R282)
            //    && usage.RegistersUsage.ContainsKey(RegisterId.R280))
            //{
            //    usage.RegistersUsage.Add(
            //        key: RegisterId.R281,
            //        value: C.IsPeakDate(start.UtcMoment) ? usage.RegistersUsage[RegisterId.R280] : 0);

            //    usage.RegistersUsage.Add(
            //        key: RegisterId.R282,
            //        value: C.IsPeakDate(start.UtcMoment) ? 0 : usage.RegistersUsage[RegisterId.R280]);
            //}



            return usage;
        }
    }
}