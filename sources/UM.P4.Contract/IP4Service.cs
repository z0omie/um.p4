﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace UM.P4.Contract
{
    [ServiceContract]
    public interface IP4Service
    {
        [OperationContract]
        Task StartEans(ICollection<EanBoekItem> eanBookItem, string clientName, DateTime? startAt = null);

        [OperationContract]
        Task StopEans(ICollection<string> ean);

        [OperationContract]
        Task PauseEans(ICollection<string> eans);

        #region readings
        [OperationContract]
        Task<ICollection<P4DetailReadings>> GetDetailReadings(string ean, ICollection<DateTime> dates);

        [OperationContract]
        Task<P4DetailReadings> GetLastDetailReading(string ean);

        [OperationContract]
        Task<P4DayReadings> GetLastDayReading(string ean);

        [OperationContract]
        Task<ICollection<P4DetailReadings>> GetDetailReadingsInInterval(string ean, DateTime dateFrom, DateTime dateTo);

        [OperationContract]
        Task<ICollection<P4DayReadings>> GetDayReadings(string ean, ICollection<DateTime> dates);

        [OperationContract]
        Task<ICollection<GridOperator>> GetGridOperators();

        [OperationContract]
        Task<DateTime?> GetFirstReadingDate(string ean);
        #endregion

        #region requests
        [OperationContract]
        ICollection<DayRequest> GetLastDayRequest(string ean, int limit = 10);

        [OperationContract]
        DateTime GetFirstRejectionDate(string ean, string rejectionCode);

        #endregion

        [OperationContract]
        Task<MeteringPoint> GetMeteringPoint(string ean);

        [OperationContract]
        Task<int> GenerateRecoveryRequests(ICollection<string> eans);
    }
}
