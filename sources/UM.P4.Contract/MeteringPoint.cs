﻿using System.Collections.Generic;
using UM.P4.CoreTypes;

namespace UM.P4.Contract
{
    public class MeteringPoint
    {
        public string EanId;
        public ProductType ProdType;

        public string GridOperatorId;
        public MPState State;

        public int PeakHourEndsWith;

        public List<EnergyMeter> Meters;
    }
}
