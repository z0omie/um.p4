using UM.P4.CoreTypes;

namespace UM.P4.Contract
{
    public class EanBoekItem
    {
        public string EanCode;
        public string Street;
        public string BuildingNr;
        public string BuildingNrEx;
        public string City;
        public string ZipCode;
        public bool IsSpecialConnection;
        public ProductType ProductType;
        public string GridOperatorName;
        public string GridOperatorEan;
    }
}