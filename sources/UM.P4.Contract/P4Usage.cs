﻿using System;
using System.Collections.Generic;
using UM.P4.CoreTypes;

namespace UM.P4.Contract
{
    public class P4Usage
    {
        public DateTime DateFrom;
        public DateTime DateTo;
        public Dictionary<RegisterId, double> RegistersUsage;
        


        
        public P4Usage(DateTime dateFrom, DateTime dateTo, Dictionary<RegisterId, double> registersUsage)
        {
            DateFrom = dateFrom;
            DateTo = dateTo;
            RegistersUsage = registersUsage;
        }

        public P4Usage(KeyValuePair<DateTime, Dictionary<RegisterId, double>> readingFirst, KeyValuePair<DateTime, Dictionary<RegisterId, double>> readingSecond, int? peakHourEndsWith)
        {
            DateFrom = readingFirst.Key;
            DateTo = readingSecond.Key;

            double delivery = Math.Round(readingSecond.Value[RegisterId.R180] - readingFirst.Value[RegisterId.R180], 3);

            // if elec
            if (readingFirst.Value.ContainsKey(RegisterId.R280))
            {
                double redelivery = Math.Round(readingSecond.Value[RegisterId.R280] - readingFirst.Value[RegisterId.R280], 3);
                var isPeak = C.IsPeakDate(readingFirst.Key, peakHourEndsWith.Value);
                RegistersUsage = new Dictionary<RegisterId, double>
                {
                    {RegisterId.R181, isPeak ? 0 : delivery},
                    {RegisterId.R182, isPeak ? delivery : 0},
                    {RegisterId.R281, isPeak ? 0 : redelivery},
                    {RegisterId.R282, isPeak ? redelivery : 0}
                };
            }
            else
            {
                RegistersUsage = new Dictionary<RegisterId, double>
                {
                    {
                        RegisterId.R180, delivery
                    }
                };
            }
        }


        public double TotalDays => (DateTo - DateFrom).TotalDays;

        public double TotalUsage => RegistersUsage.ContainsKey(RegisterId.R180)
            ? RegistersUsage[RegisterId.R180]
            : RegistersUsage[RegisterId.R181] + RegistersUsage[RegisterId.R182];

        public double HighUsage => RegistersUsage[RegisterId.R182];

        public double LowUsage => RegistersUsage[RegisterId.R181];



        public double TotalReDelivery => RegistersUsage.ContainsKey(RegisterId.R280)
            ? RegistersUsage[RegisterId.R280]
            : RegistersUsage[RegisterId.R281] + RegistersUsage[RegisterId.R282];

        public double HighReDelivery => RegistersUsage[RegisterId.R282];

        public double LowReDelivery => RegistersUsage[RegisterId.R281];
    }
}