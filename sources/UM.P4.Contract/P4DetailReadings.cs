﻿using System;
using System.Collections.Generic;
using System.Linq;
using UM.P4.CoreTypes;

namespace UM.P4.Contract
{
    public class P4DetailReadings : P4Readings
    {
        public Dictionary<DateTime, Dictionary<RegisterId, double>> Readings;

        public List<P4Usage> GetDetailUsage(int? peakHourEndsBy = null)
        {
            var result = new List<P4Usage>();
            for (var i = 1; i < Readings.Count; i++)
            {
                result.Add(new P4Usage(Readings.ElementAt(i - 1), Readings.ElementAt(i), peakHourEndsBy));
            }

            return result;
        }

        public P4Usage GetDayUsage(int? peakHourEndsBy = null)
        {
            Dictionary<RegisterId, double> t = GetDetailUsage(peakHourEndsBy)
                                                .SelectMany(d => d.RegistersUsage)
                                                .GroupBy(f => f.Key)
                                                .ToDictionary(a => a.Key, a => Math.Round(a.Sum(d => d.Value), 3));

            return new P4Usage(QueryDate, QueryDate.AddDays(1), t);
        }
    }
}