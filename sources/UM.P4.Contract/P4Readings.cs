﻿using System;

namespace UM.P4.Contract
{
    public abstract class P4Readings
    {
        public string Ean;
        public DateTime QueryDate;
    }
}