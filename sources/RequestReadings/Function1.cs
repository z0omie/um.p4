using System;
using Autofac;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using UM.P4.App;
using UM.P4.RabbitMQ;

namespace RequestReadings
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static void Run([TimerTrigger("0 30 16 * * *"
            #if DEBUG
                , RunOnStartup= true
            #endif
            )]TimerInfo myTimer
         , ILogger log)
        {
            IContainer c = ContainerConfig.Configure(log);

            var requester = c.Resolve<Requester>();

            RbtMq.EnsureCompletePublisherInfrastructure();

            log.LogInformation("starting...");
            long result = 0;
            //result += requester.ReRequestMandateRenewal("871687110002993268");

            result += requester.GenerateNewRequests();
            result += requester.ReRequestNotAnswered();
            result += requester.ReRequestRejections("008", "012", "014");

            log.LogInformation($"done generating new requests. {result}");

            log.LogInformation($"Making requests...");
            var reqResult = requester.MakeRequest();


            //log.Info($"C# Timer trigger function executed at: {DateTime.Now}");
        }
    }
}
