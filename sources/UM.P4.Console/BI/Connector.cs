﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace UM.P4.Console.BI
{
	class BiMongoCollection<T>
		where T : IMongoEntity
	{
		private readonly IMongoDatabase _database;
		private readonly string _collectionName;

		public BiMongoCollection(IMongoDatabase database, string collectionName)
		{
			_database = database;
			_collectionName = collectionName;
		}

		private IMongoCollection<T> Collection => _database.GetCollection<T>(_collectionName);
		public async Task AddReplace(T item)
		{
			await Collection.ReplaceOneAsync(t => t.Id == item.Id, item, new UpdateOptions { IsUpsert = true });
		}

		public async Task InsertMany(List<T> items)
		{
			await Collection.InsertManyAsync(items, new InsertManyOptions { BypassDocumentValidation = true });
		}


		public IMongoQueryable<T> GetFiltered(Expression<Func<T, bool>> predicate)
		{
			return Collection.AsQueryable().Where(predicate);
		}
	}



	class Connector
	{
		private static IMongoDatabase _database;

		public Connector()
		{
			var client = new MongoClient("mongodb://localhost");
			_database = client.GetDatabase("BIDay");
		}

		public BiMongoCollection<BiAvgUsage> DayELK_NoProd => new BiMongoCollection<BiAvgUsage>(_database, "AvgDayUsageELK_NoProd");
		public BiMongoCollection<BiAvgUsage> DayELK_Prod => new BiMongoCollection<BiAvgUsage>(_database, "AvgDayUsageELK_Prod");
		public BiMongoCollection<BiAvgUsage> DayGAS => new BiMongoCollection<BiAvgUsage>(_database, "AvgDayUsageGAS");

		public BiMongoCollection<BiAvgUsage> MonthELK_NoProd => new BiMongoCollection<BiAvgUsage>(_database, "AvgMonthUsageELK_NoProd");
		public BiMongoCollection<BiAvgUsage> MonthELK_Prod => new BiMongoCollection<BiAvgUsage>(_database, "AvgMonthUsageELK_Prod");
		public BiMongoCollection<BiAvgUsage> MonthGAS => new BiMongoCollection<BiAvgUsage>(_database, "AvgMonthUsageGAS");

		public BiMongoCollection<BiDayEanUsage> DayEansUsage => new BiMongoCollection<BiDayEanUsage>(_database, "DayEanUsage1");
	}
}