﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using UM.P4.CoreTypes;

namespace UM.P4.Console.BI
{
	public class BiDayEanUsage : IMongoEntity
	{
		[BsonId]
		public string Id { get; set; }

		[BsonIgnore]
		public string Date { get; set; }
		[BsonIgnore]
		public string Ean { get; set; }

		public ProductType ProdType { get; set; }

		public string ZipCode { get; set; }
		public string Province { get; set; }

		public string Lat { get; set; }
		public string Lng { get; set; }

		public Dictionary<RegisterId, double> Usage { get; set; }
	}

	public class BiAvgUsage : IMongoEntity
	{
		[BsonId]
		public string Id { get; set; }
		public int Amount { get; set; }

		public Dictionary<RegisterId, double> Usage { get; set; }
	}

	public interface IMongoEntity
	{
		[BsonId]
		string Id { get; set; }
	}

}