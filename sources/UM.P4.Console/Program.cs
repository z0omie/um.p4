using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using MongoDB.Bson;
using Quartz.Impl;
using UM.P4.App;
using UM.P4.Console.BI;
using UM.P4.CoreTypes;
using UM.P4.CoreTypes.Messages;
using UM.P4.Mongo;
using UM.P4.Mongo.Models.P4;
using UM.P4.Mongo.Models.RR;
using UM.P4.RabbitMQ;
using UM.P4.WinService.Jobs;

namespace UM.P4.Console
{
    class Program
    {
        #region DetailReadingMessage



        private static readonly ConcurrentDictionary<string, MeteringPoint> MpCache = new ConcurrentDictionary<string, MeteringPoint>();

        private static BiDayEanUsage MapFromMessage(DetailReadingsMessage source, IContainer c)
        {
            if (source.Usage.ContainsValue(-1))
            {
                return null;
            }


            if (!MpCache.ContainsKey(source.Ean))
            {
                var mpRepo = c.Resolve<MeteringPointRepository>();
                var mpt = mpRepo.GetMP(source.Ean).Result;
                MpCache.TryAdd(source.Ean, mpt);
            }

            var mp = MpCache[source.Ean];

            var date = source.QueryDate;
            return new BiDayEanUsage
            {
                Date = date,
                Ean = source.Ean,
                ProdType = mp.ProdType,
                ZipCode = mp.ZipCode,
                Id = date + ":" + source.Ean,
                Usage = source.Usage.ToDictionary(k => k.Key, v => double.Parse(v.Value.ToString()))
            };
        }

        #endregion

        //private static async Task PublishAllDetailedFrom(IContainer c, DateTime dateFrom)
        //{
        //    DetailRequestService _detailRequestService = c.Resolve<DetailRequestService>();

        //    var repo = c.Resolve<P4Repository<P4DetailReadings>>();
        //    // 1. select from Mongo. Only date and ean.
        //    using (var cursor = await repo.GetCursor())
        //    {
        //        while (await cursor.MoveNextAsync())
        //        {
        //            var batch = cursor.Current;
        //            foreach (P4DetailReadings document in batch)
        //            {
        //                _detailRequestService.RaiseReadingsReceived(DetailRequestService.MapFromDb(document));
        //            }
        //        }
        //    }
        //}

        //private static DayReadingsMessage MapFromDb(P4DayReadings dbReading)
        //{
        //    var result = new DayReadingsMessage
        //    {
        //        Ean = dbReading.Ean,
        //        QueryDate = dbReading.QueryDate.ToString("yyyyMMdd"),
        //        Readings = dbReading.Readings
        //    };

        //    return result;
        //}

        //private static List<P4DayReadings> GetDayReadingFromDb(DateTime date)
        //{
        //    return _p4DayRepo.GetReadings(date).Result;
        //}

        //private static P4Repository<P4DetailReadings> _p4DetRepo;
        //private static P4Repository<P4DayReadings> _p4DayRepo;
        //private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            TelemetryConfiguration configuration = TelemetryConfiguration.CreateDefault();
            configuration.InstrumentationKey = "f027ff51-3510-4dad-8b08-f59a6b170085";
            var telemetryClient = new TelemetryClient(configuration);

            var operation = telemetryClient.StartOperation(new RequestTelemetry { Name = $"Request P4 readings" });

            try
            {
                // creatin Ilogger with .net di.
                var logger = new SimpleConsoleLogger(telemetryClient);

                // passoing logger to autofac
                IContainer c = ContainerConfig.Configure(logger);
                var requester = c.Resolve<Requester>();

                RbtMq.EnsureCompletePublisherInfrastructure();

                logger.LogInformation("starting...");
                long result = 0;
                //result += requester.ReRequestMandateRenewal("871687110002993268");

                //result += requester.GenerateNewRequests();
                //result += requester.ReRequestRejections("008", "012", "014");
                //result += requester.ReRequestFailed();
                result += requester.ReRequestNotAnswered();

                logger.LogInformation($"done generating new requests. {result}");

                logger.LogInformation($"Making requests...");
                //var reqResult = requester.MakeRequest();

                //var reqResult = requester.ReRequestFailed();

                logger.LogInformation($"DA DA DONE!");
                logger.LogInformation($"BYE!");


                //System.Console.ReadLine();
            }
            catch (Exception e)
            {
                telemetryClient.TrackException(e);
            }
            finally
            {
                telemetryClient.StopOperation(operation);
                System.Console.WriteLine("flushing telemetry...");
                telemetryClient.Flush();
                Thread.Sleep(5000);
                System.Console.WriteLine("done");
                Environment.Exit(0);
            }
        }

        //private static void doWork(Receiver receiver, RawCallTypedRepository<ResponseRawCall> rawResponseRepo)
        //{
        //    //var tt = receiver.GetResponseGO("8716892700004");
        //    var batch = rawResponseRepo.Get(2000, "5d4c9b600000000000000000", "5d4dece00000000000000000");

        //    var total = batch.Count();
        //    foreach (var rawCall in batch)
        //    {
        //        try
        //        {
        //            System.Console.WriteLine($"Doing {batch.IndexOf(rawCall)} from {total}. Id: {rawCall._id}");
        //            receiver.ProcessEDSNResponse(rawCall.ResponseXML, rawCall._id, rawCall.ResponseCreatedOn);
        //        }
        //        catch (Exception e)
        //        {
        //            System.Console.WriteLine(e);
        //        }
        //    }

        //}



        //private static void CreateBiDayEanUsagesFromDayReadings(DateTime startDate, IContainer c, Connector BIDBConnector)
        //{
        //    Dictionary<DateTime, List<BiDayEanUsage>> allUsage;
        //    while (startDate < DateTime.Now.AddMonths(1))
        //    {
        //        var endDate = startDate.AddMonths(1);

        //        System.Console.WriteLine("starting month " + startDate.ToString("yyyyMM"));


        //        allUsage = Enumerable.Range(0, (int)(endDate - startDate).TotalDays + 1)
        //            .Select(x => startDate.AddDays(x))
        //            .ToDictionary(k => k, v => new List<BiDayEanUsage>());

        //        // iterate on Date
        //        foreach (var u in allUsage)
        //        {
        //            List<P4DayReadings> readings = GetDayReadingFromDb(u.Key);

        //            foreach (var reading in readings)
        //            {
        //                var prevReading = _p4DayRepo.Get(reading.GetPrevId()).Result;

        //                if (prevReading == null)
        //                {
        //                    continue;
        //                }

        //                var message = MapFromDb(reading);
        //                var messagePrev = MapFromDb(prevReading);
        //                var usage = message.GetUsage(messagePrev);

        //                if (usage.ContainsValue(-1))
        //                {
        //                    continue;
        //                }

        //                var meteringPoint = c.Resolve<MeteringPointRepository>().GetMP(reading.Ean).Result;
        //                var biu = new BiDayEanUsage
        //                {
        //                    Id = reading.QueryDate.ToString("yyyyMMdd") + ":" + meteringPoint.EanId,
        //                    Ean = meteringPoint.EanId,
        //                    Date = reading.QueryDate.ToString("yyyyMMdd"),
        //                    ProdType = meteringPoint.ProdType,

        //                    ZipCode = meteringPoint.ZipCode,
        //                    Usage = usage
        //                };

        //                //save to DB.
        //                BIDBConnector.DayEansUsage.AddReplace(biu).Wait();
        //            }

        //            System.Console.WriteLine("Date " + u.Key + " done!");
        //        }

        //        startDate = endDate;
        //    }
        //}

        //private static void SaveDayAvg(List<BiDayEanUsage> listFrom, string date, BiMongoCollection<BiAvgUsage> dayCollection)
        //{
        //    if (!listFrom.Any()) return;

        //    BiAvgUsage elkUsageProd = new BiAvgUsage
        //    {
        //        Id = date,
        //        Amount = listFrom.Count,
        //    };
        //    elkUsageProd.Usage = new Dictionary<RegisterId, double>();

        //    foreach (var d in listFrom.First().Usage)
        //    {
        //        elkUsageProd.Usage[d.Key] = Math.Round(listFrom.Average(u => u.Usage[d.Key]), 2);
        //    }

        //    dayCollection.AddReplace(elkUsageProd).Wait();
        //}

        //private static void SaveMonthAvg(BiMongoCollection<BiAvgUsage> dayCollection, BiMongoCollection<BiAvgUsage> monthCollection, string datetoSearch)
        //{
        //    var daysUsage = dayCollection.GetFiltered(u => u.Id.StartsWith(datetoSearch)).ToList();

        //    if (!daysUsage.Any())
        //    {
        //        return;
        //    }

        //    BiAvgUsage monthUsage = new BiAvgUsage
        //    {
        //        Id = datetoSearch,
        //        Amount = daysUsage.Count,
        //        Usage = new Dictionary<RegisterId, double>()
        //    };

        //    foreach (var usageKey in daysUsage.First().Usage.Keys)
        //    {
        //        monthUsage.Usage[usageKey] = Math.Round(daysUsage.Sum(u => u.Usage[usageKey]), 2);
        //    }

        //    monthCollection.AddReplace(monthUsage).Wait();
        //}


    }

    //public static class Extentions
    //{
    //    public static IEnumerable<T> SkipPercent<T>(this ICollection<T> source, int percent)
    //    {
    //        int count = source.Count * percent / 100;
    //        return source.Skip(count);
    //    }
    //}
}