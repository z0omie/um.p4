﻿using Microsoft.ApplicationInsights;
using Microsoft.Extensions.Logging;
using System;

namespace UM.P4.Console
{
    internal class SimpleConsoleLogger : ILogger
    {
        public SimpleConsoleLogger(TelemetryClient telemetryClient)
        {
            _telemetryClient = telemetryClient;
        }

        private TelemetryClient _telemetryClient { get; }

        public IDisposable BeginScope<TState>(TState state) { return null; }

        public bool IsEnabled(LogLevel logLevel) { return true; }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            switch (logLevel)
            {
                case LogLevel.Trace:
                    break;
                case LogLevel.Debug:
                    break;
                case LogLevel.Information:
                    break;
                case LogLevel.Warning:
                    break;
                case LogLevel.Error:
                    break;
                case LogLevel.Critical:
                    break;
                case LogLevel.None:
                    break;
                default:
                    break;
            }
            //System.Diagnostics.Debug.WriteLine($"{logLevel} {state.ToString()}");
            System.Console.WriteLine($"[{DateTime.Now}] {logLevel} {state}");

            _telemetryClient.TrackTrace($"{logLevel} {state}");
        }
    }
}
