﻿using System;
using System.Threading.Tasks;
using MongoDB.Bson;
using UM.P4.Mongo;
using UM.P4.Mongo.Models.RR;

namespace UM.P4.Console
{
    public class RawCallConverter
    {
        private readonly RawCallRepository _rawCallRepo;
        private readonly RawCallTypedRepository<RequestRawCall> _rawCallRequestRepo;
        private readonly RawCallTypedRepository<ResponseRawCall> _rawCallResponseRepo;

        public RawCallConverter(RawCallRepository rawCallRepo, RawCallTypedRepository<ResponseRawCall> rawCallResponseRepo, RawCallTypedRepository<RequestRawCall> rawCallRequestRepo)
        {
            _rawCallRepo = rawCallRepo;
            _rawCallResponseRepo = rawCallResponseRepo;
            _rawCallRequestRepo = rawCallRequestRepo;
        }

        public RequestRawCall ConvertRequest(RawCall rc)
        {
            if (rc.ResponseXML == null)
            {
                return null;
            }

            var reqRc = new RequestRawCall(ObjectId.GenerateNewId(), rc.RequestXML, rc.RequestCreatedOn)
            { OldCallId = rc._id };

            reqRc.AttachResponse(rc.ResponseXML, rc.ResponseCreatedOn);

            return reqRc;
        }

        public ResponseRawCall ConvertResponse(RawCall rc)
        {
            if (rc.ResponseXML == null)
            {
                return null;
            }

            var resRc = new ResponseRawCall(ObjectId.GenerateNewId(), rc.RequestXML, rc.RequestCreatedOn)
            { OldCallId = rc._id };

            resRc.AttachResponse(rc.ResponseXML, rc.ResponseCreatedOn);

            return resRc;
        }

        public async Task Run(int take)
        {
            var requests = _rawCallRepo.GetAll(take, new DateTime(2011, 09, 01), new DateTime(2018, 9, 30));

            foreach (var rawCall in requests)
            {
                if (rawCall.Action == "P4CollectedDataBatchRequestEnvelope")
                {
                    var resp = ConvertRequest(rawCall);
                    if (resp == null)
                    {
                        continue;
                    }

                    await _rawCallRequestRepo.InsertOneAsync(resp);
                }
                else
                {
                    var resp = ConvertResponse(rawCall);
                    if (resp == null)
                    {
                        continue;
                    }

                    await _rawCallResponseRepo.InsertOneAsync(resp);
                }

            }


            //var tt = _rawCallRequestRepo.Get(10, new DateTime(2016, 09, 01), new DateTime(2018, 9, 30));
        }
    }
}