﻿using System;
using System.Globalization;
using System.Linq;
using UM.P4.CoreTypes;
using UM.P4.Mongo.Models.P4;

namespace UM.P4.Console
{
    public class PeakTimeDetector
    {
        public int Detect(P4DetailReadings detail, P4DayReadings dayBegin, P4DayReadings dayEnd)
        {
            var day181 = dayEnd.Readings[RegisterId.R182] - dayBegin.Readings[RegisterId.R182];



            var something = detail.Readings.Where(t => t.DateTime.ToNLDateTime().Hour == 7).ToList();

            var detail181_23 =
                detail.Readings.Single(t => t.DateTime.ToNLDateTime().Hour == 23 && t.DateTime.Minute == 0).Value[RegisterId.R180] -
                detail.Readings.Single(t => t.DateTime.ToNLDateTime().Hour == 7 && t.DateTime.Minute == 0).Value[RegisterId.R180];

            var detail181_21 =
                detail.Readings.Single(t => t.DateTime.ToNLDateTime().Hour == 21 && t.DateTime.ToNLDateTime().Minute == 0).Value[RegisterId.R180] -
                detail.Readings.Single(t => t.DateTime.ToNLDateTime().Hour == 7 && t.DateTime.ToNLDateTime().Minute == 0).Value[RegisterId.R180];


            if (day181.ToString(CultureInfo.InvariantCulture) == Math.Round(day181, 0).ToString(CultureInfo.InvariantCulture))
            {
                if (day181 >= detail181_23 / 1000)
                {
                    return 23;
                }
                if (day181 <= detail181_21 / 1000)
                {
                    return 21;
                }
                //if (Math.Abs(day181 - Math.Round(detail181_23 / 1000, 0)) < 0.1)
                //{
                //    return 23;
                //}
                //if (Math.Abs(day181 - Math.Round(detail181_21 / 1000, 0)) < 0.1)
                //{
                //    return 21;
                //}
                return 0;
            }

            if (Math.Abs(day181 - detail181_23 / 1000) < 0.1)
            {
                return 23;
            }
            if (Math.Abs(day181 - detail181_21 / 1000) < 0.1)
            {
                return 21;
            }

            return 0;
        }
    }
}