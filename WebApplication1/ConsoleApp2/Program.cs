﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ConsoleApp2
{
    class Program
    {
        static Random rnd = new Random();
        static string meterId;
        static void Main(string[] args)
        {

            Console.WriteLine("type meterId to start");
            meterId = Console.ReadLine();

            Thread.Sleep(1000);
            StartClient();



            Console.WriteLine("Hello World!");
        }

        public static void StartClient()
        {
            // Data buffer for incoming data.  
            byte[] bytes = new byte[1024];

            // Connect to a remote device.  
            try
            {

                // Create a TCP/IP  socket.  


                // Connect the socket to the remote endpoint. Catch any errors.  
                try
                {
                    int i = 0;
                    double currentReading = 0;

                    while (true)
                    {
                        Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                        sender.Connect("127.0.0.1", 8989);

                        Console.WriteLine("Socket connected to {0}", sender.RemoteEndPoint.ToString());

                        i++;
                        double usage = 0;

                        if (i % 5 == 0)
                        {
                            usage = rnd.Next(999) / (double)100;
                        }
                        

                        currentReading += usage;

                        var mainString = $"1-0:1.8.0({string.Format("{0:000000.00}", currentReading)}*kWh)";
                        Console.WriteLine($"Sending : {mainString}");
                        // Encode the data string into a byte array. 
                        var msg1 = telegramSample
                            .Replace("1-0:1.8.0(000053.77*kWh)", mainString)
                            .Replace("123", meterId);
                        byte[] msg = Encoding.ASCII.GetBytes(msg1);

                        // Send the data through the socket.  
                        int bytesSent = sender.Send(msg);




                        // Release the socket.  
                        sender.Shutdown(SocketShutdown.Both);
                        sender.Dispose();

                        Thread.Sleep(2000);
                    }

                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        static string telegramSample =
@"
123
/ISk5\2ME381-1008

0-0:96.1.0(52149480)
1-0:0.9.1(181523)
1-0:0.9.2(150721)
1-0:1.8.0(000053.77*kWh)
1-0:1.8.1(000053.77*kWh)
1-0:1.8.2(000000.00*kWh)
1-0:1.8.3(000000.00*kWh)
1-0:1.8.4(000000.00*kWh)
1-0:2.8.0(000001.57*kWh)
1-0:3.8.0(000014.84*kvarh)
1-0:3.8.1(000014.84*kvarh)
1-0:3.8.2(000000.00*kvarh)
1-0:3.8.3(000000.00*kvarh)
1-0:3.8.4(000000.00*kvarh)
1-0:4.8.0(000004.32*kvarh)
1-0:1.6.0(000.494*kW)
1-0:1.6.1(000.494*kW)
1-0:1.6.2(000.000*kW)
1-0:1.6.3(000.000*kW)
1-0:1.6.4(000.000*kW)
1-0:2.6.0(000.000*kW)
1-0:2.6.1(000.000*kW)
1-0:2.6.2(000.000*kW)
1-0:2.6.3(000.000*kW)
1-0:2.6.4(000.000*kW)
0-0:97.97.0(01000000)
!";
    }
}
