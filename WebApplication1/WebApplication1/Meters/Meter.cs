﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebApplication1.Meters
{
    public class Meter
    {
        public Meter(string meterId)
        {
            MeterId = meterId;
        }

        public string MeterId;
        public LimitedSizeStack meterData = new LimitedSizeStack(10);

        public event EventHandler<LimitedSizeStack> dataRefreshed;


        public int subs { get { return dataRefreshed == null ? 0 : dataRefreshed.GetInvocationList().Length; } }

        //TODO this should be awaitable
        public void dataReceived(P1Data p1Data)
        {
            meterData.Push(new KeyValuePair<DateTime, double>(p1Data.dateTime, p1Data.reading180));
            dataRefreshed?.Invoke(this, meterData);
        }
    }


    public class LimitedSizeStack : LinkedList<KeyValuePair<DateTime, double>>
    {
        private readonly int _maxSize;
        public LimitedSizeStack(int maxSize)
        {
            _maxSize = maxSize;
        }

        public void Push(KeyValuePair<DateTime, double> item)
        {
            this.AddFirst(item);

            if (this.Count > _maxSize)
                this.RemoveLast();
        }

        public string GetString()
        {
            var result = "";

            foreach (var item in this)
            {
                result += $" {item.Value}";
            }

            return result;
        }
    }
}
