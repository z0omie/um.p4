﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace WebApplication1.Meters
{
    //TODO: rebuild static to singleton using Core DI. Port/IP adres should be a prameter.
    public static class MeterListener
    {
        public static ConcurrentDictionary<string, Meter> meters = new ConcurrentDictionary<string, Meter>();

        public static Meter GetMeter(string meterId)
        {
            Meter m = null;
            meters.TryGetValue(meterId, out m);
            return m;
        }


        private static TcpListener _listener;

        private static MongoClient _client;
        private static IMongoCollection<P1Data> _collection;

        static MeterListener()
        {
            _client = new MongoClient("mongodb://localhost:27017");
            _collection = _client.GetDatabase("db1").GetCollection<P1Data>("p1Data");

            _listener = new TcpListener(IPAddress.Any, 8989);
        }

        public static List<P1Data> getP1Data(string meterId, DateTime? dateTimeFrom, DateTime? dateTimeTo)
        {
            return _collection.Find(t =>
                t.meterId == meterId
                && (dateTimeFrom == null || t.dateTime >= dateTimeFrom)
                && (dateTimeTo == null || t.dateTime <= dateTimeTo))
                .ToList();
        }

        public static List<P1Data> getP1Data(string meterId, DateTime date)
        {
            return _collection.Find(t =>
                t.meterId == meterId
                && t.dateTime >= date
                && t.dateTime <= date.AddDays(1))
                .ToList();
        }


        public static void Start(CancellationToken token)
        {
            _listener.Start();
            Byte[] bytes = new Byte[1024];

            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    break;
                }

                var clientTask = _listener.AcceptTcpClientAsync().Result;

                if (clientTask != null)
                {
                    var client = clientTask;

                    NetworkStream stream = client.GetStream();

                    using (MemoryStream ms = new MemoryStream())
                    {
                        int numBytesRead;
                        string stra = string.Empty;
                        while ((numBytesRead = stream.Read(bytes, 0, bytes.Length)) > 0)
                        {
                            ms.Write(bytes, 0, numBytesRead);
                            stra = Encoding.ASCII.GetString(ms.ToArray(), 0, (int)ms.Length);
                        }

                        var p1Data = ParseTelegram(stra);

                        var meter = meters.GetOrAdd(p1Data.meterId, new Meter(p1Data.meterId));
                        meter.dataReceived(p1Data);
                    }
                    client.GetStream().Dispose();
                }
            }
        }

        private static P1Data ParseTelegram(string telegram)
        {
            var result = new P1Data
            {
                _id = ObjectId.GenerateNewId(),
                dateTime = DateTime.UtcNow,
                raw = telegram
            };

            var filter = Builders<P1Data>.Filter.Eq("_id", result._id);
            _collection.ReplaceOne(filter, result, new UpdateOptions { IsUpsert = true });

            result.meterId = telegram.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries)[0];
            var position180 = telegram.IndexOf("1-0:1.8.0(");
            result.reading180 = double.Parse(telegram.Substring(position180 + 10, 9), CultureInfo.InvariantCulture);

            _collection.ReplaceOne(filter, result, new UpdateOptions { IsUpsert = true });

            return result;
        }


        static string telegramSample =
@"
01012017-1.01-1-1024-01234567
/ISk5\2ME381-1008

0-0:96.1.0(52149480)
1-0:0.9.1(181523)
1-0:0.9.2(150721)
1-0:1.8.0(000053.77*kWh)
1-0:1.8.1(000053.77*kWh)
1-0:1.8.2(000000.00*kWh)
1-0:1.8.3(000000.00*kWh)
1-0:1.8.4(000000.00*kWh)
1-0:2.8.0(000001.57*kWh)
1-0:3.8.0(000014.84*kvarh)
1-0:3.8.1(000014.84*kvarh)
1-0:3.8.2(000000.00*kvarh)
1-0:3.8.3(000000.00*kvarh)
1-0:3.8.4(000000.00*kvarh)
1-0:4.8.0(000004.32*kvarh)
1-0:1.6.0(000.494*kW)
1-0:1.6.1(000.494*kW)
1-0:1.6.2(000.000*kW)
1-0:1.6.3(000.000*kW)
1-0:1.6.4(000.000*kW)
1-0:2.6.0(000.000*kW)
1-0:2.6.1(000.000*kW)
1-0:2.6.2(000.000*kW)
1-0:2.6.3(000.000*kW)
1-0:2.6.4(000.000*kW)
0-0:97.97.0(01000000)
!";
    }

    public class P1Data : EventArgs
    {
        [BsonId]
        public ObjectId _id;
        public string meterId;
        public DateTime dateTime;
        public double reading180;
        public string raw;
    }
}
