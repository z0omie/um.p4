﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Meters;

namespace WebApplication1.Sockets
{
    public class WebSocketManagerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly WebSocketConnectionManager _webSocketHandler;
        
        public WebSocketManagerMiddleware(RequestDelegate next, WebSocketConnectionManager webSocketHandler)
        {
            _next = next;
            _webSocketHandler = webSocketHandler;
        }


        public async Task Invoke(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest)
            {
                await _next.Invoke(context);
                return;
            }

            var socket = await context.WebSockets.AcceptWebSocketAsync();

            var meterId = context.Request.Query["meterid"].FirstOrDefault();

            if (string.IsNullOrWhiteSpace(meterId))
            {
                await socket.SendMsgAsync("no meterid provided");
                return;
            }

            var connectedMeter = MeterListener.GetMeter(meterId);
            if (connectedMeter == null)
            {
                await socket.SendMsgAsync($"meter with id {meterId} is not connected");
                return;
            }

            var tt = new MeterClientSocket { socket = socket, meterId = meterId };
            _webSocketHandler.AddSocket(tt);

            connectedMeter.dataRefreshed += tt.HandleDataRefreshedAsync;

            await Receive(socket, async (result, buffer) =>
            {
                
                if (result.MessageType == WebSocketMessageType.Close)
                {
                    var myS = _webSocketHandler.GetSocket(socket);
                    connectedMeter.dataRefreshed -= myS.HandleDataRefreshedAsync;

                    await _webSocketHandler.RemoveSocket(socket);
                    return;
                }
            });

            //;
        }



        private async Task Receive(WebSocket socket, Action<WebSocketReceiveResult, byte[]> handleMessage)
        {
            var buffer = new byte[1024 * 4];

            while (socket.State == WebSocketState.Open)
            {
                var result = await socket.ReceiveAsync(buffer: new ArraySegment<byte>(buffer),
                                                       cancellationToken: CancellationToken.None);

                handleMessage(result, buffer);
            }
        }



    }
}

