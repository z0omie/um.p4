﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.WebSockets;
using System.Reflection;
using System.Text;
using System.Threading;

namespace WebApplication1.Sockets
{
    public static class Extentions
    {
        public static IApplicationBuilder MapWebSocketManager(this IApplicationBuilder app,
                                                              PathString path,
                                                              WebSocketConnectionManager handler)
        {
            return app.Map(path, (_app) => _app.UseMiddleware<WebSocketManagerMiddleware>(handler));
        }


        public static IServiceCollection AddWebSocketManager(this IServiceCollection services)
        {
            services.AddTransient<WebSocketConnectionManager>();

            foreach (var type in Assembly.GetEntryAssembly().ExportedTypes)
            {
                if (type.GetTypeInfo().BaseType == typeof(WebSocketConnectionManager))
                {
                    services.AddSingleton(type);
                }
            }

            return services;
        }

        public static async System.Threading.Tasks.Task SendMsgAsync(this WebSocket socket, string message)
        {
            await socket.SendAsync(buffer: new ArraySegment<byte>(array: Encoding.ASCII.GetBytes(message),
                                                      offset: 0,
                                                      count: message.Length),
                       messageType: WebSocketMessageType.Text,
                       endOfMessage: true,
                       cancellationToken: CancellationToken.None);
        }
    }
}
