﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Sockets;

namespace WebApplication1
{
    public class WebSocketConnectionManager
    {
        private ConcurrentDictionary<string, MeterClientSocket> _sockets = new ConcurrentDictionary<string, MeterClientSocket>();

        public int CountSockets()
        {
            return _sockets.Count;
        }
        public MeterClientSocket GetSocketById(string id)
        {
            return _sockets.FirstOrDefault(p => p.Key == id).Value;
        }

        public MeterClientSocket GetSocket(WebSocket socket)
        {
            return _sockets.FirstOrDefault(p => p.Value.socket == socket).Value;
        }

        public ConcurrentDictionary<string, MeterClientSocket> GetAll()
        {
            return _sockets;
        }

        public string GetId(MeterClientSocket socket)
        {
            return _sockets.FirstOrDefault(p => p.Value == socket).Key;
        }
        public string GetId(WebSocket socket)
        {
            return _sockets.FirstOrDefault(p => p.Value.socket == socket).Key;
        }

        public void AddSocket(MeterClientSocket socket)
        {
            _sockets.TryAdd(CreateConnectionId(), socket);
        }

        public async Task RemoveSocket(WebSocket socket)
        {
            var id = GetId(socket);

            MeterClientSocket s1 = null;
            _sockets.TryRemove(id, out s1);

            await socket.CloseAsync(closeStatus: WebSocketCloseStatus.NormalClosure,
                                    statusDescription: "Closed by the WebSocketManager",
                                    cancellationToken: CancellationToken.None);
        }

        private string CreateConnectionId()
        {
            return Guid.NewGuid().ToString();
        }
    }

    public class MeterWebnSocket
    {
        public readonly WebSocket socket;
        public readonly string meterId;
    }
}
