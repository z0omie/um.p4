﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Meters;

namespace WebApplication1.Sockets
{
    public class MeterClientSocket
    {
        public WebSocket socket;

        public string meterId;

        public async void HandleDataRefreshedAsync(object sender, LimitedSizeStack a)
        {
            await socket.SendMsgAsync(a.GetString());
        }
    }
}