﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Meters;

namespace WebApplication1.Controllers
{

    public class MeterInfo
    {
        public string meterId;
        public int subscriptionsNum;
        public DateTime latestDateTime;
    }
    public class SocketInfo
    {
        public string socketId;
        public string meterId;
    }


    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [Route("GetMeters")]
        [HttpGet]
        public List<MeterInfo> GetMeters()
        {
            return MeterListener.meters.Select(m => new MeterInfo
            {
                meterId = m.Value.MeterId,
                subscriptionsNum = m.Value.subs,
                latestDateTime = m.Value.meterData.Last.Value.Key
            }).ToList();
        }

        [HttpGet]
        [Route("GetSockets")]
        public List<SocketInfo> GetSockets()
        {
            return new WebSocketConnectionManager().GetAll().Select(s => new SocketInfo
            {
                socketId = s.Key,
                meterId = s.Value.meterId
            })
            .ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // GET api/values/data/5
        [HttpGet("data/{meterId}")]
        public IActionResult GetLast10(string meterId)
        {
            var connectedMeter = MeterListener.GetMeter(meterId);
            if (connectedMeter == null)
            {
                return NotFound();
            }

            var data = connectedMeter.meterData;

            return Ok(data);
        }
        // GET api/values/data/5
        [HttpGet("latestdata/{meterId}")]
        public IActionResult GetLatest(string meterId)
        {
            var connectedMeter = MeterListener.GetMeter(meterId);
            if (connectedMeter == null)
            {
                return NotFound();
            }

            var data = connectedMeter.meterData.OrderByDescending(t => t.Key).First();

            return Ok(data);
        }


        // GET api/values/data/5
        [HttpGet("dataa/{meterId}/{dateTimeFrom}/{dateTimeTo}")]
        public IActionResult GetReadings(string meterId, DateTime? dateTimeFrom = null, DateTime? dateTimeTo = null)
        {
            var data = MeterListener.getP1Data(meterId, dateTimeFrom, dateTimeTo);

            return Ok(data);
        }

        // GET api/values/data/5
        [HttpGet("dataa/{meterId}/{dateTime}")]
        public IActionResult GetDateReadings(string meterId, DateTime dateTime)
        {
            var data = MeterListener.getP1Data(meterId, new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0));

            var result = data
                .GroupBy(x => new DateTime(x.dateTime.Year, x.dateTime.Month, x.dateTime.Day, x.dateTime.Hour, x.dateTime.Minute, 0))
                .Select(g => new { DateTime = g.Key, Usage = Math.Round(g.Max(s => s.reading180) - g.Min(s => s.reading180), 2) })
                .ToList();

            return Ok(result);
        }
    }
}
