package main

import (
	//"encoding/hex"
	//"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
)

const (
	outfile string = "data.log"
)

var (
	datachan chan []byte
)

func handleConnection(conn net.Conn) {
	defer log.Println("Client connection closed")
	io.Copy(os.Stdout, conn)
	conn.Close()

}

func handleConnection2file(conn net.Conn) {
	var data []byte
	var err error

	defer conn.Close()
	defer log.Println("Client connection closed")

	if data, err = ioutil.ReadAll(conn); err != nil {
		log.Println("read error")
		return
	}

	datachan <- data

}

func datachanLogger() {
	//f, err := os.OpenFile(outfile, os.O_WRONLY|os.O_APPEND, os.ModeAppend)
	f, err := os.Create(outfile)
	if err != nil {
		panic(err)
	}

	for data := range datachan {
		//println(data)
		f.Write(data)
		f.Sync()
	}

	f.Close()
}

func main() {
	datachan = make(chan []byte, 1024)
	ln, err := net.Listen("tcp", ":8989")
	if err != nil {
		// handle error
		panic(err)
	}
	log.Println("Server started at tcp *:8989")
	go datachanLogger()
	for {
		conn, err := ln.Accept()
		if err != nil {
			// handle error
			log.Println(err)
			continue
		}
		log.Println("New client connected from ", conn.RemoteAddr())
		go handleConnection2file(conn)
	}
}
