﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace UM.P1.ConsoleApplication
{
    class Program
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;
        protected static IMongoCollection<BsonDocument> _collection;


        public static void Main()
        {
            _client = new MongoClient("mongodb://localhost:27017");
            _database = _client.GetDatabase("foo");
            _collection = _database.GetCollection<BsonDocument>("p1messages");

            TcpListener server = null;
            try
            {
                // Set the TcpListener on port 13000.
                Int32 port = 8989;

                // TcpListener server = new TcpListener(port);
                server = new TcpListener(IPAddress.Any, port);

                // Start listening for client requests.
                server.Start();

                // Buffer for reading data
                Byte[] bytes = new Byte[1124];
                String data = null;

                // Enter the listening loop.
                while (true)
                {
                    Console.Write("Waiting for a connection... ");

                    // Perform a blocking call to accept requests.
                    // You could also user server.AcceptSocket() here.
                    TcpClient client = server.AcceptTcpClient();
                    Console.WriteLine("Connected!");

                    // Get a stream object for reading and writing
                    NetworkStream stream = client.GetStream();

                    int i;

                    // Loop to receive all the data sent by the client.
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        // Translate data bytes to a ASCII string.
                        data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        var document = new BsonDocument
                        {
                            {"createdOn", DateTime.Now },
                            {"message",data},
                            {"RemoteEndPoint", client.Client.RemoteEndPoint.ToString() },
                            {"LocalEndPoint", client.Client.LocalEndPoint.ToString() },
                            {"RemoteEndPointAddress", ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString() },
                            {"LocalEndPointAddress", ((IPEndPoint)client.Client.LocalEndPoint).Address.ToString() }
                        };
                        _collection.InsertOne(document);

                        Console.WriteLine("Received: \n -----------------------------------------------------------\n{0}\n---------------------------------------------------------------\n", data);
                    }

                    // Shutdown and end connection
                    client.Close();
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                // Stop listening for new clients.
                server.Stop();
            }


            Console.WriteLine("\nHit enter to continue...");
            Console.Read();
        }
    }
}
